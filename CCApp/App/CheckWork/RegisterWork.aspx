﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterWork.aspx.cs" Inherits="CCOA.App.CheckWork.RegisterWork" %>

<%@ Register Src="~/App/Pub.ascx" TagName="Pub" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>签到</title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="jquery/lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="jquery/tablestyle.css" rel="stylesheet" type="text/css" />
    <link href="jquery/lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="jquery/lib/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="js/CKAppData.js" type="text/javascript"></script>
    <script src="js/RegisterWork.js" type="text/javascript"></script>
    <style type="text/css">
        .clearfix:after
        {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }
        .clearfix
        {
            display: inline-block;
        }
        * html .clearfix, * html
        {
            height: 1%;
        }
        .clearfix
        {
            display: block;
        }
        .clear
        {
            border-top: 1px solid transparent !important;
            border-top: 0;
            clear: both;
            line-height: 0;
            font-size: 0;
            height: 0;
            height: 1%;
        }
        .goods-detail-tab
        {
            margin: 30px 0 0 0;
            background: url(http://www.zzsky.cn/effect/images/201002070930/tabsepbg.gif) repeat-x 0 1px;
            padding-left: 5px;
            overflow: visible;
        }
        .goods-detail-tab div
        {
            cursor: pointer;
            background: url(Img/tabs_common.gif) no-repeat;
            color: #666;
            display: block;
            float: left;
            height: 17px;
            padding: 4px 2px;
            text-align: center;
            width: 91px;
        }
        
        .goods-detail-tab .active
        {
            background: url(Img/tabs_on.gif) no-repeat;
            color: #000;
            display: block;
            font-size: 14px;
            font-weight: bold;
            height: 26px;
            position: relative;
            margin-bottom: -4px;
            margin-top: -8px;
            padding-top: 6px;
            width: 117px;
        }
        .styleNull
        {
        }
    </style>
    <script type="text/javascript" language="javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="goods-detail-tab clearfix" id="tabs">
        <div class="goodsDetailTab active" onclick="put_css(1)">
            <span>上下班登记</span>
        </div>
        <div class="goodsDetailTab" onclick="put_css(2)">
            <span>外出登记</span>
        </div>
        <div class="goodsDetailTab" onclick="put_css(3)">
            <span>请假登记</span>
        </div>
        <div class="goodsDetailTab" onclick="put_css(4)">
            <span>出差登记</span>
        </div>
    </div>
    <div class="clear">
    </div>
    <div id="ka">
        <div id="div0" class="styleNull">
            <input type="hidden" id="txtrwtype" runat="server" />
            <input type="hidden" id="txtrwstate" runat="server" />
            <div id="pageloading">
            </div>
            <div id="toptoolbar">
            </div>
            <div id="maingrid" style="margin: 0; padding: 0; margin-top: 10px;">
            </div>
            <div id="stateInfo" style="display: none;">
                <table style="margin: 10px;">
                    <tr style="margin: 10px; height: 30px;">
                        <td>
                            备注：
                        </td>
                        <td>
                            <textarea id="TB_Demo" rows="10" style="height: 248px; width: 500px;">
                        
                        </textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="div1" style="display:block">
            <input type="button" value="保存" id="btnWC" onclick="WC_Click();" />
            <table>
                <tr>
                    <td>
                        目的地：
                    </td>
                    <td>
                        <input type="text" id="txtWCD" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        时间：
                    </td>
                    <td>
                        开始时间:
                        <input type="text" id="txtWCtime1" runat="server" />
                        截止时间:
                        <input type="text" id="txtWCtime2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        外出原因：
                    </td>
                    <td>
                        <input type="text" id="txtWCYY" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="maingridWC">
            </div>
        </div>
        <div id="div2" style="display: none">
            <input type="button" id="btnQJ" onclick="QJ_Click();" value="保存" />
            <table>
                <tr>
                    <td>
                        请假时间：
                    </td>
                    <td>
                        开始时间：
                        <input type="text" id="txtQJtime1" runat="server" />
                        截止时间：
                        <input type="text" id="txtQJtime2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        请假原因：
                    </td>
                    <td>
                        <input type="text" id="txtQJYY" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="maingridQJ" style="margin: 0; padding: 0;">
            </div>
        </div>
        <div id="div3" style="display: none">
            <input type="button" id="btnCC" onclick="CC_Click();" value="保存" />
            <table>
                <tr>
                    <td>
                        出差时间：
                    </td>
                    <td>
                        开始时间：
                        <input type="text" id="txtCCtime1" runat="server" />
                        截止时间：
                        <input type="text" id="txtCCtime2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        目的地：
                    </td>
                    <td>
                        <input type="text" id="txtAddr" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        出差原因：
                    </td>
                    <td>
                        <input type="text" id="txtCCYY" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="maingridCC" style="margin: 0; padding: 0;">
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        var tab = document.getElementById('tabs').getElementsByTagName('div');
        //tab切换
        function put_css(id) {
            for (var k = 0; k < tab.length; k++) {
                if (id - 1 == k) {
                    tab[k].className = 'goodsDetailTab active';
                    var div = document.getElementById("div" + k);
                    div.style.display = "block";
                    //列表展示
                    switch (k) {
                        case 0: //签到
                            SetEmpty();
                            LoadGrid();
                            break;
                        case 1: //外出
                            SetEmptyWC();
                            LoadWC();
                            break;
                        case 2: //请假
                            SetEmptyQJ();
                            LoadQJ();
                            break;
                        default:
                            SetEmptyCC();
                            LoadCC();
                            break;
                    }
                } else {
                    tab[k].className = 'goodsDetailTab';
                    var div = document.getElementById("div" + k);
                    div.style.display = "none";
                }
            }
        }
        put_css(1);
    </script>
    </form>
</body>
</html>
