﻿var DefuaultUrl = "DataService/CKDataService.aspx";

ccflow = {}
Application = {}

DataFactory = function () {
    this.data = new ccflow.Data(DefuaultUrl);
}
//配置文件
ccflow.config = {
    IsWinOpenStartWork: 1, //是否启动工作时打开新窗口 0=在本窗口打开,1=在新窗口打开, 2=打开流程一户式窗口
    IsWinOpenEmpWorks: "TRUE"//是否打开待办工作时打开新窗口
};

jQuery(function ($) {
    Application = new DataFactory();
});

ccflow.Data = function (url) {
    this.url = url;
    //获取考勤
    this.getCKCollect = function (callback, scope) {
        var tUrl = this.url;
        var params = { method: "getCKCollect" };
        queryData(tUrl, params, callback, scope);
    }
    //签到类型
    this.getRWType = function (callback, scope) {

        var tUrl = this.url;
        var params = { method: "getRWType" };
        queryData(tUrl, params, callback, scope);
    }
    //签到状态
    this.getRWState = function (callback, scope) {
        var tUrl = this.url;
        var params = { method: "getRWState" };
        queryData(tUrl, params, callback, scope);
    }
    //签到状态操作：增删改 opRWType
    this.opRWState = function (no, name, op, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "opRWState",
            no: no,
            name: name,
            op: op
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取签到类型
    this.getRegister = function (callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getRegister"

        };
        queryData(tUrl, params, callback, scope);
    }

    //签到
    this.registerWork = function (type, demo, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "registerWork",
            type: type,
            demo: demo
        };
        queryData(tUrl, params, callback, scope);
    }
    //签到类型操作：增删改 
    this.opRWType = function (no, name, regulartime, op, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "opRWType",
            no: no,
            name: name,
            regulartime: regulartime,
            op: op
        };
        queryData(tUrl, params, callback, scope);
    }
    //签到类型操作：增删改 
    this.opRWType = function (no, name, regulartime, op, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "opRWType",
            no: no,
            name: name,
            regulartime: regulartime,
            op: op
        };
        queryData(tUrl, params, callback, scope);
    }
    this.registerWorkDJ = function (no, regulartime, demo, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "registerWorkDJ",
            no: no,
            regulartime: regulartime,
            demo: demo
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取 请假
    this.getQJ = function (callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getQJ"
        };
        queryData(tUrl, params, callback, scope);
    }

    //请假
    this.registerQJ = function (starttime, endtime, demo, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "registerQJ",
            starttime: starttime,
            endtime: endtime,
            demo: demo
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取 外出
    this.getWC = function (callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getWC"
        };
        queryData(tUrl, params, callback, scope);
    }

    //外出 
    this.registerWC = function (starttime, endtime, demo, addr, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "registerWC",
            starttime: starttime,
            endtime: endtime,
            demo: demo,
            addr:addr
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取 外出
    this.getCC = function (callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getCC"
        };
        queryData(tUrl, params, callback, scope);
    }

    //出差 
    this.registerCC = function (starttime, endtime, demo, addr, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "registerCC",
            starttime: starttime,
            endtime: endtime,
            demo: demo,
            addr: addr
        };
        queryData(tUrl, params, callback, scope);
    }

    //公共方法
    function queryData(url, param, callback, scope, method, showErrMsg) {
        if (!method) method = 'GET';
        $.ajax({
            type: method, //使用GET或POST方法访问后台
            dataType: "text", //返回json格式的数据
            contentType: "application/json; charset=utf-8",
            url: url, //要访问的后台地址
            data: param, //要发送的数据
            async: true,
            cache: false,
            complete: function () { $("#load").hide(); }, //AJAX请求完成时隐藏loading提示
            error: function (XMLHttpRequest, errorThrown) {
                callback(XMLHttpRequest);
            },
            success: function (msg) {//msg为返回的数据，在这里做数据绑定
                var data = msg;
                callback(data, scope);
            }
        });
    }
}