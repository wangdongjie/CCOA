﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;
using BP.DA;
using System.Data;
using System.Text;

namespace CCOA.App.CheckWork
{
    public partial class RegisterWorkCollect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // GetData();
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        public void GetData()
        {
            StringBuilder sbContent = new StringBuilder("");
            sbContent.Append("  <table>");
            sbContent.Append("<tr><td>签到类型 </td><td>签到时间 </td><td>状态 </td><td>备注</td></tr>");
            string sql = "select FK_RWType,RegisterTime,FK_RWState,Demo  from CW_RegisterWork  order by NYR ";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sbContent.Append("<tr>");
                    sbContent.AppendFormat("<td>{0} </td><td>{1} </td><td>{2} </td><td>{3} </td>"
                       , dr["FK_RWType"].ToString(), dr["RegisterTime"].ToString(), dr["FK_RWState"].ToString(), dr["Demo"].ToString());
                    sbContent.Append("<tr/>");
                }
            }
            sbContent.Append("</table>");
          //  this.divContent.InnerHtml = sbContent.ToString();
        }
    }
}