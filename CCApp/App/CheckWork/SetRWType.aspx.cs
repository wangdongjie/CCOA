﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace CCOA.App.CheckWork
{
    public partial class SetRWType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetData();
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        public void GetData()
        {
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable("select * from cw_rwtype");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //获取页面上的控件列表
                    foreach (Control cl in this.Page.Form.Controls)
                    {
                        if (cl.ID != null && cl.ID != "")
                        {
                            if (cl.ID.IndexOf("txt") >= 0 && cl.ID == "txt" + dr["No"].ToString())
                            {
                                TextBox tb = cl as TextBox;
                                if (tb != null)
                                {
                                    tb.Text = dr["RegularTime"].ToString();
                                }
                            }

                        }

                    }

                }
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            string type = this.txttype.Value;
            string time = this.txttime.Value;
            string name = "";
            if (type != "")
            {
                BP.OA.CheckWork.RWType rwtype = new BP.OA.CheckWork.RWType();
                rwtype.No = type;

                switch (type)
                {
                    case "S1":
                        name = "上班";
                        break;
                    case "S2":
                        name = "上班";
                        break;
                    case "X1":
                        name = "下班";
                        break;
                    case "X2":
                        name = "下班";
                        break;
                }
                rwtype.Name = name;
                rwtype.RegularTime = time;

                if (rwtype.IsExits)//存在，则修改
                {
                    rwtype.Update();
                }
                else
                {
                    rwtype.Insert();
                }
            }

        }

        protected void Btn_Add_Click(object sender, EventArgs e)
        {

        }
    }
}