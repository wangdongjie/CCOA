﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Message/Site1.Master" AutoEventWireup="true"
    CodeBehind="DraftBox.aspx.cs" Inherits="CCOA.App.Message.NotSendBox"  %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
        .flickr
        {
             font: 13px ;
            padding: 10px 200px 10px 0;
            margin: 0px;
        }
        .flickr a
        {
            padding: 1px 6px;
            border: solid 1px #ddd;
            background: #fff;
            text-decoration: none;
            margin-right: 2px;
        }
        .flickr a:visited
        {
            padding: 1px 6px;
            border: solid 1px #ddd;
            background: #fff;
            text-decoration: none;
        }
        .flickr .cpb
        {
            padding: 1px 6px;
            font-weight: bold;
            font-size: 13px;
            border: none;
        }
        .flickr a:hover
        {
            color: #fff;
            background: #ffa501;
            border-color: #ffa501;
            text-decoration: none;
        }
    </style>
    <style type="text/css">
        #txtPageIndex
        {
            width: 20px;
        }
    </style>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkAll() {
            //$(".checkdelete").attr("checked", $("#checkall").attr("checked") == "checked");
            $(".checkdelete").prop("checked", $("#checkall").prop("checked"));
        }
        function checkHasSel() {
            var selCount = 0;
            $(".checkdelete").each(function () {
                if ($(this).prop("checked") == true) selCount++;
            });
            if (selCount == 0) {
                alert("你没有选择记录");
                return false;
            }
            else {
                return confirm("你真要删除你所选择的信息吗？");
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div>
        <div style="text-align: right; margin-right: 50px;">
            <asp:Button ID="btnEmptyBox" runat="server" Text="清空草稿箱" OnClientClick="return confirm('你真的要清空草稿箱吗？')" OnClick="btnEmptyBox_Click" />
            <asp:Button ID="btnDeleteMsg" runat="server" Text="删除所选信息" OnClientClick="return checkHasSel()"
                OnClick="btnDeleteMsg_Click" />
            输入关键字:<asp:TextBox ID="txtKeywords" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearchByKey" runat="server" Text="查询" OnClick="btnSearchByKey_Click" />
        </div>
        <asp:GridView ID="gridData" runat="server" CssClass="grid" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="全选" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <HeaderTemplate>
                   <input type="checkbox" title='全选' id="checkall" onclick='checkAll()' />
                </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" name="chkSel" value='<%# Eval("No") %>' class="checkdelete"  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="标记" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img style="height: 12px;" src="img/Attach.png" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" title="有附件" style="display: <%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"none":""%>">
                            <img alt="有附件" style="height: 12px;" src="<%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"":"Img/Attach.png" %>" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主题">
                    <ItemTemplate>
                        <a style="font-weight:bold; color:#444;" href="EditMsg.aspx?ID=<%# Eval("No") %>"><%#GetContentStr(Eval("Title")) %></a></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="收件人" ItemStyle-Width="300px">
                    <ItemTemplate>
                        <%# this.GetUserNames(Eval("Geter"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="创建时间" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <span title="<%# Eval("RPT") %>">
                            <%#BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(Eval("RPT")),null,null) %></span>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="text-align: center; padding-top: 5px;">
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="flickr" AlwaysShow="false"
                FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" ShowCustomInfoSection="Left"
                ShowInputBox="Never" LayoutType="Table" OnPageChanged="AspNetPager1_PageChanged">
            </webdiyer:AspNetPager>
        </div>
    </div>
</asp:Content>
