﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.DA;

namespace CCOA.App.Message
{
    public partial class SendBox : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        private void DeleteUseLessData()
        {
            //string sSql = "delete from OA_Message where OID not in (select FK_MsgNo from OA_InBox) and OID not in (select FK_MsgNo from OA_SendBox)";
            //BP.DA.DBAccess.RunSQL(sSql);
            BP.OA.Message.Message.DeleteUseLessData();
        }
        public string GetUserNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return BP.OA.GPM.GetUserNames(users);
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.SetFocus(btnSearchByKey);
            if (!this.IsPostBack)
            {
                this.LoadData(true);
            }
        }
        private void LoadData(bool start)
        {
            string con = null;
            string key = this.txtKeywords.Text.Trim();
            if (!String.IsNullOrEmpty(key))
            {
                con = String.Format(" and ( B.Doc like '%{0}%' OR Title like '%{1}%')", key, key);
            }
            //需要返回列：No,Content,Geter,RPT
            string sSql = String.Format("select A.OID as No,B.Title as Title,A.SendTime as RPT,Receiver as Geter,B.AttachFile from OA_MessageSendBox A inner join OA_Message B on A.FK_MsgNo=B.OID where FK_SendUserNo='{0}' {1}", BP.Web.WebUser.No, con);
            if (start)
            {
                AspNetPager1.RecordCount = BP.OA.Main.GetPagedRowsCount(sSql);  //第一次需要初始化
                AspNetPager1.PageSize = BP.OA.Main.GetConfigItem("PageSize") == null ? 20 : Convert.ToInt32(BP.OA.Main.GetConfigItem("PageSize"));
                AspNetPager1.CurrentPageIndex = 1;
            }
            //this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, this.AspNetPager1.PageCount, this.AspNetPager1.RecordCount, this.AspNetPager1.PageSize });
            int total = DBAccess.RunSQLReturnCOUNT(sSql);
            int PageCount = total % (AspNetPager1.PageSize) == 0 ? total / (AspNetPager1.PageSize) : total / (AspNetPager1.PageSize) + 1;
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, PageCount, total, this.AspNetPager1.PageSize });
            this.gridData.DataSource = BP.OA.Main.GetPagedRows(sSql, 0, " Order by No desc ", this.AspNetPager1.PageSize, this.AspNetPager1.CurrentPageIndex);
            //System.Data.DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            //this.gridData.DataSource = dt;
            this.gridData.DataBind();
        }
        public string GetContentStr(object evalContent)
        {
            string s = String.Format("{0}", evalContent);
            if (!String.IsNullOrEmpty(s))
            {
                if (s.Length > 100)
                    s = s.Substring(0, 100) + "..";
            }
            return s;
        }
        public string GetUrl(object evalNo)
        {
            //return String.Format("javascript:zDialog_open0('SendDetail.aspx?OID={0}','发件',600,600,false)", evalNo);
            return String.Format("SendDetail1.aspx?OID={0}", evalNo);
        }
        #endregion

        #region //3.页面事件(Page Event)
        /// <summary>
        /// 根据关键字 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchByKey_Click(object sender, EventArgs e)
        {
            this.LoadData(true);
        }
        protected void btnEmptyBox_Click(object sender, EventArgs e)
        {
            string sql = String.Format("Insert into OA_MessageRecycleBox(OID,AddTime,FromType,FK_MsgNo,FK_UserNo) select OID,SendTime,{2},FK_MsgNo,'{3}' from OA_MessageSendBox Where FK_SendUserNo='{0}'; "
                                                    + "Delete from OA_MessageSendBox Where FK_SendUserNo='{0}';"
                                                    , BP.Web.WebUser.No, DateTime.Now, 1, BP.Web.WebUser.No);
            BP.DA.DBAccess.RunSQL(sql);
            this.DeleteUseLessData();
            this.LoadData(true);
        }

        protected void btnDeleteMsg_Click(object sender, EventArgs e)
        {
            string chkSel = this.Request.Form["chksel"];
            //string sql = String.Format("Delete OA_MessageSendBox Where FK_SendUserNo='{0}' and OID in ({1})", BP.Web.WebUser.No, chkSel);
            string sql = String.Format("Insert into OA_MessageRecycleBox(OID,AddTime,FromType,FK_MsgNo,FK_UserNo) select OID,SendTime,{3},FK_MsgNo,'{4}' from OA_MessageSendBox Where FK_SendUserNo='{0}' and OID in ({1}); "
                                                 + "Delete from OA_MessageSendBox Where FK_SendUserNo='{0}' and OID in ({1});"
                                                    , BP.Web.WebUser.No, chkSel, DateTime.Now, 1, BP.Web.WebUser.No);
            BP.DA.DBAccess.RunSQL(sql);
            this.DeleteUseLessData();
            this.LoadData(true);
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            if (this.gridData.Rows.Count > 0)
                this.gridData.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            this.LoadData(false);
        }
        #endregion
    }
}