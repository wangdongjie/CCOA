﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Message
{
    public partial class MsgDetail : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private string rOID
        {
            get { return Convert.ToString(Request.QueryString["ID"]); }
        }

        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
            }
        }
        //数据库中存储的附件的连接
        string DocFile = "/SendMsgUpFile/4546.docx";

        private void LoadData()
        {
            string[] data = new String[]{
            "No,Title,Content,Geter,RPT,DocFile"
            ,"1,到期提醒,您设置了中转站文件过期前的邮件提醒以下文件即将过期,CCfolw-小黄,2013年6月20日(星期四)上午10:07,/SendMsgUpFile/4546.docx"
            };
            System.Data.DataTable dt = BP.OA.Main.GetTestTable(data);

            this.divUsers.InnerHtml = dt.Rows[0][3].ToString();
            this.lblSTime.Text = dt.Rows[0][4].ToString();
            this.divContent.InnerHtml = dt.Rows[0][2].ToString();
        }
        #endregion

        #region //3.页面事件(Page Event)
        /// <summary>
        /// 超链接 下载
        /// </summary>
        protected void HrefClik(object sender, EventArgs e)
        {
            //下载 
            string filename = "";
            string filepath = Server.UrlDecode(DocFile);
            filename = Server.MapPath(filepath);
            Response.ContentType = "application/x-zip-compressed";
            filename = filename.Substring(filename.LastIndexOf("\\") + 1);
            Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(filename));
            Response.TransmitFile(filepath);
        }

        #endregion
    }
}