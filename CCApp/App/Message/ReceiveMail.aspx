﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true"  CodeBehind="ReceiveMail.aspx.cs" MasterPageFile="~/Main/master/Site1.Master"
 Inherits="CCOA.App.Message.ReceiveMail" %>

 <%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
 </asp:Content>
 <asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
<asp:Panel ID="mainPan" runat="server">
    <div style="font-size: 35px; font-weight: bolder;">
        <img style="width: 40px; height: 40px;" src="img/msgbox.png" alt="收件箱" />收件箱</div>
<%--        <asp:Button ID="Button2" runat="server" Text="发送" OnClick="btnSendMsg_Click" />--%>
    <div> 
        <asp:GridView ID="gridData" runat="server" CssClass="grid" AutoGenerateColumns="False" >
            <Columns>
                <asp:TemplateField HeaderText="全选" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <HeaderTemplate>
                   <input type="checkbox" title='全选' id="checkall" onclick='checkAll()' />
                </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" name="chkSel" value='<%# Eval("No") %>' class="checkdelete"  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="标记" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img style="height: 12px;" src="img/Attach.png" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" title="有附件" style="display: <%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"none":""%>">
                            <img alt="有附件" style="height: 12px;" src="<%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"":"Img/Attach.png" %>" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主题">
                    <ItemTemplate>
                        <a style="font-weight:bold; color:#444;" href="<%# GetUrl(Eval("No")) %>"><%#GetContentStr(Eval("Title")) %></a>
                        <%# BP.OA.Main.GetTimeImg(Eval("RPT"),2,"新邮件","img/email_New.gif",0,0)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="收件人" ItemStyle-Width="300px">
                    <ItemTemplate>
                        <%#this.GetUserNames(Eval("Geter"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发送时间" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# Eval("RPT") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="text-align: center; padding-top: 5px;">
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="flickr">
            </webdiyer:AspNetPager>
        </div>
    </div>
    </asp:Panel>
    <asp:Panel ID="login" runat="server">
        <div id="log" style=" margin-left:auto; margin-right:auto; margin-top:200px; width:500px; height:300px; text-align:center;padding-top:5px;" >
            <table class="userLog" style="text-align:center;width: 482px; height: 157px">
                <tr>
                    <td class="style1" style="text-align:right;">邮箱登录名</td>
                    <td>
                        <div id="div_Users" style="display:block; margin-top: 1px; padding-top: 1px;">
                        <asp:TextBox ID="txt_Users" runat="server" Width="70%" BackColor="#eeeeee" ></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style1" style="text-align:right;">登录密码</td>
                    <td>
                        <div id="div1" style="display:block; margin-top: 1px; padding-top: 1px;">
                        <asp:TextBox ID="txt_Pwd" runat="server" Width="70%" BackColor="#eeeeee" ></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <div id="div3" style="display:block; margin-top: 1px; padding-top: 1px;">
                        <asp:Button ID="Button1" Text="Login" runat="server" Width="70%" 
                                BackColor="#eeeeee" onclick="BtnLogin_Click" ></asp:Button>
                        </div>
                    </td>
                    <td>
                        <div id="div2" style="display:block; margin-top: 1px; padding-top: 1px;">
                        <asp:Button ID="Btn2" Text="Cancel" runat="server" Width="70%" BackColor="#eeeeee" 
                                onclick="Btn2_Click" ></asp:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>

