﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OutRegist.aspx.cs" Inherits="CCOA.App.Attendance.OutRegist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/lang/en.js" type="text/javascript"></script>
    <script src="../../Js/Validate/formValidator-4.0.1.min.js" type="text/javascript"></script>
    <script src="../../Js/Validate/formValidatorRegex.js" type="text/javascript"></script>
    <script src="../../Js/Validate/site.js" type="text/javascript"></script>
    <link href="../../Js/Validate/style/validator.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Validate/DateTimeMask.js" type="text/javascript"></script>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .td2
        {
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="90%" align="center">
        <tr>
            <td>
                外出人：
            </td>
            <td class="td2">
                <input type="text" name="COPY_TO_ID" value="">
            </td>
        </tr>
        <tr>
            <td>
                外出原因：
            </td>
            <td class="td2">
                <textarea name="OUT_TYPE" cols="60" rows="3"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                外出时间：
            </td>
            <td class="td2">
                日期
                <input type="text" name="OUT_DATE" size="15" maxlength="10" class="BigInput" value="2014-05-07"
                    onclick="WdatePicker()" />
                从
                <input type="text" name="OUT_TIME1" size="5" maxlength="5" class="BigInput" readonly
                    value="22:15" onclick="WdatePicker({dateFmt:'HH:mm'})">
                至
                <input type="text" name="OUT_TIME2" size="5" maxlength="5" class="BigInput" readonly
                    value="23:15" onclick="WdatePicker({dateFmt:'HH:mm'})"><br>
            </td>
        </tr>
        <tr>
            <td>
                审批人：
            </td>
            <td class="td2">
                <select>
                    <option value="admin">系统管理员</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                是否用车：
            </td>
            <td class="td2">
                <input type="radio" name="RD" value="1">是
                <input type="radio" name="RD" value="0" checked="true">否
            </td>
        </tr>
        <tr align="center" class="TableControl">
            <td colspan="2">
                <input type="submit" value="申请外出" class="BigButton" title="申请外出">&nbsp;&nbsp;
                <input type="button" value="返回上页" class="BigButton" onclick="location='../'">&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
