﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using BP.En;
using BP.DA;
using BP.Sys;
using BP.WF;
using BP.OA.Notepaper;
using BP.OA;
using BP.OA.PrivPlan;
using BP.OA.Calendar;
using BP.Tools;
namespace CCOA.App.Serv
{
    /// <summary>
    /// ToolsServer 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://ccflow.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class ToolsServer : System.Web.Services.WebService
    {
        #region  我的记事便签
        /// <summary>
        /// 获取我的记事便签
        /// </summary>
        /// <param name="UserNo">用户编号</param>
        /// <returns></returns>
        [WebMethod]
        //[System.Web.Services.Protocols.SoapHeader("oaHeader")]
        public string GetMyNotepaper(string UserNo)
        {
            //if (!oaHeader.ValidateUser) return "error:没有权限访问。";
            if (string.IsNullOrEmpty(UserNo)) return "error:没有传入用户编号。";

            Notepapers myNotepapers = new Notepapers();
            myNotepapers.RetrieveByAttr(NotepaperAttr.FK_Emp, UserNo);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(myNotepapers);
        }
        /// <summary>
        /// 添加记事便签
        /// </summary>
        /// <param name="UserNo">用户编号</param>
        /// <param name="Title">标题</param>
        /// <param name="Content">便签内容</param>
        /// <returns>处理情况</returns>
        [WebMethod]
        public string CreateNotepaper(string UserNo, string Title, string Content)
        {
            #region 信息验证
            string msg = "error:";
            if (string.IsNullOrEmpty(UserNo))
                msg += "人员编号为空，";
            if (string.IsNullOrEmpty(Title))
                msg += "标题为空";
            if (!msg.Equals("error:")) return msg;
            #endregion

            Notepaper notepaper = new Notepaper();
            notepaper.Name = Title;
            notepaper.NoteContent = Content;
            notepaper.FK_Emp = UserNo;
            int i = notepaper.Insert();
            return i > 0 ? "true" : "false";
        }

        /// <summary>
        /// 根据编号获取记事便签详细
        /// </summary>
        /// <param name="notepaperID">编号</param>
        /// <returns></returns>
        [WebMethod]
        public string GetNotepaper(string notepaperID)
        {
            Notepapers notepapers = new Notepapers();
            notepapers.RetrieveByAttr(NotepaperAttr.No, notepaperID);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(notepapers);
        }

        /// <summary>
        /// 更新记事便签
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="title">标题</param>
        /// <param name="content">内容</param>
        /// <returns>执行情况</returns>
        [WebMethod]
        public string EditNotepaper(string id, string title, string content)
        {
            Notepaper notepaper = new Notepaper();
            notepaper.RetrieveByAttr(NotepaperAttr.No, id);
            if (notepaper.No != "")
            {
                notepaper.Name = title;
                notepaper.NoteContent = content;
                int i = notepaper.Update();
                return i > 0 ? "true" : "false";
            }
            return "false";
        }

        /// <summary>
        /// 删除记事便签
        /// </summary>
        /// <param name="id">记事便签编号</param>
        /// <returns>执行成功true,失败false</returns>
        [WebMethod]
        public string DelNotepaper(string id)
        {
            Notepaper notepaper = new Notepaper();
            int i = notepaper.Delete(NotepaperAttr.No, id);
            return i > 0 ? "true" : "false";
        }
        #endregion

        #region 我的通讯录
        /// <summary>
        /// 获取部门通讯录列表
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetDepartmentContacts(string DeptNo)
        {
            if (string.IsNullOrEmpty(DeptNo)) return "error:没有传入部门编号。";
            ALDepts aldepts = new ALDepts();
            aldepts.RetrieveByAttr(ALDeptAttr.No, DeptNo);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(aldepts);
        }

        /// <summary>
        /// 获取个人通讯录列表
        /// </summary>
        /// <param name="UserNo">当前执行添加的人员</param>
        /// <returns></returns>
        [WebMethod]
        public string GetPersonAddrBook(string fk_emp)
        {
            if (string.IsNullOrEmpty(fk_emp)) return "error:没有传入用户编号 ";
            ALEmps alemps = new ALEmps();
            alemps.RetrieveByAttr(ALEmpAttr.FK_Emp, fk_emp);

            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(alemps);
        }
        /// <summary>
        /// 添加个人通讯录
        /// </summary>
        /// <param name="FK_Emp">当前执行的添加人员</param>
        /// <param name="Name">姓名</param>
        /// <param name="Dept">部门</param>
        /// <param name="Tel">电话号码</param>
        /// <param name="Email">电子邮箱</param>
        /// <param name="FaxNo">传真号</param>
        /// <param name="Role">角色</param>
        /// <param name="PosiTion">职位</param>
        /// <param name="Organization">组织</param>
        /// <param name="Address">住址</param>
        /// <param name="Birthday">生日</param>
        /// <param name="MyHomePage">个人主页</param>
        ///  <param name="Remarks">备注</param>
        /// <returns></returns>
        [WebMethod]
        public string CreatePersAddrBook(string FK_Emp, string Name, string Dept, string Tel, string Email, string FaxNo,
         string Role, string PosiTion, string Organization, string Address, string Birthday, string MyHomePage, string Remarks)
        {
            #region 信息验证
            string msg = "error:";
            if (string.IsNullOrEmpty(FK_Emp))
                msg += "执行添加人员不能为空";
            if (string.IsNullOrEmpty(Name))
                msg += "姓名不能为空";
            if (string.IsNullOrEmpty(Dept))
                msg += "部门不能为空";
            if (string.IsNullOrEmpty(Tel))
                msg += "电话不能为空";
            if (string.IsNullOrEmpty(Email))
                msg += "邮箱不能为空";
            if (!msg.Equals("error:")) return msg;
            #endregion
            ALEmp alemp = new ALEmp();
            alemp.FK_Emp = FK_Emp;
            alemp.Name = Name;
            alemp.Dept = Dept;
            alemp.Tel = Tel;
            alemp.faxNo = FaxNo;
            alemp.Email = Email;
            alemp.Birthday = Birthday;
            alemp.MyHomePage = MyHomePage;
            alemp.Organization = Organization;
            alemp.PosiTion = PosiTion;
            alemp.Role = Role;
            alemp.Address = Address;
            alemp.Remarks = Remarks;
            int i = alemp.Insert();
            return i > 0 ? "true" : "false";
        }
        /// <summary>
        /// 编辑个人通讯录
        /// </summary>
        /// <param name="No">编号</param>
        /// <param name="Name">姓名</param>
        /// <param name="Dept">部门</param>
        /// <param name="Tel">电话号码</param>
        /// <param name="Email">电子邮箱</param>
        /// <param name="FaxNo">传真号</param>
        /// <param name="Role">角色</param>
        /// <param name="PosiTion">职位</param>
        /// <param name="Organization">组织</param>
        /// <param name="Address">住址</param>
        /// <param name="Birthday">生日</param>
        /// <param name="MyHomePage">个人主页</param>
        ///  <param name="Remarks">备注</param>
        /// <returns></returns>
        [WebMethod]
        public string EditPersAddrBook(string No, string Name, string Dept, string Tel, string Email, string FaxNo,
         string Role, string PosiTion, string Organization, string Address, string Birthday, string MyHomePage, string Remarks)
        {

            ALEmp alemp = new ALEmp();
            alemp.RetrieveByAttr(ALEmpAttr.No, No);
            if (alemp.No != "")
            {
                alemp.Name = Name;
                alemp.Dept = Dept;
                alemp.Tel = Tel;
                alemp.faxNo = FaxNo;
                alemp.Email = Email;
                alemp.Birthday = Birthday;
                alemp.MyHomePage = MyHomePage;
                alemp.Organization = Organization;
                alemp.PosiTion = PosiTion;
                alemp.Role = Role;
                alemp.Address = Address;
                alemp.Remarks = Remarks;
                int i = alemp.Update();
                return i > 0 ? "true" : "false";
            }
            return "false";

        }
        /// <summary>
        /// 删除个人通讯录
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>执行成功true,失败false</returns>
        [WebMethod]
        public string DelPersAddrBook(string id)
        {
            ALEmp alemp = new ALEmp();
            int i = alemp.Delete(ALEmpAttr.No, id);
            return i > 0 ? "true" : "false";
        }
        #endregion

        #region  我的计划
        /// <summary>
        /// 获取我的计划列表
        /// </summary>
        /// <param name="id">用户编号</param>
        /// <returns>返回计划列表</returns>
        [WebMethod]
        public string GetMyPlan(string id)
        {
            if (string.IsNullOrEmpty(id)) return "error:没有传入用户编号。";

            string sSql = string.Format("select a.Task,b.* from OA_PrivPlanItem a ,OA_PrivPlan b  where  b.FK_UserNo='"+id+"'");
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            return   DataTableConvertJson.DataTable2Json(dt,dt.Rows.Count);
        }
        /// <summary>
        /// 添加计划
        /// </summary>
        /// <param name="planDoc">计划简介</param>
        /// <param name="itemHours">耗时  格式：1~8</param> 
        /// <param name="itemDoc">所有计划任务</param>
        /// <param name="count">计划项数目</param>
        ///  <param name="itemType">任务类型 0:可选任务   1:组要任务</param>
        /// <returns></returns>
        [WebMethod]
        public string CreatePrivPlan(string planDoc, int count, int itemHours, string itemDoc, int itemType)
        {

            if (String.IsNullOrEmpty(planDoc) || planDoc.Trim().Length < 10)
            {            
                return "计划简介不能为空，且不能少于10个字！";
            }
            if (count == 0)
            {
                              return "计划项不能为空！";
            }
            bool hasMainTask = false;
            int hoursSum = 0;
            for (int i = 1; i <= count; i++)
            {
                
                hoursSum += itemHours;
                if (itemType == 1) hasMainTask = true;
                if (hasMainTask && itemHours == 0)
                {                   
                    return "计划主要任务项必须设置所用时间！";
                }
                if (String.IsNullOrEmpty(itemDoc) || itemDoc.Trim().Length < 10)
                {                  
                    return "所有计划任务的要求不能为空，最少10个字，且最好要量化！";
                }
            }
            if (!hasMainTask)
            {
               
                return "每日计划任务项不能没有主要任务项！";
            }
            if (hoursSum < 6)
            {
                
                return"计划任务所用时间总和不得小于6个小时！";
            }
            //3.数据处理
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            pp.AddTime = DateTime.Now;
            pp.PlanDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
            pp.MyDoc = planDoc;
            pp.FK_UserNo = BP.Web.WebUser.No;
            pp.Checked = false;
            pp.Insert();
            if (pp.OID == 0)
            {
                        return"计划添加失败！";
            }
            for (int i = 1; i <= count; i++)
            {
               
                BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
                ppi.FK_PrivPlan = pp.OID;
                ppi.AddTime = DateTime.Now;
                ppi.Hours = itemHours;
                ppi.Score = 0;
                ppi.Sort = i;
                ppi.Succeed = false;
                ppi.Task = itemDoc;
                ppi.Insert();
            }
            //4.统计/日志
            //5.提交信息
         
            return "计划添加成功！";

        }
        /// <summary>
        /// 编辑计划
        /// </summary>
        /// <param name="planDoc">计划简介</param>
        /// <param name="itemHours">耗时</param>
        /// <param name="itemDoc">所有计划任务</param>
        /// <returns></returns>
        [WebMethod]
        public string EditPrivPlan(string id, string planDoc, int itemHours, string itemDoc)
        {
            string msg = "error:";
            if (string.IsNullOrEmpty(planDoc) || planDoc.Trim().Length < 10)
                msg += "计划简介不能为空，且不能少于10个字！";
            if (string.IsNullOrEmpty(itemDoc) || itemDoc.Trim().Length < 10)
                msg += "所有计划任务的要求不能为空，最少10个字，且最好要量化！";
            //privpan类编辑
            BP.OA.PrivPlan.PrivPlan privpan = new BP.OA.PrivPlan.PrivPlan();
            privpan.RetrieveByAttr(PrivPlanAttr.OID, id);
            if (privpan.OID != 0)
            {
                privpan.AddTime = DateTime.Now;
                privpan.PlanDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
                privpan.MyDoc = planDoc;
                privpan.FK_UserNo = BP.Web.WebUser.No;
                privpan.Checked = false;
                privpan.Update();
                //PrivPlanItem 类编辑
                BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
                ppi.FK_PrivPlan = privpan.OID;
                ppi.AddTime = DateTime.Now;
                ppi.Hours = itemHours;
                ppi.Score = 0;
                ppi.Succeed = false;
                ppi.Task = itemDoc;
                ppi.Update();
                int i = ppi.Update();
                return i > 0 ? "true" : "false";
            }
            return "false";
        }
        /// <summary>
        /// 通过id删除计划
        /// </summary>
        /// <param name="id">计划编号</param>
        /// <returns>执行成功true,失败false</returns>
        [WebMethod]
        public string DelPrivPlan(string id)
        {
            BP.OA.PrivPlan.PrivPlan privpan = new BP.OA.PrivPlan.PrivPlan();
            privpan.Delete(PrivPlanAttr.OID,id);
            BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
            int i = ppi.Delete(PrivPlanItemAttr.FK_PrivPlan, id);  
            return i > 0 ? "true" : "false";

        }
        #endregion

        #region 我的日程
        /// <summary>
        /// 获取日程列表
        /// </summary>
        /// <param name="id">用户编号</param>
        /// <returns></returns>
        [WebMethod]
        public string GetCalendarTask(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return "error:没有传入用户编号";
            }
            CalendarTasks Calendars = new CalendarTasks();
            Calendars.RetrieveByAttr(CalendarTaskAttr.FK_UserNo, id);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(Calendars);

        }
        /// <summary>
        /// 添加日程
        /// </summary>
        /// <param name="FK_UserNo">所属用户</param>
        /// <param name="CalendarCatagory">日历类型</param>
        /// <param name="Title">日程标题</param>
        /// <param name="StartDate">开始日期</param>
        /// <param name="EndDate">结束日期</param>
        /// <param name="Location">地点</param>
        /// <param name="Notes">文本</param>
        /// <param name="Url">网址</param>
        /// <param name="IsAllDay">全天行为</param>
        /// <param name="Reminder">日程内容</param>
        /// <param name="IsNew">是否为新</param>
        /// <returns></returns>
        [WebMethod]
        public string CreatCalendarTask(string FK_UserNo, int CalendarCatagory, string Title, DateTime StartDate, DateTime EndDate,
        string Location, string Notes, string  Url,bool IsAllDay,string Reminder,bool IsNew)
        {
            string msg = "error:";
            if (string.IsNullOrEmpty(FK_UserNo))
                msg += "所属用户不能为空";
            if (string.IsNullOrEmpty(Title))
                msg += "日程标题不能为空";
            if (string.IsNullOrEmpty(Reminder))
                msg += "日程内容不能为空";
            CalendarTask calendartask = new CalendarTask();
            calendartask.FK_UserNo = FK_UserNo;
            calendartask.CalendarCatagory = CalendarCatagory;
            calendartask.Title = Title;
            calendartask.StartDate = StartDate;
            calendartask.EndDate = EndDate;
            calendartask.Location = Location;
            calendartask.Notes = Notes;
            calendartask.Url = Url;
            calendartask.IsAllDay = IsAllDay;
            calendartask.Reminder = Reminder;
            calendartask.IsNew = IsNew;
            int i = calendartask.Insert();
            return i > 0 ? "true" : "false";
        }
        /// <summary>
        /// 编辑日程
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="CalendarCatagory">日历类型</param>
        /// <param name="Title">日程标题</param>
        /// <param name="StartDate">开始日期</param>
        /// <param name="EndDate">结束日期</param>
        /// <param name="Location">地点</param>
        /// <param name="Notes">文本</param>
        /// <param name="Url">网址</param>
        /// <param name="IsAllDay">全天行为</param>
        /// <param name="Reminder">日程内容</param>
        /// <param name="IsNew">是否为新</param>
        /// <returns></returns>
        [WebMethod]
        public string EditCalendarTask(string id, int CalendarCatagory, string Title, DateTime StartDate, DateTime EndDate,
        string Location, string Notes, string  Url,bool IsAllDay,string Reminder,bool IsNew)
        {
            CalendarTask calendartask = new CalendarTask();
            calendartask.RetrieveByAttr(CalendarTaskAttr.OID,id);
            if (calendartask.OID!=0)
            {
                calendartask.CalendarCatagory = CalendarCatagory;
                calendartask.Title = Title;
                calendartask.StartDate = StartDate;
                calendartask.EndDate = EndDate;
                calendartask.Location = Location;
                calendartask.Notes = Notes;
                calendartask.Url = Url;
                calendartask.IsAllDay = IsAllDay;
                calendartask.Reminder = Reminder;
                calendartask.IsNew = IsNew;
                int i = calendartask.Update();
                return i > 0 ? "true" : "false";
                
            }
            return "false";
        }
        /// <summary>
        /// 删除日程
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [WebMethod]
        public string DelCalendarTask(string id)
        {
            CalendarTask calendartask = new CalendarTask();
            int i = calendartask.Delete(CalendarTaskAttr.OID, id);
            return i > 0 ? "true" : "false";
        }
        #endregion
    }
}
