﻿using System;
using System.Collections.Generic;
using System.Web;

namespace CCOA.App.Serv
{
    public class OASoapHeader : System.Web.Services.Protocols.SoapHeader
    {
        private string userNo = string.Empty;
        private string passWord = string.Empty;

        /// <summary>
        /// 构造函数
        /// </summary>
        public OASoapHeader()
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="userNo">用户编号</param>
        /// <param name="passWord">密码</param>
        public OASoapHeader(string userNo, string passWord)
        {
            this.userNo = userNo;
            this.passWord = passWord;
        }

        /// <summary>
        /// 获取或设置用户用编号
        /// </summary>
        public string UserNo
        {
            get { return UserNo; }
            set { UserNo = value; }

        }

        /// <summary>
        /// 获取或设置用户密码
        /// </summary>
        public string PassWord
        {
            get { return passWord; }
            set { passWord = value; }
        }

        /// <summary>
        /// 用户验证
        /// </summary>
        /// <returns></returns>
        public bool ValidateUser
        {
            get
            {
                try
                {
                    BP.Port.Emp emp = new BP.Port.Emp(UserNo);
                    if (emp.Pass == PassWord)
                        return true;
                }
                catch (Exception ex)
                {
                }
                return false;
            }
        }
    }
}