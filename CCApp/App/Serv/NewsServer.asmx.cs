﻿using BP.OA;
using BP.Sys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using BP.Tools;

namespace CCOA.App.Serv
{
    /// <summary>
    /// NewsServer 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。
    // [System.Web.Script.Services.ScriptService]
    public class NewsServer : System.Web.Services.WebService
    {
        #region 1.新闻管理
        /// <summary>
        /// 获取新闻类型
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNewsCategoryAll()
        {
            ArticleCatagorys atcts = new ArticleCatagorys();
            atcts.RetrieveAll();
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(atcts);
        }
        /// <summary>
        /// 根据新闻类型获取新闻列表
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNewsByCategory(string no)
        {
            Articles arts = new Articles();
            arts.RetrieveByAttr("FK_ArticleCatagory", no);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(arts);
        }
        /// <summary>
        /// 根据新闻oid获取详细新闻
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNewDetailByID(string id)
        {
            //返回数据表格得包含以下字段：Title,KeyWords,ArticleCatagory,AddTime,FK_UserNo,Doc,ArticleSource

            Articles arts = new Articles();
            arts.RetrieveByAttr("OID", id);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(arts);
        }
        /// <summary>
        /// 根据新闻oid删除
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string DelNewByID(string id)
        {
            Article ar = new Article();
            ar.OID = Int32.Parse(id);
            return ar.Delete() > 0 ? "true" : "false";
        }
        //<summary>
        //根据新闻多个oid批量删除
        //用“，”隔开
        //</summary>
        //<returns></returns>
        [WebMethod]
        public string DelMutiNewByID(string idList)
        {
            string sSql = String.Format("Delete from OA_Article Where OID in ({0})", idList);
            return BP.DA.DBAccess.RunSQL(sSql) > 0 ? "true" : "false";
        }
        /// <summary>
        /// 根据新闻oid编辑
        /// </summary>
        /// <param name="oid">文章id</param>
        /// <param name="FK_UserNo">发布者</param>
        /// <param name="FK_ArticleCatagory">新闻类型是int类型</param>
        /// <param name="AddTime">添加时间</param>
        /// <param name="Title">标题</param>
        /// <param name="KeyWords">关键字</param>
        /// <param name="ArticleSource">文章来源</param>
        /// <param name="Doc">内容</param>
        /// <param name="AttachFile">附件地址</param>
        /// <returns></returns>
        [WebMethod]
        public string EditNew(string oid, string FK_UserNo, string FK_ArticleCatagory,
        string AddTime, string Title, string KeyWords, string ArticleSource, string Doc, string AttachFile)
        {
            Articles ars = new Articles();
            ars.RetrieveAll();
            Article ar = (Article)ars.Filter(ArticleAttr.OID, oid);
            if (ar != null)
            {
                ar.FK_UserNo = FK_UserNo;
                ar.FK_ArticleCatagory = FK_ArticleCatagory;
                ar.AddTime = BP.DA.DataType.CurrentDataTime; 
                ar.Title = Title;
                ar.KeyWords = KeyWords;
                ar.ArticleSource = ArticleSource;
                ar.Doc = Doc;
                ar.AttachFile = AttachFile;
                return ar.Update() > 0 ? "true" : "false";
            }
            return "false";
        }
        /// <summary>
        /// 添加新闻
        /// </summary>
        /// <param name="FK_UserNo">发布者</param>
        /// <param name="FK_ArticleCatagory">新闻类型</param>
        /// <param name="AddTime">添加时间</param>
        /// <param name="Title">标题</param>
        /// <param name="KeyWords">关键字</param>
        /// <param name="ArticleSource">新闻来源</param>
        /// <param name="Doc">新闻内容</param>
        /// <param name="AttachFile">附件是文件的连接地址</param>
        /// <returns></returns>
        [WebMethod]
        public string AddNew(string FK_UserNo, string FK_ArticleCatagory, string Title, string KeyWords, string ArticleSource, string Doc, string AttachFile)
        {
            Article ar = new Article();
            ar.FK_UserNo = FK_UserNo;
            ar.FK_ArticleCatagory = FK_ArticleCatagory;
            ar.AddTime = BP.DA.DataType.CurrentDataTime; 
            ar.Title = Title;
            ar.KeyWords = KeyWords;
            ar.ArticleSource = ArticleSource;
            ar.AttachFile = AttachFile;
            ar.Doc = Doc;
            return ar.Insert() > 0 ? "true" : "false";
        }
        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="oid">oid编号</param>
        /// <returns></returns>
        [WebMethod]
        public string SetTop(string oid)
        {
            Articles art = new Articles();
            art.RetrieveAll();
            Article ar = (Article)art.Filter("OID", oid);

            if (ar != null)
            {
                ar.SetTop = DateTime.Now;
                return "true";
            }
            return "false";
        }
        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="oid">编号</param>
        /// <returns></returns>
        [WebMethod]
        public string CancelTop(string oid)
        {
            Articles art = new Articles();
            art.RetrieveAll();
            Article ar = (Article)art.Filter("OID", oid);
            if (ar != null)
            {
                ar.SetTop = DateTime.MinValue;
                return "true";
            }
            return "false";
        }
        #endregion
        #region 2.我的新闻
        [WebMethod]
        public string GetMyNews(string no)
        {
            Articles arts = new Articles();
            arts.RetrieveByAttr("FK_UserNo", no);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(arts);
        }
        #endregion
    }
}
