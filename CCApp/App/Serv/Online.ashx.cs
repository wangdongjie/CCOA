﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCOA.Serv
{
    /// <summary>
    /// online 的摘要说明
    /// </summary>
    public class Online : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {            
            //更新在线状态
            context.Response.ContentType = "text/plain";
            #region //此处被改动，由于效率不高可删除
            //WX.CurUser cu = WX.Main.NewCurUser(); //new WX.CurUser(WX.Authentication.GetUserName());            
            //cu.LoadOnlineUser();
            //cu.OnlineUser.LastUpdateTime.set_DateTime_Now();
            //int no =cu.OnlineUser.Update();
            #endregion
            //登出时退出
            if (!BP.OA.Auth.IsOnline())
            {
                context.Response.Write("LOGIN_OUT");
                return;
            }
            //测试是否连接数据库
            string userName=BP.Web.WebUser.No;
            
            BP.OA.Main.CheckUser(userName);

            int countOfOnlineUsers = BP.OA.Main.OnlineCount;
            //在线人数统计
            context.Response.Write("<a onclick=addTab('在线用户','/Main/Sys/OnLineUsers.aspx','icon-home') href='#'>在线用户：<strong>" + countOfOnlineUsers + "</strong> 人</a>");
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}