﻿<%@ Page Title="CCOA数据库修复与安装" Language="C#" MasterPageFile="WinOpen.master" AutoEventWireup="true" Inherits="WF_Admin_DBInstall_CCOA" Codebehind="DBInstall.aspx.cs" %>
<%@ Register src="Pub.ascx" tagname="Pub" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<center>
<table  style="text-align:left;width:600px" >

<tr>
<tr>
<td> <img src='/DataUser/ICON/ccoa.png'  width="300px" alt="icon" style=" float:left" />  <div style=" float:right "><h2>数据库修复与安装.</h2></div></td>
</tr>
<td>
    <uc1:Pub ID="Pub1" runat="server" />
    </td>
    </tr>

    <tr>
    <td>

    <fieldset>
    <legend>安装说明：  </legend>

    <ul>
    <li>1，因为ccoa依赖于ccgpm才能运行，所以此安装包含了ccgpm。</li>
    <li>2，我们把ccflow集成到了ccoa中，所以此安装包含了ccflow。</li>
    <li>3，使用ccoa必须遵守ccoa的GPL开源协议。</li>
    <li>4，<font color=red> 如果安装失败,请删除数据库重新安装.</font></li>
    <li>5，admin的默认密码为 123 , 人员表为SELECT * FROM Port_Emp</li>
    </ul>

    </fieldset>

    <fieldset>
    <legend>测试用户</legend>
    <table>
  <tr>
  <td> 登录帐号</td>
  <td> 密码</td>
  <td> 名称</td>
  <td> 部门</td>
  <td> 岗位</td>
  </tr>

  <tr>
  <td> zhoupeng</td>
  <td> 123</td>
  <td> 周朋</td>
  <td> 总部</td>
  <td> 总经理</td>
  </tr>

   <tr>
  <td> zhanghaicheng</td>
  <td> 123</td>
  <td> 张海成</td>
  <td> 市场部</td>
  <td> 市场部经理</td>
  </tr>

  <tr>
  <td> zhangyifan</td>
  <td> 123</td>
  <td> 张一帆</td>
  <td> 市场部</td>
  <td> 销售人员岗</td>
  </tr>

   <tr>
  <td> zhoushengyu</td>
  <td> 123</td>
  <td> 周升雨</td>
  <td> 市场部</td>
  <td> 销售人员岗</td>
  </tr>


   <tr>
  <td> qifenglin</td>
  <td> 123</td>
  <td> 祁凤林</td>
  <td> 研发部</td>
  <td> 研发部经理</td>
  </tr>

   <tr>
  <td> zhoutianjiao</td>
  <td> 123</td>
  <td> 周天娇</td>
  <td> 研发部</td>
  <td> 程序员岗</td>
  </tr>

   <tr>
  <td> guoxiangbin</td>
  <td> 123</td>
  <td> 郭祥斌</td>
  <td> 服务部</td>
  <td> 客服部经理</td>
  </tr>

  <tr>
  <td> fuhui</td>
  <td> 123</td>
  <td> 福惠</td>
  <td> 服务部</td>
  <td> 技术服务岗</td>
  </tr>

  <tr>
  <td> yangyilei</td>
  <td> 123</td>
  <td> 杨依雷</td>
  <td> 财务部</td>
  <td> 财务部经理</td>
  </tr>

   <tr>
  <td> guobaogeng</td>
  <td> 123</td>
  <td> 郭宝庚</td>
  <td> 财务部</td>
  <td> 出纳岗</td>
  </tr>
  
   <tr>
  <td> liping</td>
  <td> 123</td>
  <td> 李萍</td>
  <td> 人力资源部</td>
  <td> 人力资源部经理</td>
  </tr>

   <tr>
  <td> liyan</td>
  <td> 123</td>
  <td> 李言</td>
  <td> 人力资源部</td>
  <td> 人力资源助理岗</td>
  </tr>
  </table>
    </fieldset>
    
  
    </td>
    </tr>
    </table>
    </div>
    </center>
</asp:Content>

