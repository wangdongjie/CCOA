﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using BP.OA.PrivPlan;
using BP.DA;
using System.Data.SqlClient;
namespace CCOA.App.PrivPlan
{
    public partial class CheckMonthView : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        //private DateTime rDate=DateTime.Now;
        private DateTime rDate
        {
            get
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
            set { }
        }
        private string ddlDepsele
        {
            get
            {
                string ddlDepsele = Convert.ToString(Request.QueryString["ddlDepsele"]);

                return ddlDepsele;
            }
        }
        private string ddlEmsele
        {
            get
            {
                string ddlDepsele = Convert.ToString(Request.QueryString["ddlEmsele"]);

                return ddlDepsele;
            }
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (string.IsNullOrEmpty(rDate.ToString()))
                {
                    this.rDate = DateTime.Now;
                }
                this.lblDate.Text = String.Format("&nbsp;&nbsp;{0:yyyy年MM月}", this.rDate);
                this.lbThisMonth.ForeColor = String.Format("{0:yyyy-MM}", this.rDate) == String.Format("{0:yyyy-MM}", DateTime.Now) ? System.Drawing.Color.Red : System.Drawing.Color.FromArgb(5, 5, 5);

                this.ddlDeptData();
                if (!string.IsNullOrEmpty(ddlDept.SelectedItem.Text.ToString()))
                {
                    ddlEmpMethod();
                }
                if (ddlEmp.SelectedItem != null)
                {
                    LoadData(ddlEmp.SelectedItem.Value.ToString());
                }
            }
        }
        //部门绑定
        private void ddlDeptData()
        {
            string sql = "SELECT  No,Name FROM Port_Dept";
            DataTable sqldt = DBAccess.RunSQLReturnTable(sql);
            ddlDept.DataSource = sqldt;
            ddlDept.DataTextField = "Name";
            ddlDept.DataValueField = "No";
            ddlDept.DataBind();//数据绑定
        }
        private void LoadData(string name)
        {
            if (string.IsNullOrEmpty(rDate.ToString()))
            {
                this.rDate = DateTime.Now;
            }

            DataTable dtPrivPlan = BP.DA.DBAccess.RunSQLReturnTable(String.Format("select OID,PlanDate,Checked from OA_PrivPlan where PlanDate like '{0:yyyy-MM}%' and FK_UserNo='{1}'", this.rDate, name));
            DataTable dtPrivPlanItem = BP.DA.DBAccess.RunSQLReturnTable(String.Format("select Succeed,Task,Reason,Improve,FK_PrivPlan,Sort from OA_PrivPlanItem where FK_PrivPlan in (select OID from OA_PrivPlan where PlanDate like '{0:yyyy-MM}%' and FK_UserNo='{1}') order by Sort", this.rDate, name));
            dtPrivPlanItem.Columns.Add("SucceedTitle");
            foreach (DataRow dr in dtPrivPlanItem.Rows)
            {
                dr["SucceedTitle"] = Convert.ToInt32(dr["Succeed"]) == 1 ? "成功" : "<span style='color:red;'>失败</span>";
            }
            DataTable dt = new DataTable();
            dt.Columns.Add("Day");
            dt.Columns.Add("Tasks");
            dt.Columns.Add("Reasons");
            dt.Columns.Add("Improves");
            DateTime dStart = DateTime.Parse(String.Format("{0:yyyy-MM-01}", this.rDate));
            DateTime dEnd = dStart.AddMonths(1).AddDays(-1);
            for (DateTime d = dStart; (d - dEnd).Days <= 0; d = d.AddDays(1))
            {
                DataRow dr = dt.Rows.Add();
                dr["Day"] = d;

                //string s_d = String.Format("{0:yyyy-MM-dd}", d); 
                //get drPP
                DataRow[] drsPP = dtPrivPlan.Select(String.Format("PlanDate='{0:yyyy-MM-dd}'", d));
                if (drsPP.Length == 0) continue;
                DataRow drPP = drsPP[0];
                //get drsPPI
                DataRow[] drsPPI = dtPrivPlanItem.Select(String.Format("FK_PrivPlan='{0}'", drPP["OID"]));
                //get Tasks
                dr["Tasks"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI, "（{2}）{0}.{1}", "<br/>", "Sort,Task,SucceedTitle");
                dr["Reasons"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI, "（{2}）{0}.{1}", "<br/>", "Sort,Reason,SucceedTitle");
                dr["Improves"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI, "（{2}）{0}.{1}", "<br/>", "Sort,Improve,SucceedTitle");
            }
            this.rptMonth.DataSource = dt;
            this.rptMonth.DataBind();
        }


        #endregion

        #region //3.页面事件(Page Event)
        protected void lbReturn_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate));
        }
        #endregion

        protected void lbPreviousMonth_Click(object sender, EventArgs e)
        {
            //this.rDate = this.rDate.AddMonths(-1);
            //LoadData(ddlEmp.SelectedItem.Value.ToString());

            this.Response.Redirect(String.Format("CheckMonthView.aspx?Date={0:yyyy-MM-dd}", this.rDate.AddMonths(-1)), true);

        }

        protected void lbNextMonth_Click(object sender, EventArgs e)
        {
            //this.rDate = this.rDate.AddMonths(1);
            //LoadData(ddlEmp.SelectedItem.Value.ToString());
            //isRe = false;
            this.Response.Redirect(String.Format("CheckMonthView.aspx?Date={0:yyyy-MM-dd}", this.rDate.AddMonths(1)), true);
        }

        protected void lbThisMonth_Click(object sender, EventArgs e)
        {
        //    this.rDate = DateTime.Now;
        //    LoadData(ddlEmp.SelectedItem.Value.ToString());
            //isRe = false;
            //string ddlEmsele = ddlEmp.SelectedItem.Text.ToString();
            //string ddlDepsele = ddlDept.SelectedItem.Text.ToString();
            this.Response.Redirect(String.Format("CheckMonthView.aspx?Date={0:yyyy-MM-dd}&ddlDepsele={1}&ddlEmsele={2}", DateTime.Now, ddlDepsele, ddlEmsele), true);
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlEmpMethod();
        }
        //人员绑定
        private void ddlEmpMethod()
        {
            string no = ddlDept.SelectedItem.Value.ToString();

            string sql = string.Format("SELECT No,Name,FK_Dept FROM Port_Emp where FK_Dept='{0}'", no);
            DataTable sqldt = DBAccess.RunSQLReturnTable(sql);

            ddlEmp.DataSource = sqldt;
            ddlEmp.DataTextField = "Name";
            ddlEmp.DataValueField = "No";
            ddlEmp.DataBind();//数据绑定
        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            this.rDate = DateTime.Now;
            if (ddlEmp.SelectedItem != null)
            {
                LoadData(ddlEmp.SelectedItem.Value.ToString());
            }
        }

    }
}