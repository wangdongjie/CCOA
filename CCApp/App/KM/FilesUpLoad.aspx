﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilesUpLoad.aspx.cs" Inherits="CCOA.App.KM.FilesUpLoad" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../../Js/jquery.uploadify/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../../Js/jquery.uploadify/swfobject.js" type="text/javascript"></script>
    <link href="../../Js/jquery.uploadify/uploadify.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery.uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var FolderID = getArgsFromHref("FolderID");
            $("#uploadify").uploadify({
                'uploader': '../../Js/jquery.uploadify/uploadify.swf',    // 做上传的Flash插件
                'script': 'FilesUpLoadServer.ashx?UploadType=upload',   // 服务器处理页面（支持多种语言，例如您可以修改成PHP、ASP、JSP等语言）
                'scriptData': { 'FolderID': FolderID },
                'cancelImg': '../../Js/jquery.uploadify/cancel.gif',  // 关闭按钮的图片地址
                'folder': 'Files', // 保存文件的文件夹
                'queueID': 'fileQueue',
                'fileDesc': '请选择你电脑里格式为*.rar;*.zip;*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.jpg;*.gif的文件',  // 描述（必须和fileExt一起使用）
                'fileExt': '*.rar;*.zip;*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.jpg;*.gif;*.png',   // 允许浏览上传的文件扩展名（必须和fileDesc一起使用）
                'sizeLimit': 104857600, // 文件大小限制100M（注意，在ASP.NET中Web.Config也要配置）
                'auto': false,
                'multi': true,  // 是否支持多文件上传
                'buttonText': 'file',    // 按钮上的文本
                'onError': function (a, b, c, d) {
                    if (d.status == 404)
                        alert('Could not find upload script.');
                    else if (d.type === "HTTP")
                        alert('error ' + d.type + ": " + d.status);
                    else if (d.type === "File Size")
                        alert("文件：" + c.name + ' ' + ' 已超出文件大小限制！');
                    else
                        alert('error ' + d.type + ": " + d.info);
                },
                'onComplete': function (a, b, c, d, e) {    // 完成一个上传后执行
                    $("#div_Msg").addClass("div_Msg").text("剩余 " + e.fileCount + " 个文件正在上传 . . .");
                },
                'onAllComplete': function (a, b) {   // 完成所有上传后执行
                    $("#div_Msg").addClass("div_Msg").text("恭喜您 , 所选的 " + b.filesUploaded + " 个文件已成功上传 ! ");
                },
                'onSelectOnce': function (a, b) {   // 浏览一次本机文件后执行
                    $("#div_Msg").addClass("div_Msg").text("据统计：总共 " + b.fileCount + " 个可上传文件 ! ");
                },
                'onCancel': function (a, b, c, d) { // 取消一个将要上传的文件后执行
                    $("#div_Msg").addClass("div_Msg").text("据统计：总共 " + d.fileCount + " 个可上传文件 ! ");
                }
            });
        });

        //sArgName表示要获取哪个参数的值
        function getArgsFromHref(sArgName) {
            var sHref = window.location.href;
            var args = sHref.split("?");
            var retval = "";
            if (args[0] == sHref) /*参数为空*/
            {
                return retval; /*无需做任何处理*/
            }
            var str = args[1];
            args = str.split("&");
            for (var i = 0; i < args.length; i++) {
                str = args[i];
                var arg = str.split("=");
                if (arg.length <= 1) continue;
                if (arg[0] == sArgName) retval = arg[1];
            }
            return retval;
        }
    </script>
</head>
<body>
    <div class="div_FilesBox">
        <div class="div_Handler">
            <a href="javascript:$('#uploadify').uploadifyUpload()">开始上传</a> <a href="javascript:$('#uploadify').uploadifyClearQueue()">
                全部取消</a>
            <input type="file" name="uploadify" id="uploadify" />
        </div>
        <div id="fileQueue">
        </div>
        <div id="div_Msg">
        </div>
    </div>
</body>
</html>
