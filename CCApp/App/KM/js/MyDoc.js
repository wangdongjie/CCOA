﻿var _CurentTreeNo = "128";
var pathLevel = 0;
var pathArrary = new Array();

//弹出上传文件窗口
function ShowUploadDialog() {
    $("<div id='uploadFileDialog'></div>").append($("<iframe id='frmUpload' width='100%' height='98%' frameborder='0' src='FilesUpLoad.aspx?FolderID=" + _CurentTreeNo + "'/>")).dialog({
        title: "添加文件",
        width: 600,
        height: 500,
        closed: false,
        modal: true,
        iconCls: 'icon-save',
        resizable: true,
        onClose: function () {
            LoadMyDocGrid();
            $("#uploadFileDialog").remove();
        },
        buttons: [{
            text: '关闭',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#uploadFileDialog').dialog("close");
            }
        }]
    });
}

//新建文档
function updateAddMyDocAction(dowhat) {
    var dialogTitle = "";
    $("#TB_FileName").val('');
    $("#TB_KeyWord").val('');
    $("#TB_Doc").val('');
    //$("#TB_FileName").removeAttr("disabled");
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        //文件夹修改
        if (row.DocType == 0) {
            AddFolder("edit");
            return;
        }

        //文件修改
        dialogTitle = "修改文档";

        $("#TB_FileName").val(row.Title);
        $("#TB_KeyWord").val(row.KeyWords);
        var fileSizeVal = row.FileSize;
        var fileSizeText = "0";

        if (fileSizeVal) {
            fileSizeText = fileSizeVal + "KB";
            if (fileSizeVal > 1024) {
                fileSizeVal = fileSizeVal / 1024;
                fileSizeText = fileSizeVal + "M";
            }
        }
        $("#TB_FileSize").val(fileSizeText);
        $("#TB_Doc").val(row.Doc);

        $("#TB_FileName").attr("disabled", "disabled");
        $("#TB_FileSize").attr("disabled", "disabled");

    }
    else {
        $.messager.alert('提示', '请选择记录后再进行修改！', 'info');
        return;
    }

    //弹出窗体
    $('#newDocDialog').dialog({
        title: dialogTitle,
        width: 600,
        height: 400,
        closed: false,
        modal: true,
        iconCls: 'icon-save',
        resizable: true,
        onClose: function () {

        },
        buttons: [{
            text: '确定',
            iconCls: 'icon-ok',
            handler: function () {
                var fileName = $("#TB_FileName").val();
                var keyWord = $("#TB_KeyWord").val();
                var doc = $("#TB_Doc").val();
                if (fileName == "") {
                    $.messager.alert('提示', '文件名不允许为空！', 'info');
                    $("#TB_FileName").focus();
                    return;
                }
                if (dialogTitle == "新建文档") {
                    OA.data.addMyDoc(fileName, keyWord, doc, _CurentTreeNo, function (js) {
                        if (js == "true") {
                            $("#TB_FileName").val('');
                            $("#TB_KeyWord").val('');
                            $("#TB_Doc").val('');
                            LoadMyDocGrid();
                            $('#newDocDialog').dialog("close");
                        } else {
                            $.messager.alert('提示', '新建失败，请检查！', 'info');
                        }
                    }, this);
                }
                else {
                    //修改文档
                    var row = $('#docGrid').datagrid('getSelected');
                    OA.data.editMyDoc(row.MyPK, keyWord, doc, function (js) {
                        if (js == "true") {
                            $("#TB_FileName").val('');
                            $("#TB_KeyWord").val('');
                            $("#TB_Doc").val('');
                            LoadMyDocGrid();
                            $('#newDocDialog').dialog("close");
                        } else {
                            $.messager.alert('提示', '修改失败，请检查！', 'info');
                        }
                    }, this);
                }
            }
        }, {
            text: '取消',
            iconCls: 'icon-cancel',
            handler: function () {
                $("#TB_FileName").val('');
                $("#TB_KeyWord").val('');
                $("#TB_Doc").val('');
                $('#newDocDialog').dialog("close");
            }
        }]
    });
}

//新增目录
function AddFolder(doWhate) {
    if (doWhate && doWhate == "edit") {
        var row = $('#docGrid').datagrid('getSelected');
        $("#sortName").val(row.Title);
    }
    //$("#sortName").val(node.text);
    //弹出窗体
    $('#treeSortDialog').dialog({
        title: "目录名称",
        width: 300,
        height: 130,
        modal: true,
        resizable: true,
        buttons: [{
            text: '确定',
            iconCls: 'icon-ok',
            handler: function () {
                var sortName = $("#sortName").val();
                if (sortName == "") {
                    CC.Message.showError("提示", "目录名称不能为空。");
                    return;
                }
                //修改目录名称
                if (doWhate && doWhate == "edit") {
                    var row = $('#docGrid').datagrid('getSelected');
                    //修改目录名称
                    OA.data.updateSortName(row.MyPK, sortName, function (js) {
                        if (js == "true") {
                            $("#sortName").val("");
                            $('#treeSortDialog').dialog('close');
                            LoadMyDocGrid();
                        }
                    }, this);
                    return;
                }
                //新增目录
                OA.data.addFolder(_CurentTreeNo, sortName, function (js) {
                    if (js == "true") {
                        $("#sortName").val("");
                        $('#treeSortDialog').dialog('close');
                        LoadMyDocGrid();
                    }
                }, this);
            }
        }, {
            text: '取消',
            handler: function () {
                $("#sortName").val("");
                $('#treeSortDialog').dialog('close');
            }
        }]
    });
}
//删除目录或文件
function DelDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        $.messager.confirm('提示', '确定删除所选项?', function (r) {
            if (r) {
                //删除文件夹
                if (row.DocType == "0") {
                    $.messager.confirm('提示', '当前选择为文件夹，将删除文件夹及子项，确定执行?', function (r) {
                        if (r) {
                            OA.data.delDoc(row.MyPK, "true", function (js) {
                                if (js == "true") {
                                    LoadMyDocGrid();
                                } else {
                                    $.messager.alert('提示', '删除失败，没有权限删除！', 'info');
                                }
                            }, this);
                        }
                    });
                }
                else {
                    OA.data.delDoc(row.MyPK, "false", function (js) {
                        if (js == "true") {
                            LoadMyDocGrid();
                        } else {
                            $.messager.alert('提示', '删除失败，没有权限删除！', 'info');
                        }
                    }, this);
                }
            }
        });
    } else {
        $.messager.alert('提示', '请选择记录后再进行删除！', 'info');
    }
}

//下载文档
function DownLoadMyDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        if (row.DocType == 0 || row.DocType == "undefined") {
            $.messager.alert('提示', '不支持文件夹下载！', 'info');
            return;
        }

        window.open("FilesUpLoadServer.ashx?UploadType=download&MyPK=" + row.MyPK);
    } else {
        $.messager.alert('提示', '请选择记录后再进行下载！', 'info');
    }
}

//文档属性
function ViewFileInfo() {
    var dialogTitle = "";
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        //文件修改
        dialogTitle = "文档属性";

        $("#TB_FileName").val(row.Title);
        $("#TB_KeyWord").val(row.KeyWords);
        $("#TB_Doc").val(row.Doc);
        var fileSizeVal = row.FileSize;
        var fileSizeText = "0";

        if (fileSizeVal) {
            fileSizeText = fileSizeVal + "KB";
            if (fileSizeVal > 1024) {
                fileSizeVal = fileSizeVal / 1024;
                fileSizeText = fileSizeVal + "M";
            }
        }
        $("#TB_FileSize").val(fileSizeText);

        $("#TB_FileName").attr("readonly", "true");
        $("#TB_KeyWord").attr("disabled", "disabled");
        $("#TB_FileSize").attr("disabled", "disabled");
        $("#TB_Doc").attr("disabled", "disabled");
    }
    else {
        $.messager.alert('提示', '请选择记录后再进行查看！', 'info');
        return;
    }
    //修改阅读次数
    OA.data.ReadTimeEdit(row.MyPK, function (js) {
        //弹出窗体
        $('#newDocDialog').dialog({
            title: dialogTitle,
            width: 600,
            height: 500,
            closed: false,
            modal: true,
            iconCls: 'icon-save',
            resizable: true,
            onClose: function () {
                $("#TB_FileName").removeAttr("disabled");
                $("#TB_KeyWord").removeAttr("disabled");
                $("#TB_FileSize").removeAttr("disabled");
                $("#TB_Doc").removeAttr("disabled");
            },
            buttons: [{
                text: '确定',
                iconCls: 'icon-ok',
                handler: function () {
                    $("#TB_FileName").val('');
                    $("#TB_KeyWord").val('');
                    $("#TB_Doc").val('');
                    $("#TB_FileSize").val('');
                    $('#newDocDialog').dialog("close");
                }
            }]
        });
    }, this);
}

//共享文件
function ShareMyDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        OA.data.getShareInfo(row.MyPK, function (js) {
            var shareData = eval('(' + js + ')');

            $("#CB_IsShare").removeAttr("checked");
            $("#CB_AllEmps").attr("checked", "checked");
            //是否共享
            if (shareData.IsShare == "1") {
                $("#CB_IsShare").attr("checked", "checked");
            }
            //共享方式
            if (shareData.IsShare == "1" && shareData.ShareType != "1") {
                $("#CB_AllEmps").removeAttr("checked");
                $("#allowPersons").removeAttr("disabled");
                $("#selectedPersons").removeAttr("disabled");
            }
            else {
                $("#allowPersons").attr("disabled", "disabled");
                $("#selectedPersons").attr("disabled", "disabled");
            }
            $("#allowPersonVals").val(shareData.SharePerson.val);
            $("#allowPersons").val(shareData.SharePerson.text);

            //弹出窗体
            $('#shareDialog').dialog({
                title: "共享",
                width: 500,
                height: 360,
                closed: false,
                modal: true,
                iconCls: 'icon-save',
                resizable: true,
                onClose: function () {
                    $("#CB_IsShare").removeAttr("checked");
                    $("#CB_AllEmps").attr("checked", "checked");

                    $("#allowPersonVals").val("");
                    $("#allowPersons").val("");

                    //设置为不可用
                    $("#allowPersons").attr("disabled", "disabled");
                    $("#selectedPersons").attr("disabled", "disabled");
                },
                buttons: [{
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        var shareType = row.DocType; //0文件夹
                        var isShare = $("#CB_IsShare").attr("checked");
                        var isAllEmps = $("#CB_AllEmps").attr("checked");
                        var sharePersons = $("#allowPersonVals").val();
                        //共享
                        if (isShare == "checked") {
                            if (isAllEmps != "checked" && sharePersons == "") {
                                $.messager.alert('提示', '当前选择不是所有人员，请指定人员！', 'info');
                                return;
                            }
                        }
                        //执行共享
                        OA.data.shareFileManage(shareType, isShare, isAllEmps, sharePersons, row.MyPK, function (js) {
                            if (js == "true") {
                                $('#shareDialog').dialog("close");
                                LoadMyDocGrid();
                            } else {
                                $.messager.alert('提示', '共享失败，请联系管理员！', 'info');
                            }
                        }, this);
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $('#shareDialog').dialog("close");
                    }
                }]
            });
        }, this);
    } else {
        $.messager.alert('提示', '请选择记录后再进行操作！', 'info');
        return;
    }
}

//点击路径进行回退
function FolderPathClick(TreeNo, level) {
    var html = "";
    var newArrry = new Array();
    for (var i = 0; i <= level; i++) {
        html += pathArrary[i];
        newArrry.push(pathArrary[i]);
    }
    $("#folderPath").html(html);
    pathArrary = newArrry;
    pathLevel = level;
    _CurentTreeNo = TreeNo;

    LoadMyDocGrid();
}

//加载我的文档datagrid
function LoadMyDocGrid() {
    var searchContent = $("#TB_Search").val();
    //工具栏
    var toolbar = [
            { 'text': '新建目录', 'iconCls': 'icon-addfolder', 'handler': 'AddFolder' },
            { 'text': '添加文件', 'iconCls': 'icon-addfile', 'handler': 'ShowUploadDialog' },
            '-',
            { 'text': '编辑', 'iconCls': 'icon-edit', 'handler': 'updateAddMyDocAction' },
            { 'text': '删除', 'iconCls': 'icon-delete', 'handler': 'DelDoc' },
             '-',
             { 'text': '共享', 'iconCls': 'icon-sharedir', 'handler': 'ShareMyDoc' },
             '-',
             { 'text': '属性', 'iconCls': 'icon-save-close', 'handler': 'ViewFileInfo' },
            { 'text': '下载', 'iconCls': 'icon-down', 'handler': 'DownLoadMyDoc' }
            ];
    //查询数据
    OA.data.getMyDoc(_CurentTreeNo, searchContent, function (js, scope) {
        if (js) {
            if (js.status == "500") return;
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#docGrid').datagrid({
                data: pushData,
                width: 'auto',
                toolbar: toolbar,
                striped: true,
                rownumbers: true,
                singleSelect: true,
                loadMsg: '数据加载中......',
                columns: [[{ field: 'Title', title: '文件名', width: 260, formatter: function (value, rec) {
                    var url = "<img align='middle' alt='' src='../../Js/Js_EasyUI/themes/icons/tree_folder.gif'/>" + value;
                    if (rec.DocType == 1) {
                        url = "<img align='middle' alt='' src='../../Images/FileType/" + rec.FileExt + ".gif'/>" + value;
                    }
                    return url;
                }
                },
                { field: 'FK_EmpText', title: '发布人', width: 60, align: 'left' },
                { field: 'FK_DeptText', title: '部门', width: 90, align: 'left' },
                { field: 'RDT', title: '创建日期', width: 120, align: 'left' },
                { field: 'EDTER', title: '修改人', width: 60 },
                { field: 'EDT', title: '修改日期', width: 120 },
                { field: 'FileStatusText', title: '共享', width: 60, formatter: function (value, rec) {
                    var url = "";
                    if (rec.IsShare == 1) {
                        url = "<img align='middle' alt='' src='../../Js/Js_EasyUI/themes/icons/sharedir.gif'/>";
                    }
                    return url;
                }
                },
                { field: 'ReadTimes', title: '阅读次数', width: 90 },
                { field: 'DownLoadTimes', title: '下载次数', width: 90 }
                ]],
                onDblClickRow: function (rowIndex, rowData) {
                    if (rowData.DocType == 0) {
                        _CurentTreeNo = rowData.MyPK;
                        var html = $("#folderPath").html();
                        pathLevel++;
                        var appendHtml = "<img align='middle' alt='' src='../../Images/Btn/Next.gif'/>"
                        appendHtml += "<a href='#' onclick=\"FolderPathClick('" + _CurentTreeNo + "','" + pathLevel + "')\">" + rowData.Title + "</a>";
                        //路径集合
                        pathArrary.push(appendHtml);
                        html += appendHtml;
                        $("#folderPath").html(html);
                        LoadMyDocGrid();
                    }
                    else {
                        ViewFileInfo();
                    }
                }
            });
        }
    }, this);
}

//搜索
function MyDocSearch() {
    LoadMyDocGrid();
}
//初始化
$(function () {
    //路径集合
    pathArrary.push("<a href='#' onclick=\"FolderPathClick('" + _CurentTreeNo + "','0')\">我的文档</a>");
    LoadMyDocGrid();

    //设置为不可用
    $("#allowPersons").attr("disabled", "disabled");
    $("#selectedPersons").attr("disabled", "disabled");

    //是否共享给全体
    $("#CB_AllEmps").click(
            function () {
                if ($("#CB_AllEmps").attr("checked") != "checked") {
                    $("#allowPersons").removeAttr("disabled");
                    $("#selectedPersons").removeAttr("disabled");
                }
                else {
                    $("#allowPersons").attr("disabled", "disabled");
                    $("#selectedPersons").attr("disabled", "disabled");
                }
            });
});