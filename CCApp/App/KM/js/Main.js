﻿var _CurentTreeNo = "99";
var pathLevel = 0;
var pathArrary = new Array();

//弹出上传文件窗口
function ShowUploadDialog() {
    $("<div id='uploadFileDialog'></div>").append($("<iframe id='frmUpload' width='100%' height='98%' frameborder='0' src='FilesUpLoad.aspx?FolderID=" + _CurentTreeNo + "'/>")).dialog({
        title: "添加文件",
        width: 600,
        height: 500,
        closed: false,
        modal: true,
        iconCls: 'icon-save',
        resizable: true,
        onClose: function () {
            LoadCompanyDocGrid();
            $("#uploadFileDialog").remove();
        },
        buttons: [{
            text: '关闭',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#uploadFileDialog').dialog("close");
            }
        }]
    });
}

//修改文档属性
function updateAddDocAction(dowhat) {
    var dialogTitle = "";
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        if (row.DocType == 0) {
            $.messager.alert('提示', '请选择文件！', 'info');
            return; 
        }
        //文件修改
        dialogTitle = "修改文档属性";

        $("#TB_FileName").val(row.Title);
        $("#TB_KeyWord").val(row.KeyWords);
        var fileSizeVal = row.FileSize;
        var fileSizeText = "0";

        if (fileSizeVal) {
            fileSizeText = fileSizeVal + "KB";
            if (fileSizeVal > 1024) {
                fileSizeVal = fileSizeVal / 1024;
                fileSizeText = fileSizeVal + "M";
            }
        }
        $("#TB_FileSize").val(fileSizeText);
        $("#TB_Doc").val(row.Doc);

        $("#TB_FileName").attr("disabled", "disabled");
        $("#TB_FileSize").attr("disabled", "disabled");

    }
    else {
        $.messager.alert('提示', '请选择记录后再进行修改！', 'info');
        return;
    }
    //弹出窗体
    $('#newDocDialog').dialog({
        title: dialogTitle,
        width: 600,
        height: 400,
        closed: false,
        modal: true,
        iconCls: 'icon-save',
        resizable: true,
        onClose: function () {

        },
        buttons: [{
            text: '确定',
            iconCls: 'icon-ok',
            handler: function () {
                var fileName = $("#TB_FileName").val();
                var keyWord = $("#TB_KeyWord").val();
                var doc = $("#TB_Doc").val();
                if (fileName == "") {
                    $.messager.alert('提示', '文件名不允许为空！', 'info');
                    $("#TB_FileName").focus();
                    return;
                }
                //修改文档
                var row = $('#docGrid').datagrid('getSelected');
                OA.data.editMyDoc(row.MyPK, keyWord, doc, function (js) {
                    if (js == "true") {
                        $("#TB_FileName").val('');
                        $("#TB_KeyWord").val('');
                        $("#TB_Doc").val('');
                        LoadCompanyDocGrid();
                        $('#newDocDialog').dialog("close");
                    } else {
                        $.messager.alert('提示', '修改失败，请检查！', 'info');
                    }
                }, this);
            }
        }, {
            text: '取消',
            iconCls: 'icon-cancel',
            handler: function () {
                $("#TB_FileName").val('');
                $("#TB_KeyWord").val('');
                $("#TB_Doc").val('');
                $('#newDocDialog').dialog("close");
            }
        }]
    });
}

//设置权限及目录
function SetSortTree() {
    //CCOA添加Tab方法
    if (self.parent && self.parent.addTab) {
        self.parent.addTab("知识树", "../App/KM/Sort.aspx", "");
    }
    //精英添加Tab方法
    if (self.parent && self.parent.shmev1) {
        self.parent.shmev1.system.openMenuHref('知识管理', '/App/KM/Main.aspx', 'iframetabs')
    }
}

//删除目录或文件
function DelDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        $.messager.confirm('提示', '确定删除所选项?', function (r) {
            if (r) {
                //删除文件夹
                if (row.DocType == "0") {
                    $.messager.confirm('提示', '当前选择为文件夹，将删除文件夹及子项，确定执行?', function (r) {
                        if (r) {
                            OA.data.delDoc(row.MyPK, "true", function (js) {
                                if (js == "true") {
                                    LoadCompanyDocGrid();
                                } else {
                                    $.messager.alert('提示', '删除失败，没有权限删除！', 'info');
                                }
                            }, this);
                        }
                    });
                }
                else {
                    OA.data.delDoc(row.MyPK, "false", function (js) {
                        if (js == "true") {
                            LoadCompanyDocGrid();
                        } else {
                            $.messager.alert('提示', '删除失败，没有权限删除！', 'info');
                        }
                    }, this);
                }
            }
        });
    } else {
        $.messager.alert('提示', '请选择记录后再进行删除！', 'info');
    }
}

//下载文档
function DownLoadDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        if (row.DocType == 0 || row.DocType == "undefined") {
            $.messager.alert('提示', '不支持文件夹下载！', 'info');
            return;
        }

        window.open("FilesUpLoadServer.ashx?UploadType=download&MyPK=" + row.MyPK);
    } else {
        $.messager.alert('提示', '请选择记录后再进行下载！', 'info');
    }
}

//文档属性
function ViewFileInfo() {
    var dialogTitle = "";
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        //文件修改
        dialogTitle = "文档属性";

        $("#TB_FileName").val(row.Title);
        $("#TB_KeyWord").val(row.KeyWords);
        $("#TB_Doc").val(row.Doc);
        var fileSizeVal = row.FileSize;
        var fileSizeText = "0";

        if (fileSizeVal) {
            fileSizeText = fileSizeVal + "KB";
            if (fileSizeVal > 1024) {
                fileSizeVal = fileSizeVal / 1024;
                fileSizeText = fileSizeVal + "M";
            }
        }
        $("#TB_FileSize").val(fileSizeText);

        $("#TB_FileName").attr("disabled", "disabled");
        $("#TB_KeyWord").attr("disabled", "disabled");
        $("#TB_FileSize").attr("disabled", "disabled");
        $("#TB_Doc").attr("disabled", "disabled");
    }
    else {
        $.messager.alert('提示', '请选择记录后再进行查看！', 'info');
        return;
    }
    //修改阅读次数
    OA.data.ReadTimeEdit(row.MyPK, function (js) {
        //弹出窗体
        $('#newDocDialog').dialog({
            title: dialogTitle,
            width: 600,
            height: 500,
            closed: false,
            modal: true,
            iconCls: 'icon-save',
            resizable: true,
            onClose: function () {
                $("#TB_FileName").removeAttr("disabled");
                $("#TB_KeyWord").removeAttr("disabled");
                $("#TB_FileSize").removeAttr("disabled");
                $("#TB_Doc").removeAttr("disabled");
            },
            buttons: [{
                text: '确定',
                iconCls: 'icon-ok',
                handler: function () {
                    $("#TB_FileName").val('');
                    $("#TB_KeyWord").val('');
                    $("#TB_Doc").val('');
                    $("#TB_FileSize").val('');
                    $('#newDocDialog').dialog("close");
                }
            }]
        });
    }, this);
}

//点击路径进行回退
function FolderPathClick(TreeNo, level) {
    var html = "";
    var newArrry = new Array();
    for (var i = 0; i <= level; i++) {
        html += pathArrary[i];
        newArrry.push(pathArrary[i]);
    }
    $("#folderPath").html(html);
    pathArrary = newArrry;
    pathLevel = level;
    _CurentTreeNo = TreeNo;

    LoadCompanyDocGrid();
}

//加载知识管理datagrid
function LoadCompanyDocGrid() {
    var searchContent = $("#TB_Search").val();
    //工具栏
    var toolbar = [
            { 'text': '设置目录及权限', 'iconCls': 'icon-config', 'handler': 'SetSortTree' },
            { 'text': '添加文件', 'iconCls': 'icon-addfile', 'handler': 'ShowUploadDialog' },
            '-',
            { 'text': '编辑文件属性', 'iconCls': 'icon-edit', 'handler': 'updateAddDocAction' },
            { 'text': '删除', 'iconCls': 'icon-delete', 'handler': 'DelDoc' },
             '-',
             { 'text': '属性', 'iconCls': 'icon-save-close', 'handler': 'ViewFileInfo' },
            { 'text': '下载', 'iconCls': 'icon-down', 'handler': 'DownLoadDoc' }
            ];
    //查询数据
    OA.data.getCompanyDoc(_CurentTreeNo, searchContent, function (js, scope) {
        if (js) {
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#docGrid').datagrid({
                data: pushData,
                width: 'auto',
                toolbar: toolbar,
                striped: true,
                rownumbers: true,
                singleSelect: true,
                loadMsg: '数据加载中......',
                columns: [[{ field: 'Title', title: '文件名', width: 260, formatter: function (value, rec) {
                    var url = "<img align='middle' alt='' src='../../Js/Js_EasyUI/themes/icons/tree_folder.gif'/>" + value;
                    if (rec.DocType == 1) {
                        url = "<img align='middle' alt='' src='../../Images/FileType/" + rec.FileExt + ".gif'/>" + value;
                    }
                    return url;
                }
                },
                { field: 'FK_EmpText', title: '发布人', width: 100, align: 'left' },
                { field: 'FK_DeptText', title: '部门', width: 100, align: 'left' },
                { field: 'RDT', title: '创建日期', width: 120, align: 'left' },
                { field: 'EDTER', title: '修改人', width: 100 },
                { field: 'EDT', title: '修改日期', width: 120 },
                { field: 'ReadTimes', title: '阅读次数', width: 90 },
                { field: 'DownLoadTimes', title: '下载次数', width: 90 }
                ]],
                onDblClickRow: function (rowIndex, rowData) {
                    if (rowData.DocType == 0) {
                        _CurentTreeNo = rowData.MyPK;
                        var html = $("#folderPath").html();
                        pathLevel++;
                        var appendHtml = "<img align='middle' alt='' src='../../Images/Btn/Next.gif'/>"
                        appendHtml += "<a href='#' onclick=\"FolderPathClick('" + _CurentTreeNo + "','" + pathLevel + "')\">" + rowData.Title + "</a>";
                        //路径集合
                        pathArrary.push(appendHtml);
                        html += appendHtml;
                        $("#folderPath").html(html);
                        LoadCompanyDocGrid();
                    }
                    else {
                        ViewFileInfo();
                    }
                }
            });
        }
    }, this);
}

//搜索
function MyDocSearch() {
    LoadCompanyDocGrid();
}

//初始化
$(function () {
    //路径集合
    pathArrary.push("<a href='#' onclick=\"FolderPathClick('" + _CurentTreeNo + "','0')\">单位知识资料库</a>");
    LoadCompanyDocGrid();
});