﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="NoticeListMobile.aspx.cs" Inherits="CCOA.App.Notice.NoticeListMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-role="page" data-theme="e">
        <div data-role="header" data-position="fixed" data-theme="b" >
            <a href="/CCMobile/Home.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                公告列表</h2>
        </div>
        <div class="ui-content" data-role="main" style="padding: 0px;">
            <table style="width: 100%;">
                <asp:Repeater runat="server" ID="rptNotice">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <img src="img/Notice1.png" />
                            </td>
                            <td>
                                <a href="/App/Notice/NoticeBrowserMobile.aspx?OID=<%# Eval("No") %>">
                                    <%# Eval("Name") %></a>&nbsp;&nbsp;<%# Convert.ToInt32(Eval("ReadState")) == 0 ? "<img alt='新公告' src='../../Main/Img/email_new.gif'>" : ""%><br />
                                <font color="green" size="-3">
                                发布者：<%#Eval("UserName")%>
                                    | <span><%# String.Format(BP.DA.DataType.ParseSysDate2DateTimeFriendly((Eval("RDT")).ToString()))%></span>
                                </font>
                            </td>
                        </tr>
                        <separatortemplate>
                     <tr>
                     <td colspan="6"><hr /></td>
                     </tr>
                    </separatortemplate>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>
</asp:Content>
