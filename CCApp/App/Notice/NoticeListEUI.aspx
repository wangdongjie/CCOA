﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="NoticeListEUI.aspx.cs" Inherits="CCOA.App.Notice.NoticeListEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";

            var pushData = eval('(' + js + ')');
            $('#noteListGrid').datagrid({
                columns: [[
                    { field: "OID", title: "编号", width: 80, hidden: true },
                    { field: 'Title', title: '标题', width: 320, formatter: function (value, rec) {
                        var title = "<img src='../../Images/Notice/Import" + rec.ImportLevel + ".png' style='width: 18px; height: 18px;' align='middle' />&nbsp;";

                        title += "<a href='NoticeBrowser.aspx?OID=" + rec.OID + "'>" + rec.Title + "</a>";

                        var d1 = new Date();
                        var d2 = new Date(rec.RDT.replace(/-/g, "/"));
                        var h = parseInt((d1 - d2) / 3600000);
                        //小于2小时
                        if (h < 2) {
                            title += "&nbsp;&nbsp;<img src='img/news_New.gif' alt='最新'/>";
                        }
                        //置顶
                        if (rec.SetTop) {
                            title += "&nbsp;&nbsp;<img src='img/top.gif' alt='置顶'/>";
                        }
                        return title;
                    }
                    },
                    { field: 'UserNameText', title: '发布者', align: 'center', width: 80 },
                    { field: 'NoticeCategory', title: '分类', align: 'center', width: 80 },
                    { field: 'Importance', title: '重要程度', align: 'center', width: 80 },
                    { field: 'ReadState', title: '阅读状态', align: 'center', width: 80, formatter: function (value, rec) {
                        return "<a href=\"javascript:zDialog_open0('NoticeRead.aspx?OID=" + rec.OID + "','公告阅读情况',600,600,false);\">已读" + rec.ReadState + "</a>";
                    }
                    },
                    { field: 'RDT', title: '发布时间', align: 'center', width: 100 },
                    { field: 'PublishState', title: '发布状态', align: 'center', width: 80 },
                    { field: 'mg', title: '操作', align: 'center', width: 280, formatter: function (value, rec) {
                        var extTools = "<a href='NoticeEdit.aspx?OID=" + rec.OID + "'>编辑</a>";
                        extTools += "&nbsp;|&nbsp;<a href=\"javascript:ChangeState('" + rec.OID + "');\">" + rec.NoticeStaTitle + "</a>";
                        extTools += "&nbsp;|&nbsp;<a href=\"javascript:void(0)\"  onclick=\"delete_dg('" + rec.OID + "');\">删除</a>";
                        extTools += "&nbsp;|&nbsp;<a href=\"javascript:void(0)\"  onclick=\"over_top('" + rec.OID + "');\">置顶</a>";
                        extTools += "&nbsp;|&nbsp;<a href=\"javascript:void(0)\"  onclick=\"cancel_top('" + rec.OID + "');\">取消置顶</a>";
                        return extTools;
                    }
                    }
                ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                    var row = $('#noteListGrid').datagrid('getSelected');
                    if (row) {
                        if (window.parent.addTab && 1 == 2) {//先关闭
                            window.parent.addTab(row.Title, "../App/News/NewsEdit.aspx?ID=" + row.OID, "");
                        } else {
                            window.location.href = "NoticeBrowser.aspx?OID=" + row.OID;
                        }
                    }
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#noteListGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }

        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            var catagory = $("[id$=categoryId]").val();

            var params = {
                method: "query",
                pageNumber: this.pageNumber,
                pageSize: this.pageSize,
                noticeCatagory: catagory
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        //刷新
        function ReLoadGridData() {
            var grid = $('#noteListGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            LoadGridData(curPage, 20);
        }
        //修改公告状态
        function ChangeState(oid) {
            var params = {
                method: "changestate",
                OID: oid
            };
            queryData(params, function (js) {
                ReLoadGridData();
            }, this);
        }
        //删除
        function delete_dg(oid) {
            $.messager.confirm('提示', '是否确定要删除？', function (y) {
                if (y) {
                    var params = {
                        method: "del",
                        OID: oid
                    };
                    queryData(params, function (js) {
                        ReLoadGridData();
                    }, this);
                }
            });
        }
        //////////////////置顶//////////////////
        function over_top(oid) {
            var params = {
                method: "overtop",
                OID: oid
            };
            queryData(params, function (js) {
                ReLoadGridData();
            }, this);
        }
        //////////////////取消置顶//////////////////
        function cancel_top(oid) {
            var params = {
                method: "canceltop",
                OID: oid
            };
            queryData(params, function (js) {
                ReLoadGridData();
            }, this);
        }
        //初始化
        $(function () {
            $("#pageloading").show();
            LoadGridData(1, 20);
        });

        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "NoticeDataService.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
    <div id="pageloading">
    </div>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 6px; height: 28px;">
            请选择：<asp:RadioButtonList runat="server" ID="rbl_Category" AutoPostBack="true" RepeatLayout="Flow"
                RepeatDirection="Horizontal" OnSelectedIndexChanged="rbl_Category_SelectedIndexChanged">
            </asp:RadioButtonList>
        </div>
        <table id="noteListGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
        <input type="hidden" id="categoryId" name="categoryId" runat="server" value="0" />
    </div>
    </form>
</asp:Content>
