﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

namespace CCOA.App.Notice
{
    public partial class NoticeListEUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindCategory();
            }
        }
        protected void rbl_Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            categoryId.Value = this.rbl_Category.SelectedValue;
        }
        public void BindCategory()
        {
            DataTable dt = this.GetNoticeCategorys();
            BP.OA.UI.Dict.BindListCtrl(dt, this.rbl_Category, "Name", "No", null, "0#所有类型", "0");
        }
        private DataTable GetNoticeCategorys()
        {
            string sql = "select No,Name from OA_NoticeCategory order by No";
            return BP.DA.DBAccess.RunSQLReturnTable(sql);
        }
    }
}