﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;

namespace CCOA.App.DailyRecord
{
    public partial class MySheet : System.Web.UI.Page, IHttpHandler
    {
        #region 属性.
        public int PageIdx
        {
            get
            {
                try
                {
                    return int.Parse(this.Request.QueryString["PageIdx"]);
                }
                catch
                {
                    return 1;
                }
            }
        }
        #endregion 属性.


        protected void Page_Load(object sender, EventArgs e)
        {

           
                //变量定义: 第几页.
                int currPageIdx = this.PageIdx;
                int pageSize = 12; //页面记录条数.

                //实体查询.
                BP.OA.DRSheets ens = new BP.OA.DRSheets();

                //创建查询对象
                BP.En.QueryObject qo = new QueryObject(ens);

                qo.AddWhere(DRSheetAttr.Rec, BP.Web.WebUser.No);// 设置查询条件.


                qo.addOrderByDesc(DRSheetAttr.RDT);

                //把代码放到表格尾部, 形成 第1,2,3,4,5 页 ....... 
                this.Pub1.Clear();
                this.Pub1.BindPageIdx(qo.GetCount(),
                    pageSize, currPageIdx, "MySheet.aspx?1=2&3=xx");


                //每页有15条数据，取第2页的数据.
                qo.DoQuery("OID", pageSize, currPageIdx);

                DataTable dt = ens.ToDataTableField();
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Doc"] = BP.DA.DataType.ParseText2Html(dr["Doc"].ToString());
                }
                this.rep_List.DataSource = dt;
                this.rep_List.DataBind();

            }
            

        

        //获取评论数据源 
        public DataTable getdt(string id)
        {
            string Ssql = String.Format(" select * from  OA_DRComment   where  FK_COM='{0}'", id);
            DataTable dtcom = BP.DA.DBAccess.RunSQLReturnTable(Ssql);
            return dtcom;

        }




        //删除
        public string DeleteSheet(string id)
        {
            BP.OA.DRSheet en = new DRSheet();
            en.OID = int.Parse(id);
            en.Delete();
            return "";


        }

        //日期格式转换
        public static string GetWeekNameOfDay(DateTime idt)
        {
            string dt, week = "";
            dt = idt.DayOfWeek.ToString();
            switch (dt)
            {
                case "Monday":
                    week = "星期一";
                    break;
                case "Tuesday":
                    week = "星期二";
                    break;
                case "Wednesday":
                    week = "星期三";
                    break;
                case "Thursday":
                    week = "星期四";
                    break;
                case "Friday":
                    week = "星期五";
                    break;
                case "Saturday":
                    week = "星期六";
                    break;
                case "Sunday":
                    week = "星期日";
                    break;

            }
            return week;
        }

        //获取DRSta值
        public string GetDRSta( string  id)
        {
            string sSql = String.Format("select  DRSta from  OA_DRSheet where OID='{0}'",id);
            return BP.DA.DBAccess.RunSQLReturnString(sSql);
        }
      
        public void butDele_Click(object sender, EventArgs e)
            
        {
        
      
            //获取OID的值
            string id = "";
            LinkButton linkbutton = (LinkButton)sender;
           
           
            //判断HiddenField是否存在
            if (linkbutton.NamingContainer.FindControl("HiddenField1") != null)
            {
                //存在，把对象转换为HiddenField控件
                HiddenField hf = (HiddenField)linkbutton.NamingContainer.FindControl("HiddenField1");
                //取出HiddenField的Value值。
                id = hf.Value;
                BP.OA.DRSheet en = new DRSheet();
                en.OID = int.Parse(id);

                if (GetDRSta(id)!="1")
                {
                    en.Delete();
                }
                else
                {
                    Response.Write("<script>alert('共享后的日志不能删除！')</script>");
                }
           
                //string sSql = string.Format("delete from OA_DRSheet where OID='{0}'", id);
                //BP.DA.DBAccess.RunSQL(sSql); 

            }
            //变量定义: 第几页.
            int currPageIdx = this.PageIdx;
            int pageSize = 12; //页面记录条数.

            //实体查询.
            BP.OA.DRSheets ens = new BP.OA.DRSheets();

            //创建查询对象
            BP.En.QueryObject qo = new QueryObject(ens);

            qo.AddWhere(DRSheetAttr.Rec, BP.Web.WebUser.No);// 设置查询条件.


            qo.addOrderByDesc(DRSheetAttr.RDT);

            //把代码放到表格尾部, 形成 第1,2,3,4,5 页 ....... 
            this.Pub1.Clear();
            this.Pub1.BindPageIdx(qo.GetCount(),
                pageSize, currPageIdx, "MySheet.aspx?1=2&3=xx");


            //每页有15条数据，取第2页的数据.
            qo.DoQuery("OID", pageSize, currPageIdx);

            DataTable dt = ens.ToDataTableField();
            foreach (DataRow dr in dt.Rows)
            {
                dr["Doc"] = BP.DA.DataType.ParseText2Html(dr["Doc"].ToString());
            }
            this.rep_List.DataSource = dt;
            this.rep_List.DataBind();



        }
    }
}
