﻿using BP.OA.Vote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCOA.App.Vote
{
    /// <summary>
    /// vote 的摘要说明
    /// </summary>
    public class vote : IHttpHandler
    {
        HttpContext _Context = null;
        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            OA_VoteDetail detail = new OA_VoteDetail();
            detail.FK_VoteOID = int.Parse(context.Request["v"]);
            OA_Vote v = new OA_Vote(detail.FK_VoteOID);
            detail.IsAnony = v.IsAnonymous;
            detail.FK_Emp = BP.Web.WebUser.No;
            detail.VComment = context.Request["n"];
            detail.VoteItem = context.Request["p"]; ;
            detail.VoteDate = BP.OA.Main.CurDateTime;
            int i = detail.Insert();
            //if (i > 0)
            //{
            //    return i;
            //}
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(i);
            _Context.Response.End();
        }
        public void InsertVote()
        {

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}