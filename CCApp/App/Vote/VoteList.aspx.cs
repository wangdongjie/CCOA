﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.Web;

namespace CCOA.App.Vote
{
    public partial class VoteList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ChangeState();
                DataTable dt = BP.OA.Vote.OA_Vote.GetVotes();
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
        void ChangeState()
        {
            string str1 = Request.QueryString["m"];
            if (!string.IsNullOrEmpty(str1))
            {
                if (str1 == "change")
                {
                    string str2 = Request.QueryString["id"];
                    string str3 = Request.QueryString["state"];
                    if (!string.IsNullOrEmpty(str2) && !string.IsNullOrEmpty(str3))
                    {
                        int result = BP.OA.Vote.OA_Vote.ChangeState(str3, str2);
                    }
                }
                else if (str1 == "delete")
                {
                    string str2 = Request.QueryString["id"];
                    if (!string.IsNullOrEmpty(str2))
                    {
                        int result = BP.OA.Vote.OA_Vote.DeleteVote(str2);
                    }
                }
            }
        }
        public string Check(object o)
        {
            string strVal = Convert.ToString(o);
            string span1 = "<span style='color:Red'>{0}</span>";
            string span2 = "<span style='color:Green'>{0}</span>";
            if (!string.IsNullOrEmpty(strVal) && strVal == "1")
            {
                return string.Format(span2, "是");
            }
            return string.Format(span1, "否");
        }
        public string CheckText(object o)
        {
            return Convert.ToString(o) == "1" ? "停用" : "启用";
        }
        //统一时间格式 改
        public string DateFormat(object o)
        {
            if (o != null)
            {
                return (Convert.ToDateTime(o)).ToString("yyyy-MM-dd HH:mm:ss");
            }
            return string.Empty;
        }
        public string State(object o)
        {
            string result = "0";
            if (o != null && o != string.Empty)
            {
                return o.ToString();
            }
            return result;
        }
        public string CheckPermission(object obj)
        {
            string emp = Convert.ToString(obj);
            string user = WebUser.No.ToLower();
            if (user == "admin" || user == emp)
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }

    }
}