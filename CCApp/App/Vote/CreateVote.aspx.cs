﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.Vote;
using BP.Web;
using System.Data;

namespace CCOA.App.Vote
{
    public partial class CreateVote : System.Web.UI.Page
    {
        public string Today
        {
            get { return DateTime.Now.ToString(); }
        }
        public string UserNo
        {
            get { return WebUser.No; }
        }
        /// <summary>
        /// 页面操作类型，是新建还是编辑。
        /// </summary>
        public string MyType
        {
            get;
            set;
        }
        //默认实体
        public BP.OA.Vote.OA_Vote en = new OA_Vote();

        public CreateVote()
        {
            en.IsAnonymous = "1";
            en.IsEnable = "1";
            en.VoteType = "单投";
            en.DateFrom = BP.OA.Main.CurDateTimeStr;
            en.DateTo = BP.OA.Main.CurDateTimeStr;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //提交（保存新建投票）；
            if (Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(hOid.Value))
                {
                    SaveCreate();
                }
                else
                {
                    UpdateVote();
                }
            }
            //判断是查看还是编辑
            else
            {
                string strMethod = Request.QueryString["m"];
                string strId = Request.QueryString["id"];
                hOid.Value = strId;
                //如果ID和Method不为空则为编辑，否则为新建。
                if (!string.IsNullOrEmpty(strMethod) && !string.IsNullOrEmpty(strId))
                {
                    MyType = "Edit";
                    DoRequest(strMethod, strId);
                }
                else
                {
                    MyType = "Create";
                }
            }
        }

        private void UpdateVote()
        {
            OA_Vote vote = new OA_Vote(Convert.ToInt32(hOid.Value));
            vote.FK_Emp = UserNo;
            int i = BP.OA.UI.PageCommon.Update(this, vote);
            //string[] colunms = new string[] { "Title", "Item", "FK_Emp", "VoteType", "IsAnonymous", "IsEnable", "DateFrom", "DateTo", "CreateDate", "VoteEmpName", "VoteEmpNo" };
            //int i = Common.Update(this, "OID", hOid.Value, colunms, "OA_Vote");
            if (i > 0)
            {
                Common.Alert("修改成功！", this);
            }
            else
            {
                Common.Alert("修改失败！", this);
            }
        }


        public void SaveCreate()
        {
            OA_Vote vote = new OA_Vote();
            BP.OA.UI.PageCommon.GetEntity(this, vote);
            vote.FK_Emp = UserNo;
            //如果不选默认为启用
            if (string.IsNullOrEmpty(vote.IsEnable))
            {
                vote.IsEnable = "1";
            }
            //新增 统一时间格式
            vote.DateFrom = BP.OA.Main.TransDateTime(vote.DateFrom);//开始时间
            vote.DateTo = BP.OA.Main.TransDateTime(vote.DateTo);//结束时间
            vote.CreateDate = BP.OA.Main.TransDateTime(DateTime.Now.ToString());//创建时间
            //新增 不选择  就默认为全部人员参与投票

            if (string.IsNullOrEmpty(vote.IsAnonymous))
            {
                vote.IsAnonymous = "1";
            }
            int i = vote.Insert();
            if (i > 0)
            {
                Common.Alert("添加成功！", this);
            }
            else
            {
                Common.Alert("添加失败！", this);
            }
        }
        //根据ID查询内容。
        public void DoRequest(string method, string id)
        {
            DataTable dt = BP.OA.Vote.OA_Vote.GetVoteByOID(id);

            if (dt != null && dt.Rows.Count == 1)
            {
                en = Common.GetValue<BP.OA.Vote.OA_Vote>(dt.Rows[0]);
            }
        }
        public string Check(object o)
        {
            string strVal = Convert.ToString(o);
            if (!string.IsNullOrEmpty(strVal) && strVal == "1")
            {
                return "checked='checked'";
            }
            return string.Empty;
        }
        public string CheckType1(object o)
        {
            string strVal = Convert.ToString(o);
            if (!string.IsNullOrEmpty(strVal) && strVal == "单投")
            {
                return "checked='checked'";
            }
            return string.Empty;
        }
        public string CheckType2(object o)
        {
            string strVal = Convert.ToString(o);
            if (!string.IsNullOrEmpty(strVal) && strVal == "多投")
            {
                return "checked='checked'";
            }
            return string.Empty;
        }
        /// <summary>
        /// 检查页面状态，如果是编辑则投票列表页面为只读。
        /// </summary>
        public string CheckMyType()
        {
            if (MyType == "Edit")
            {
                return "readonly='readonly'";
            }
            return string.Empty;
        }
    }
}