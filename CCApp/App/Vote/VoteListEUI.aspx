﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true" CodeBehind="VoteListEUI.aspx.cs" Inherits="CCOA.App.Vote.VoteListEUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                    { field: 'OID', title: '编号', width: 60 },
                    { field: 'Title', title: '投票主题', width: 150 },
                    {
                        field: 'IsAnonymousText', title: '是否匿名', width: 120,
                        styler: function (value, row, index) {
                            if (value == "允许") {
                                return 'color:green;';
                            } else {
                                return 'color:red;';
                            }
                        }
                    },
                    {
                        field: 'IsEnableText', title: '是否启用', width: 120,
                        styler: function (value, row, index) {
                            if (value == "启用") {
                                return 'color:green;';
                            } else {
                                return 'color:red;';
                            }
                        }
                    },
                    { field: 'CreateDate', title: '创建时间', width: 120 },
                    { field: 'DateFrom', title: '开始时间', width: 120 },
                    { field: 'DateTo', title: '结束时间', width: 120 },
                ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                selectOnCheck: false,
                checkOnSelect: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                    var row = $('#newsGrid').datagrid('getSelected');
                    if (row) {
                        if (window.parent.addTab) {
                            window.parent.addTab(row.Title, "../App/Vote/VoteResult.aspx?id=" + row.OID, "");
                        } else {
                            window.location.href = "../App/Vote/VoteResult.aspx?id=" + row.OID;
                        }
                    }
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            var keyWords = $("#TB_KeyWords").val();
            var params = {
                method: "getnewslist",
                keyWords: encodeURI(keyWords),
                pageNumber: pageNumber,
                pageSize: pageSize,
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        //投票编辑
        function NewsEdit() {
            var row = $('#newsGrid').datagrid('getSelected');
            if (row) {
                window.location.href = "CreateVote.aspx?m=edit&&id=" + row.OID;
            }
            else {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
            }
        }
        //查看投票
        function VoteView() {
            var row = $('#newsGrid').datagrid('getSelected');
            if (row) {
                window.location.href = "VoteResult.aspx?id=" + row.OID;
            }
            else {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
            }
        }
        //删除邮件
        function DelNews() {
            var rows = $('#newsGrid').datagrid('getChecked');
            var ids = [];
            if (rows && rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    ids.push(rows[i].OID);
                    test = rows[i].OID;
                }
            }
            //判断是否选中项
            if (ids.length == 0) {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
                return;
            }
            $.messager.confirm('确认', '您确认想要删除记录吗？', function (r) {
                if (r) {
                    var params = {
                        method: 'newsdelete',
                        newIds: ids.join(',')
                    };
                    queryData(params, function (js, scope) {
                        var grid = $('#newsGrid');
                        var options = grid.datagrid('getPager').data("pagination").options;
                        var curPage = options.pageNumber;
                        LoadGridData(curPage, 20);
                    }, this);
                }
            });
        }
        //初始化
        $(function () {
            $("#pageloading").show();
            LoadGridData(1, 20);
        });
        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "VoteList.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="pageloading">
    </div>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0; overflow: hidden;">
        <div id="tb" style="padding: 6px; height: 28px;">
            <div style="float: left;">
                <%--   投票类型：<select id="NewCatagory" name="NewCatagory"></select>--%>
                关键字：<input id="TB_KeyWords" type="text" style="width: 150px;" />
                <%--<input id="start" name="dd" type="text" class="easyui-datebox" editable="false" />---
                <input id="end" type="text " name="dd" class="easyui-datebox" editable="false" />--%>
            </div>
            <a id="DoQueryByKey" style="float: left;" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                onclick="LoadGridData(1, 20)">查询</a>
            <%-- <a id="DoClear" href="#" style="float: left;" class="easyui-linkbutton" onclick="LoadClear()">清空</a>--%>
            <div class="datagrid-btn-separator">
            </div>
            <a id="VoteView" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-addfile'" onclick="VoteView()">查看投票</a>
            <a id="editNews" href="#" style="float: left;" class="easyui-linkbutton"
                data-options="plain:true,iconCls:'icon-edit'" onclick="NewsEdit()">编辑</a>
            <a id="deleteNews" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                onclick="DelNews()">删除</a>
            <div class="datagrid-btn-separator">
            </div>

        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
    </div>
</asp:Content>
