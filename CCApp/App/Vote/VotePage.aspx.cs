﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.Web;

namespace CCOA.App.Vote
{
    public partial class VotePage : System.Web.UI.Page
    {
        public BP.OA.Vote.OA_Vote en = null;
        private string _VoteId = null;
        /// <summary>
        /// 投票ID
        /// </summary>
        public string VoteId
        {
            get
            {
                if (_VoteId == null)
                {
                    _VoteId = Request.QueryString["id"];
                }
                return _VoteId;
            }
        }
        public string UserNo
        {
            get { return WebUser.No; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                InsertVote();
            }
            else
            {
                if (!string.IsNullOrEmpty(VoteId))
                {
                    DataTable dt = BP.OA.Vote.OA_Vote.GetVoteByOID(VoteId);
                    if (dt != null && dt.Rows.Count == 1)
                    {
                        en = Common.GetValue<BP.OA.Vote.OA_Vote>(dt.Rows[0]);
                        lblTitle.Text = en == null ? "" : en.Title;
                        lblFromTime.Text = en == null ? "" : BP.OA.Main.TransDate_CN(en.DateFrom);
                        lblToTime.Text = en == null ? "" : BP.OA.Main.TransDate_CN(en.DateTo);
                    }
                }
            }
        }

        void InsertVote()
        {
            //string[] colunms = new string[] { "FK_VoteOID", "VoteItem", "Comment", "IsAnony", "FK_Emp" };
            BP.OA.Vote.OA_VoteDetail detail = new BP.OA.Vote.OA_VoteDetail();
            detail = BP.OA.UI.PageCommon.GetEntity(this, detail) as BP.OA.Vote.OA_VoteDetail;
            detail.VoteDate = BP.OA.Main.CurDateTime;
            int i = detail.Insert();
            if (i > 0)
            {
                //Common.Alert("投票成功！", this);
               this.Page.Response.Redirect("VoteResult.aspx?id="+detail.FK_VoteOID);
            }
            else
            {
                Common.Alert("投票失败！", this);
            }
        }

        public bool IsHasVoted()
        {
            if (!string.IsNullOrEmpty(VoteId))
            {
                return BP.OA.Vote.OA_VoteDetail.IsHasVoted(WebUser.No, Request.QueryString["id"]);
            }
            return false;
        }
    }
}