﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Sys;
using BP.Web;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class ViewPlan : System.Web.UI.Page
    {
        /// <summary>
        /// 编号
        /// </summary>
        private string No
        {
            get
            {
                return this.Request["PK"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //装载数据
                PlanCatalog planCatalog = new PlanCatalog(this.No);
                //if (WebUser.No != "admin" && planCatalog.CheckLookRight(WebUser.No) == false)
                //{
                //    this.Response.Redirect("ErrorMsg.aspx?ErModel=look", true);
                //    return;
                //}
                ui_PCModel.Text = planCatalog.PCModel == PCModel.PlanModel ? "计划" : "阶段";
                ui_FinishPercent.Text = planCatalog.FinishPercent.ToString();
                ui_FK_DeptText.Text = planCatalog.FK_DeptText;
                ui_Name.Text = planCatalog.Name;
                ui_PersonCharge.Text = planCatalog.PersonChargeText;
                ui_PlanPartake.Text = planCatalog.PlanPartakeText;
                ui_StartDate.Text = planCatalog.StartDate;
                ui_EndDate.Text = planCatalog.EndDate;
                ui_SimpleContents.Text = planCatalog.SimpleContents;
                ui_PK_NO.Value = this.No;
            }
            catch (Exception ex)
            {
            }
        }
    }
}