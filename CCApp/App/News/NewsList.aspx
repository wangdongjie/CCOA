﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="NewsList.aspx.cs" Inherits="CCOA.App.News.NewsList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">    
    <link rel="stylesheet" type="text/css" href="../../js/js_EasyUI/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="../../js/js_EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="../demo.css" />
    <link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/js_EasyUI/jquery.easyui.min.js"></script>
    <script src="../../js/js_EasyUI/plugins/jquery.menubutton.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkHasSel() {
            var selCount = 0;
            $(".checkdelete").each(function () {
                if ($(this).prop("checked") == true) selCount++;
            });
            if (selCount == 0) {
                alert("你没有选择任何新闻！");
                return false;
            }
            else {
                return confirm("你确定要删除所选择的"+selCount+"条新闻吗？");
            }
        }</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="padding-left: 10px; float:left;color: #444; margin-bottom:2px; width:700px;">
        <div style="text-align: left; float: left;">
           新闻目录：<asp:DropDownList runat="server" ID="ddlNewsCatagory" Width="100px"></asp:DropDownList>
            关键字：<asp:TextBox ID="tbKeyWords" runat="server" Width="250" BorderStyle="Solid" BorderColor="#aaaaaa"
                BorderWidth="1"></asp:TextBox>&nbsp;
                <asp:Button ID="LinkButton1" Font-Bold="true" ForeColor="#234323" runat="server" OnClick="Query" Text="查询" />
        </div>
        <div style="float:left;"><asp:Button ID="LinkButton3" Font-Bold="true" ForeColor="#234323" runat="server" OnClick="QueryAll" Text="显示所有" /></div>
        <div style="float: left;">
            <asp:Button runat="server" ID="lbDelSel" OnClick="DelSel" Font-Bold="true" ForeColor="#234323"
                OnClientClick="return checkHasSel()" Text="批量删除" />
        </div>
    </div>
    <asp:GridView ID="GridView1" DataKeyNames="id" CssClass="grid" runat="server" AutoGenerateColumns="False"
        OnDataBound="GridView1_DataBound">
        <HeaderStyle HorizontalAlign="Left" />
        <Columns>
            <asp:TemplateField ItemStyle-Width="20">
                <HeaderTemplate>
                    <input class="checkall" type="checkbox" onclick='$(".checkdelete").prop("checked", $("input[class=checkall]").prop("checked"));' />
                </HeaderTemplate>
                <ItemTemplate>
                    <input name="checksel" type="checkbox" class="checkdelete" id="checksel" value='<%#Eval("id") %>' />
                </ItemTemplate>
                <ItemStyle Width="20px"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="id" HeaderText="编号" ItemStyle-Width="40" ReadOnly="true">
                <ItemStyle Width="40px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="ArticleCatagory" HeaderText="新闻目录" ItemStyle-Width="70px">
                <ItemStyle Width="70px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="标题">
                <ItemTemplate>
                    <asp:HyperLink ID="Label1" runat="server" Font-Bold="false" Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("id","NewsArticle.aspx?ID={0}") %>'></asp:HyperLink>
                    <%# BP.OA.Main.GetTimeImg(Eval("AddTime"),2,"新邮件","img/news_New.gif",0,0)%>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("SetTop")))?"":"<img src='img/top.gif' alt='置顶'"%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="关键字">
                <ItemTemplate>
                    <span title='<%#Eval("KeyWords") %>'>
                        <%# GetLimitedLength(Eval("KeyWords"))%></span>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
                <ItemStyle Width="80px"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="AddTime" HeaderText="发布时间" ItemStyle-Width="120px">
                <ItemStyle Width="120px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="BrowseCount" HeaderText="浏览" ItemStyle-Width="40px" DataFormatString="{0} 次">            
            </asp:BoundField>
            <asp:TemplateField ShowHeader="False" HeaderText="操作" ItemStyle-Width="190px">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink3" runat="server" Text="查看" NavigateUrl='<% #String.Format("NewsArticle.aspx?page=nl&ID={0}",Eval("id")) %>'></asp:HyperLink>
                    &nbsp;|&nbsp;
                    <asp:HyperLink ID="HyperLink4" runat="server" Text="编辑" NavigateUrl='<% #String.Format("NewsEdit.aspx?page=nl&ID={0}",Eval("id")) %>'></asp:HyperLink>
                    &nbsp;|&nbsp;
                    <a href="javascript:void(0)" class="easyui-menubutton" menu="#mm_<%#Eval("id") %>">更多</a>
                    <div id="mm_<%#Eval("id") %>" style="width: 120px; display: none;">
                        <div>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text="查看" NavigateUrl='<% #String.Format("NewsArticle.aspx?page=nl&ID={0}",Eval("id")) %>'></asp:HyperLink></div>
                        <div iconCls="icon-edit">
                            <asp:HyperLink ID="HyperLink2" runat="server" Text="编辑" NavigateUrl='<% #String.Format("NewsEdit.aspx?page=nl&ID={0}",Eval("id")) %>'></asp:HyperLink>
                        </div>
                        <div iconCls="icon-delete">
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" OnClick="Del"
                                OnClientClick="return confirm('是否真的要删除这条新闻吗？');" CommandName='<% #Eval("id") %>'
                                Text="删除" /></div>
                        <div>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" OnClick="SetTop"
                                CommandName='<% #Eval("id") %>' Text=' 置顶' ToolTip='<%#Eval("SetTop") %>' /></div>
                        <div>
                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" OnClick="CancelTop" Enabled='<%# !String.IsNullOrEmpty(Convert.ToString(Eval("SetTop"))) %>'
                                CommandName='<% #Eval("id") %>' Text=' 取消置顶' ToolTip='<%#Eval("SetTop") %>' /></div>
                    </div>
                </ItemTemplate>
                <ItemStyle Width="190px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <RowStyle HorizontalAlign="Center" />
        <HeaderStyle HorizontalAlign="Center" />
    </asp:GridView>
    <div style="text-align: center;">
   
    <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                     PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    OnPageChanged="AspNetPager1_PageChanged" CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
