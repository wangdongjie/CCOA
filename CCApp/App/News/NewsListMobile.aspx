﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="NewsListMobile.aspx.cs" Inherits="CCOA.App.News.NewsListMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
    <style type="text/css">
        a:link,a:visited
        {
             text-decoration:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div data-role="page" data-theme="e">
        <div data-role="header"  data-position="fixed" data-theme="b">
            <a href="/CCMobile/Home.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                新闻列表</h2>
        </div>
        <div class="ui-content" data-role="main" style="padding: 0px;">
            <%
                BP.OA.ArticleCatagorys cats = new BP.OA.ArticleCatagorys();
                cats.RetrieveAll();

                BP.OA.Articles ens = new BP.OA.Articles();
                ens.RetrieveAll();
    
            %>
            <table>
                <%
                    int cs = 0;//显示条数控制
                    foreach (BP.OA.ArticleCatagory itemCat in cats)
                   {
                       cs = 0;
                %>
                <tr>
                    <th colspan="2">
                        <a href="NewsCategoryMobile.aspx?No=<%=itemCat.No %>" data-role="button" data-icon="star"> 
                            <%=itemCat.Name%></a>
                    </th>
                </tr>
                <%
                    
                    foreach (BP.OA.Article item in ens)
                    {
                        
                        if (item.FK_ArticleCatagory != itemCat.No)
                            continue;
                        cs++;
                        if (cs > 5)
                            break;
                        
                %>
                
                <tr>
                    <td>
                        <img src="img/xinwen.png" style="width: 60px;" />
                    </td>
                    <td>
                       <a href="/App/News/NewArticleMobile.aspx?ID=<%=item.OID %>" > <%=item.Title %></a>
                        <br />
                        <font color="green" size="-3" >
                            <%= BP.DA.DataType.ParseSysDate2DateTimeFriendly( item.AddTime) %>
                            |
                            <%= item.FK_UserNo %>
                        </font>
                    </td>
                </tr>
                <tr>
                <td colspan="2"><hr /></td>
                </tr>
                
                <%}
                       
                   } %>
                    
            </table>
        </div>
    </div>
</asp:Content>
