﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="NewsArticle.aspx.cs" Inherits="CCOA.App.News.NewsArticle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
    .article_intro{padding: 10px 10px 10px 10px; background-color: #eeeeee;  color:#444444;}
    .ari{font-weight:bold; }
    .arl{}
    div.article{padding-left: 10px;min-height:300px;}
    p.article p{min-height:0px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="font-size: 11px; position: absolute; width: 50px; height: 50px; top: 2px; left: 2px;">
        <% 
            if (Request["page"] == "mynl")%>
        <%  {%>
        <a href="MyNewsList.aspx">
        <% } %>
        <%else %>
        <%  {%>
        <a href="NewsList.aspx">
        <% } %>
        <img style="width: 40px; height: 40px;" src="img/back.png" alt="返回新闻列表" /></a>
    </div>
    <p style="font-size: 25px; font-weight: bolder; padding-left: 10px; text-align: center;">
        <asp:Label runat="server" ID="lblTitle"></asp:Label>
    </p>
    <p class="article_intro">
        <span class="ari">作者：</span><asp:Label runat="server" ID="lblUser" CssClass="arl"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
            class="ari">发布时间：</span><asp:Label runat="server" ID="lblAddTime" CssClass="arl"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="ari">新闻来源：</span><asp:Label runat="server" ID="lblArticleSource" CssClass="arl"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                    class="ari">新闻目录：</span><asp:Label runat="server" ID="lblNewsCatagory" CssClass="arl"></asp:Label>
    </p>
    <div class="article">
        <asp:Literal ID="liContent" runat="server"></asp:Literal>
    </div>
    <asp:Panel runat="server" ID="pnAttachFile" Visible="false" Style="border-top: dashed 1px #777;">
        &nbsp;&nbsp;&nbsp;<img src="img/attach1.png" style="height: 18px;" alt="附件">附件下载(右击另存下载)：
       <asp:Literal runat="server" ID="liAttachFiles"></asp:Literal>
    </asp:Panel>
</asp:Content>
