﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace CCOA.App.News
{
    public partial class NewsListEUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(null, "新闻");
            }
        }
    }
}