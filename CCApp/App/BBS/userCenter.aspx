﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true" CodeBehind="userCenter.aspx.cs" Inherits="CCOA.App.BBS.userCenter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<ul class="sorttag clearfix"  style="margin-top:8px;">
  <li class="ntop"></li>
  <li class="ncurr" ><a href="">个人信息</a></li>
  <li ><a href="userforum.aspx?act=f&FK_Emp=<%=FK_Emp %>" class="nlast">主题</a></li>
  <li class="nbott"></li>
</ul>

<div class="threadtype userCenter">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50" height="100">&nbsp;</td>
            <td style="width:100px; display:block; text-align:center;">
            <div class="author_photo_in" ><span class="author_photo_bg"></span>
              <img src="" width="81" height="81" onerror="this.src = '/images/default.gif'" runat="server" id="userimg"/>
            </div>
              <label id="lab_EditImg" runat="server"><a href="userImg.aspx">修改头像</a></label>
            </td>
            <td style="text-align:left;"></td>
        </tr>
        <tr class="title">
            <td width="50">&nbsp;</td>
            <th colspan="2">用户名称：<asp:Label ID="lab_userName" runat="server">123123</asp:Label></th>
        </tr>
    </table>    
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50">&nbsp;</td>
            <td width="300">经验：<asp:Label ID="lab_exp" runat="server">123456</asp:Label></td>
            <td align="right">积分：<asp:Label ID="lab_integral" runat="server">123456</asp:Label></td>
            <td align="right">金币：<asp:Label ID="lab_gold" runat="server">123456</asp:Label></td>
        </tr>
        <tr class="title"><th>&nbsp;</th><th colspan="3" height="20">统计信息</th></tr>        
        <tr>
            <td>&nbsp;</td>
            <td align="right">主题数：<asp:Label ID="lab_themeCount" runat="server">123456</asp:Label></td>
            <td align="right" colspan="2">回复数：<asp:Label ID="lab_replyCount" runat="server">123456</asp:Label></td>
        </tr>
        <tr class="title"><th>&nbsp;</th><th colspan="3" height="20">其它信息</th></tr>        
        <tr>
            <td>&nbsp;</td>
            <td align="right">注册时间：<asp:Label ID="lab_regDate" runat="server">123456</asp:Label></td>
            <td colspan="2" align="right">最后活动时间：<asp:Label ID="lab_lastDate" runat="server">123456</asp:Label></td>
        </tr>
    </table>
</div>
</asp:Content>
