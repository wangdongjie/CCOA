﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.Web;
using BP.Sys;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class Post : WebPage
    {
        /// <summary>
        /// 主题编号
        /// </summary>
        public string FK_Motif
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Motif");
            }
        }
        /// <summary>
        /// 对外接口，用于改变Session名称，支持WebConfig配置
        /// </summary>
        private string CheckCode_SessionName
        {
            get
            {
                string code = Convert.ToString(ConfigurationManager.AppSettings["CheckCode_SessionName"]);
                if (String.IsNullOrEmpty(code))
                    return "CheckCode";
                else
                    return code;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //如果为空进行新增
                    if (string.IsNullOrEmpty(FK_Motif))
                    {
                        string classNo = StringFormat.GetQuerystring("FK_Class");
                        BBSClass bbsClass = new BBSClass(classNo);

                        if (string.IsNullOrEmpty(bbsClass.No))
                        {
                            this.Alert("出错了。");
                            return;
                        }

                        hidden_classid.Value = bbsClass.No;
                        lab_className.Text = bbsClass.Name;
                    }
                    else
                    {
                        //进行编辑
                        BBSMotif motif = new BBSMotif(FK_Motif);
                        hidden_classid.Value = motif.FK_Class;
                        txt_title.Text = motif.Name;
                        editorContent.Value = motif.Contents;
                        BBSClass bbsClass = new BBSClass(motif.FK_Class);
                        lab_className.Text = bbsClass.Name;
                    }
                }
                catch
                {
                    this.Alert("出错了。");
                }
            }
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            //string code = this.txt_vali.Text;
            //验证用户变量
            //if (HttpContext.Current.Session[this.CheckCode_SessionName] != null
            //    && Convert.ToString(HttpContext.Current.Session[this.CheckCode_SessionName]) != code)
            //{
            //    this.Alert("验证码不对");
            //    return;
            //}

            string title = txt_title.Text;
            string content = editorContent.Value;
            //if (title.Length < 10 || title.Length > 100)
            //{
            //    this.Alert("标题不能为空，且不能超过50字！");
            //    return;
            //}
            if(title.Length < 1)
            {
                this.Alert("标题不能为空！");
                return;
            }
            else if (title.Length > 50)
            {
                this.Alert("标题不能超过50字！");
                return;
            }
            if (content.Length < 1)
            {
                this.Alert("内容不能为空！");
                return;
            }
            
            //如果为空进行新增
            if (string.IsNullOrEmpty(FK_Motif))
            {
                BBSMotif motif = new BBSMotif();
                motif.Name = title;
                motif.Contents = content;
                motif.FK_Class = hidden_classid.Value;
                motif.FK_Emp = WebUser.No;
                motif.RDT = DateTime.Now;
                motif.LastReTime = DateTime.Now;
                int i = motif.Insert();
                motif.Contents = content;
                motif.Update();
                if (i > 0)
                    Response.Redirect("List.aspx?FK_Class=" + motif.FK_Class);
            }
            else
            {
                BBSMotif motif = new BBSMotif(FK_Motif);
                motif.Name = title;
                motif.Contents = content;
                int i= motif.Update();
                if (i > 0)
                    Response.Redirect("thread.aspx?FK_Class=" + motif.FK_Class + "&FK_Motif=" + FK_Motif);
            }
        }

        protected void btn_Back_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FK_Motif))
            {
                Response.Redirect("List.aspx?FK_Class=" + hidden_classid.Value);
            }
            else
            {
                Response.Redirect("thread.aspx?FK_Class=" + hidden_classid.Value + "&FK_Motif=" + FK_Motif);
            }
        }
    }
}