﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Sys;
using BP.Web;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class userCenter : WebPage
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public string FK_Emp
        {
            get
            {
                string userNo = StringFormat.GetQuerystring("FK_Emp");
                if (string.IsNullOrEmpty(userNo)) return WebUser.No;
                return userNo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Localize htmNav = (Localize)this.Page.Master.FindControl("htm_Nav");
            htmNav.Text = "<a href=\"default.aspx\" class=\"ghome\">论坛首页</a> > <a href=\"\" class=\"ghome\">用户中心</a>";
            if (!IsPostBack)
            {
                if (FK_Emp != "")
                {
                    BBSUserInfo userInfo = new BBSUserInfo(FK_Emp);
                    if (userInfo.FK_EmpText != "")
                    {
                        lab_EditImg.Disabled = true;
                        userimg.Src = string.IsNullOrEmpty(userInfo.UserImg) ? "images/default.gif" : userInfo.UserImg;

                        lab_userName.Text = userInfo.FK_EmpText;
                        lab_exp.Text = userInfo.Exp.ToString();
                        lab_gold.Text = userInfo.Gold.ToString();
                        lab_integral.Text = userInfo.Integral.ToString();
                        lab_replyCount.Text = userInfo.ReplayCount.ToString();
                        lab_themeCount.Text = userInfo.ThemeCount.ToString();
                        lab_regDate.Text = userInfo.RegDate.ToString();
                        lab_lastDate.Text = userInfo.LastDate.ToString();
                    }                    
                }
            }
        }
    }
}