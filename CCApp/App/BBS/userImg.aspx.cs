﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using BP.Sys;
using BP.Web;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class userImg : WebPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Localize htmNav = (Localize)this.Page.Master.FindControl("htm_Nav");
            htmNav.Text = "<a href=\"default.aspx\" class=\"ghome\">论坛首页</a> > <a href=\"userCenter.aspx\" class=\"ghome\">用户中心</a> > <a href='#' class=\"ghome\">修改头像</a>";

            if (!IsPostBack)
            {
                BBSUserInfo userInfo = new BBSUserInfo(WebUser.No);

                if (userInfo.UserImg != "")
                {
                    userimg.Src = userInfo.UserImg;
                }
                else
                {
                    userimg.Src = "images/default.gif";
                }
            }
        }

        protected void btn_upload_Click(object sender, EventArgs e)
        {
            if (upload_Img.FileName == "")
            {
                this.Alert("请指定上传文件");
                return;
            }

            string fileExt = Path.GetExtension(upload_Img.FileName);
            string dateFolder = HttpContext.Current.Server.MapPath("\\DataUser\\BBSImage");
            if (!Directory.Exists(dateFolder))  // 检测是否存在磁盘目录
            {
                Directory.CreateDirectory(dateFolder);  // 不存在的情况下，创建这个文件目录 例如 C:/wwwroot/Files/201111/
            }
            upload_Img.SaveAs(dateFolder + "\\" + WebUser.No + fileExt);

            BBSUserInfo userInfo = new BBSUserInfo(WebUser.No);
            userInfo.UserImg = BP.WF.Glo.CCFlowAppPath + "DataUser/BBSImage/" + WebUser.No + fileExt;
            userInfo.Update();
            Response.Redirect("userCenter.aspx");
        }
    }
}