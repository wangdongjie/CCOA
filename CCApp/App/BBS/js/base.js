﻿function InitKind(targerID, rootPath, editors) {
    KindEditor.ready(function (K) {
        var var_rootPath = rootPath;
        editors.obj = K.create(targerID, {
            cssPath: '/ctrl/kindeditor/plugins/code/prettify.css',
            uploadJson: '/ctrl/kindeditor/asp.net/upload_json.ashx',
            fileManagerJson: '/ctrl/kindeditor/asp.net/file_manager_json.ashx',
            allowFileManager: true,
            afterCreate: function () {
                var self = this;
                K.ctrl(document, 13, function () {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
                K.ctrl(self.edit.doc, 13, function () {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
            }
        });
        prettyPrint();
    });
}

document.onkeydown = function (evt) {
    var b = !!evt, oEvent = evt || window.event;
    if (oEvent.keyCode == 8) {
        var node = b ? oEvent.target : oEvent.srcElement;
        var reg = /^(input|textarea)$/i, regType = /^(password|text|textarea)$/i;
        if (!reg.test(node.nodeName) || !regType.test(node.type) || node.readOnly || node.disabled) {
            if (b) {
                evt.cancelBubble = true;
                evt.preventDefault();
                evt.stopPropagation();
            }
            else {
                oEvent.cancelBubble = true;
                oEvent.keyCode = 0;
                oEvent.returnValue = false;
            }
        }
    }
}

/*16进制加密*/
function EnEight(contentStr) {
    var monyer = new Array(); var i, s;
    for (i = 0; i < contentStr.length; i++) {
        monyer += "-" + contentStr.charCodeAt(i).toString(16);
    }
    return monyer;
}
/*16进制解密*/
function DeEight(contentStr) {
    var monyer = new Array(); var i;
    var s = contentStr.split("-");
    for (i = 1; i < s.length; i++) {
        monyer += String.fromCharCode(parseInt(s[i], 16));
    }
    return monyer;
}

//获取传值
function getParameter(param) {
    var query = window.location.search;
    var iLen = param.length;
    var iStart = query.indexOf(param);

    if (iStart == -1) {
        return null;
    }

    iStart += iLen + 1;
    var iEnd = query.indexOf("&", iStart);

    if (iEnd == -1) {
        return query.substring(iStart);
    }

    return query.substring(iStart, iEnd);
}

//获取cookie
function GetCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return unescape(arr[2]);
    }
    return null;
}

//删除cookie
function DelCookie(name) {
    document.cookie = name + "=" + escape("Null") + "; expires=Fri, 31 Dec 1999 23:59:59 GMT;";
}

function CheckIframe() {
    if (window.parent.document.getElementById('iframeTop') == null) {
        var var_PathNameData = window.location.pathname.split('/');
        var var_PageName = var_PathNameData[var_PathNameData.length - 1].split('.')[0];
        window.parent.location.href = 'ManageMain.aspx?target=' + var_PageName + '';
    }
}

//判断是否IE浏览器
function IEBrowserCheck() {
    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
            (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
            (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
            (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
            (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
    if (Sys.ie) {
        return true;
    }
    return false;
}