﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="CCOA.App.Meeting.Order"
    EnableEventValidation="false" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/lang/en.js" type="text/javascript"></script>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <style type="text/css">
        .table th:first-child
        {
            text-align: right;
            width: 120px;
            padding-right: 20px;
        }
        .table td
        {
            text-align: left;
        }
        .table div
        {
            float: left;
        }
        .table input
        {
            width: 300px;
        }
        textarea
        {
            height: 100px;
            width: 300px;
        }
        .Validform_checktip
        {
            margin-left: 5px;
            margin-top: 10px;
            color: Red;
        }
    </style>
    <script language="javascript" type="text/javascript">
        //会议参与人员   岗位 部门        
        function CheckClick() {
            if ($("#cb_Users").attr("checked") != "checked") {
                $("#div_Users").fadeIn(10);
                $("#span_Users_part").fadeIn(10);
                $("#span_Users_all").fadeOut(10);
            } else {
                $("#div_Users").fadeOut(10);
                $("#span_Users_part").fadeOut(10);
                $("#span_Users_all").fadeIn(10);
            }
        }

        function RefreshParent() {
            window.parent.ReFreashRoomGrid();
        }

        function openDialog2(sender, e) {
            var diag = new Dialog();
            diag.Width = 620;
            diag.Height = 450;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e + "?no=" + $("#hidResNo").val();
            diag.OKEvent = function () {
                var result1 = "", result2 = "";
                var ddl = diag.innerFrame.contentWindow.document.getElementsByName('cbNo');
                $(ddl).each(function () {
                    if (this.checked == true) {
                        result1 += this.value + ",";
                        result2 += this.title + ",";
                    }
                    $("#hidResNo").val(result1);
                    $("#txtResName").val(result2);
                });
                diag.close();
            };
            diag.show();
        }



        //页面加载
        $(document).ready(function () {
            $(".registerform").Validform({
                tiptype: 2,
                showAllError: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post" class="registerform">
    <table class="table">
        <tr>
            <th class="Td1">
                会议标题
            </th>
            <td>
                <div>
                    <input id="txtTitle" type="text" name="Title" value="<%=ording.Title%>" maxlength="50"
                        datatype="s4-50" errormsg="标题只能4-50个字符！" nullmsg="标题不能为空！" />
                </div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                申请人
            </th>
            <td>
                <input type="text" value="<%=BP.Web.WebUser.Name%>" readonly="readonly" />
                <input type="hidden" value="<%=BP.Web.WebUser.No%>" name="Fk_Emp" />
            </td>
        </tr>
        <tr>
            <th>
                申请部门
            </th>
            <td>
                <input type="text" value="<%=BP.Web.WebUser.FK_DeptName%>" readonly="readonly" />
                <input type="hidden" value="<%=BP.Web.WebUser.FK_Dept%>" name="Fk_Dept" />
            </td>
        </tr>
        <tr>
            <th>
                会议室
            </th>
            <td>
                <select id="ddlRoom" name="Fk_Room" style="width: 150px;">
                    <%foreach (BP.OA.Meeting.Room item in rooms)
                      {
                          string state = string.Empty;
                          if (ording.Fk_Room == item.No || (FK_RoomNo != null && FK_RoomNo == item.No))
                          {
                              state = "selected='selected'";
                          }
                          string str = "<option value={0} " + state + ">{1}</option>";
                          Response.Write(string.Format(str, item.No, item.Name));
                      } %>
                </select>
            </td>
        </tr>
        <tr>
            <th>
                开始时间
            </th>
            <td>
                <div>
                    <input readonly="readonly" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss', minDate:'%y-%M-%d',maxDate:'%y-%M-{%d+7}'})"
                        class="Wdate" name="DateFrom" id="txtStartTime" value="<%=ording.DateFrom%>"
                        datatype="*" nullmsg="请选择开始时间！" />
                </div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                结束时间
            </th>
            <td>
                <div>
                    <input readonly="readonly" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d',maxDate:'%y-%M-{%d+7}'})"
                        class="Wdate" name="DateTo" id="txtEndTime" value="<%=ording.DateTo%>" datatype="*"
                        nullmsg="请选择结束时间！" />
                </div>
                <div>
                    <span class="Validform_checktip"><span style="color: Red">*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                参与人员
            </th>
            <td class="text" style="text-align: left;">
                <input type="checkbox" id="cb_Users" checked="checked" name="cb_Users" onclick="CheckClick()"
                    style="width: 20px" />
                <span id="span_Users_all">发给<b>全体人员</b>/部分人员</span> <span id="span_Users_part" style="display: none;">
                    发给全体人员/<b>部分人员<br />
                        （填写为以下三个条件的交集）</b></span>
                <div id="div_Users" style="display: none; margin-top: 5px; padding-top: 5px; border-top: dashed 1px #aaa;">
                    <input type="button" value="添加人员" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#txt_ToEmps','txt_ToEmps','txt_ToEmpNames2');" />（不选择表示全部人员）<br />
                    <asp:TextBox ID="txt_ToEmpNames2" runat="server" TextMode="MultiLine" Width="85%"
                        Rows="2" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="txt_ToEmps" runat="server"></asp:HiddenField>
                    <br />
                    <input type="button" value="添加岗位" onclick="zDialog_open2('../../ctrl/SelectStations/SelectStation.aspx', '选择岗位', 500, 400,'#txt_ToStations','txt_ToStations','txt_ToStationNames');" />（不选择表示所有岗位）<br />
                    <asp:TextBox ID="txt_ToStationNames" runat="server" TextMode="MultiLine" Width="85%"
                        Rows="2" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="txt_ToStations" runat="server"></asp:HiddenField>
                    <br />
                    <input type="button" value="添加部门" onclick="zDialog_open2('../../ctrl/SelectDepts/SelectDept_zTree1.aspx', '选择部门', 500, 400,'#txt_ToDepts','txt_ToDepts','txt_ToDeptNames');" />（不选择表示所有部门）<br />
                    <asp:TextBox ID="txt_ToDeptNames" runat="server" TextMode="MultiLine" Width="85%"
                        Rows="2" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="txt_ToDepts" runat="server"></asp:HiddenField>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                会议室资源
            </th>
            <td>
                <textarea id="txtResName" readonly="readonly" onfocus="openDialog2(this,'ResSelect.aspx')"><%=GetResourceName(ording.MTResource)%></textarea>
                <input type="hidden" id="hidResNo" value="<%=ording.MTResource%>" name="MTResource" />
            </td>
        </tr>
        <tr>
            <th>
                备注
            </th>
            <td>
                <textarea name="Note"><%=ording.Note%></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="submit" id="submit" style="width: 80px" onclick="window.close()" value="确定" />
            </td>
        </tr>
    </table>
    <input type="hidden" value="<%=ording.OID%>" name="OID" />
    <input type="hidden" value="<%=ording.State%>" name="State" />
    </form>
</body>
</html>
