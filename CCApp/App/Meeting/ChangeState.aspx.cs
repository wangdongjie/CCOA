﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Meeting
{
    public partial class ChangeState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string oid = Request.QueryString["oid"];
            string state = Request.QueryString["state"];
            if (!string.IsNullOrEmpty(oid) && !string.IsNullOrEmpty(state))
            {
                BP.OA.Meeting.RoomOrding ording = new BP.OA.Meeting.RoomOrding();
                ording.OID = Convert.ToInt32(oid);
                int i = ording.Update("State", state);
                if (i > 0 && state == "3")
                {
                    Common.Alert("会议已结束！", this);
                    Reload();
                }
                else if (i > 0 && state == "2")
                {
                    Common.Alert("会议已取消！", this);
                    Reload();
                }
            }
        }

        public void Reload()
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "load", "window.parent.Load();", true);
        }
    }
}