﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.DA;
using BP.Web;

namespace CCOA.App.Meeting
{
    public partial class SummaryShow : System.Web.UI.Page
    {
        public BP.OA.Meeting.RoomOrding ording = new BP.OA.Meeting.RoomOrding();
        public BP.OA.Meeting.SponsorMeeting sponsor = new BP.OA.Meeting.SponsorMeeting();
        protected void Page_Load(object sender, EventArgs e)
        {
            Show();
        }

        private void Show()
        {
            string oid = Request.QueryString["oid"];
            //判断sender是否含有当前用户
            string sentToMe = "SenderEmps like('%" + WebUser.No + ",%') ";
            string strJiYao =string.Format("SELECT FK_OID FROM OA_MeetingSummary  WHERE  FK_OID ={0} and  {1}",int.Parse(oid),sentToMe);
            int isExitJiYao = DBAccess.RunSQLReturnCOUNT(strJiYao);
            if (isExitJiYao!=1)
            {
                oid = null;
            }
            if (!string.IsNullOrEmpty(oid))
            {
                BP.OA.Meeting.RoomOrdings ordings = new BP.OA.Meeting.RoomOrdings();
                ordings.Retrieve(BP.OA.Meeting.RoomOrdingAttr.OID, oid);
                BP.OA.Meeting.SponsorMeetings sponsors = new BP.OA.Meeting.SponsorMeetings();
                sponsors.Retrieve(BP.OA.Meeting.SponsorMeetingAttr.FK_SponsorOID, oid);
                if (ordings.Count > 0 && sponsors.Count > 0)
                {
                    ording = ordings[0] as BP.OA.Meeting.RoomOrding;
                    sponsor = sponsors[0] as BP.OA.Meeting.SponsorMeeting;
                }
                DataTable dt = BP.OA.Meeting.MeetingSummaries.GetMeetringSummary(oid);

                Repeater1.DataSource = dt;
                Repeater1.DataBind();

            }
        }
        public string GetEmpName(object obj)
        {
            return MyHelper.GetEmpName(obj);
        }
    }
}