﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Meeting/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="CCOA.App.Meeting.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/Trim.js" type="text/javascript"></script>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //会议纪要添加成功
        //        function Sponsor(i) {
        //            diag.close();
        //            if (i > 0) {
        //                alert("会议纪要添加成功！");
        //            }
        //            else {
        //                alert("会议纪要添加失败！");
        //            }
        //        }
        var i = 0;
        //发起会议
        function Refresh(i, j) {
            diag.close();
            if (i > 0 && j > 0) {
                alert("发起成功！");
            }
            else {
                alert("发起失败！");
            }
            Load();
        }
        function Load() {
            var room = $("input[name=rdNo]:checked").val();
            $("#divCon").load("Details.aspx?room=" + room + "&date=<%=CurrDate%>&i=" + i.toString());
            i = i + 1;
        }
        function GetText(para) {
            return trim($("#span" + para + "").text());
        }
        //发起会议
        function Sponsor(para) {
            var strVal = GetText(para);
            if (strVal == "未发起") {
                var url = "SponsorMeeting.aspx?oid=" + para;
                DoOpen(url, 500, 700, '发起会议');
            }
            else {
                alert("会议" + strVal + "不能发起会议！");
            }
        }
        //会议纪要
        function Summary(para) {
            var strVal = GetText(para);
            if (strVal == "已结束") {
                var url = "Summary.aspx?oid=" + para;
                DoOpen(url, 400, 800, '会议纪要');
            }
            else {
                alert("会议" + strVal + "不能添加会议纪要！");
            }
        }
        //会议变更
        function Modify(para) {
            var strVal = GetText(para);
            if (strVal == "未发起" || strVal == "未开始") {
                var url = "Order.aspx?oid=" + para;
                DoOpen(url, 500, 600, '会议变更');
            }
            else {
                alert("会议" + strVal + "不能变更会议！");
            }
        }
        //结束会议
        function Stop(para) {
            var strVal = GetText(para);
            if (strVal == "正在进行") {
                ChangeState(para, 3);
            }
            else {
                alert("会议" + strVal + "不能结束会议！");
            }
        }
        //取消会议
        function Cancel(para) {
            var strVal = GetText(para);
            if (strVal == "未发起" || strVal == "未开始") {
                ChangeState(para, 2);
            }
            else {
                alert("会议" + strVal + "不能取消会议！");
            }
        }
        function ChangeState(para, state) {
            var url = "ChangeState.aspx?oid=" + para.toString() + "&state=" + state.toString();
            $("#iframe1").attr("src", url);
        }
        var diag = new Dialog();
        //打开窗口
        function DoOpen(url, height, width, title) {
            diag.Width = width;
            diag.Height = height;
            diag.Title = title;
            diag.URL = url;
            diag.show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="easyui-datagrid">
                <thead>
                    <tr>
                        <th data-options="field:'select'">
                            选择
                        </th>
                        <th data-options="field:'no'">
                            编号
                        </th>
                        <th data-options="field:'name'">
                            名称
                        </th>
                        <th data-options="field:'state'">
                            状态
                        </th>
                        <th data-options="field:'admin'">
                            管理员
                        </th>
                        <th data-options="field:'operate'">
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <input type="radio" name="rdNo" value='<%#Eval("No")%>' onclick='Load()' />
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"No") %>
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"Name") %>
                </td>
                <td>
                    已有 <span style="color: Red">
                        <%#CheckOrderNum(DataBinder.Eval(Container.DataItem, "No"))%></span> 个预定
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"FK_EmpName")%>
                </td>
                <td style="text-align: center" colspan="2">
                    <a href="javascript:void(0)" onclick="DoOpen('<%#"Order.aspx?id="+DataBinder.Eval(Container.DataItem,"No") %>',500,600,'会议室预订' )">
                        预定 </a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
    <div id="divCon" style="height: 100%; width: 100%">
    </div>
    <iframe id="iframe1" name="iframe1" style="height: 0px; width: 0px; display: none;">
    </iframe>
</asp:Content>
