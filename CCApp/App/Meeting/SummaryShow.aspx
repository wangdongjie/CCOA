﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SummaryShow.aspx.cs" Inherits="CCOA.App.Meeting.SummaryShow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        .table th
        {
            text-align: right;
            width:80px;
        }        
        .table td
        {
            text-align: left;
        }
    </style>
</head>
<body>
    <table class="table">
        <tr>
            <th>
                会议议题：
            </th>
            <td>
                <span>
                    <%=ording.Title%></span>
            </td>
        </tr>
        <tr>
            <th>
                主办部门：
            </th>
            <td>
                <span>
                    <%=ording.Fk_DeptName%></span>
            </td>
        </tr>
        <tr>
            <th>
                会议地点：
            </th>
            <td>
                <span>
                    <%=ording.Fk_RoomName%></span>
            </td>
        </tr>
        <tr>
            <th>
                与会人员：
            </th>
            <td>
                <span>
                    <%=GetEmpName(sponsor.ParticipantsEmpNo)%></span>
            </td>
        </tr>
        <tr>
            <th>
                开始时间：
            </th>
            <td>
                <span>
                    <%=ording.DateFrom%></span>
            </td>
        </tr>
        <tr>
            <th>
                结束时间：
            </th>
            <td>
                <span>
                    <%=ording.DateTo%></span>
            </td>
        </tr>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <tr>
                    <th>
                        纪要内容：
                    </th>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"Content")%>
                    </td>
                </tr>
                <tr>
                    <th>
                        发送纪要给：
                    </th>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"SenderEmps")%>
                    </td>
                </tr>
                <tr>
                    <th>
                        记录员：
                    </th>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"Recorder")%>
                    </td>
                </tr>
                <tr>
                    <th>
                        附件：
                    </th>
                    <td>
                        <a href='<%#Eval("Attachment")%>'>
                            <%#Eval("FileName")%></a>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</body>
</html>
