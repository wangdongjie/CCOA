﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderDetail.aspx.cs" Inherits="CCOA.App.Meeting.OrderDetail"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .imgsta
        {
            height: 18px;
            width: 18px;
        }
        .tdTitle
        {
            background-color: #E6F0F6;
            text-align: right;
            padding-right: 10px;
            width: 120px;
        }
        .tdContent
        {
            text-align: left;
            padding-left: 10px;
        }
    </style>
    <script type="text/javascript">
        function ShowAlert() {
            return confirm("你确定要删除么?");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater ID="Repeater1" runat="server">
        <ItemTemplate>
            <table style="margin-top: 20px;">
                <tr>
                    <td style="background-image: url(../../Images/title2.png);" colspan="4">
                        <div style="float: right;">
                            <span style="font-weight: bold">状态：</span>
                            <%#CheckState(DataBinder.Eval(Container.DataItem, "DTFrom"), DataBinder.Eval(Container.DataItem, "OID"))%>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        会议ID：
                    </td>
                    <td class="tdContent">
                        <%#DataBinder.Eval(Container.DataItem,"OID")%>
                    </td>
                    <td class="tdTitle">
                        预订者：
                    </td>
                    <td class="tdContent">
                        <%#DataBinder.Eval(Container.DataItem,"OrderName")%>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        会议室名称：
                    </td>
                    <td class="tdContent">
                        <%#DataBinder.Eval(Container.DataItem,"RoomName")%>
                    </td>
                    <td class="tdTitle">
                        鲜花水果：
                    </td>
                    <td class="tdContent">
                        需要鲜花水果
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        会议事由：
                    </td>
                    <td class="tdContent" colspan="3">
                        <%#DataBinder.Eval(Container.DataItem,"Title")%>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        开始时间：
                    </td>
                    <td class="tdContent">
                        <%#DataBinder.Eval(Container.DataItem,"DTFrom")%>
                    </td>
                    <td class="tdTitle">
                        结束时间：
                    </td>
                    <td class="tdContent">
                        <%#DataBinder.Eval(Container.DataItem,"DTTo")%>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        备注：
                    </td>
                    <td class="tdContent" colspan="3">
                        <%#DataBinder.Eval(Container.DataItem,"Doc")%>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
