﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="CCOA.App.Meeting.Summary"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Ctrl/kindeditor/kindeditor-min.js" type="text/javascript"></script>
    <script src="../../Ctrl/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <style type="text/css">
        .table th
        {
            width: 150px;
            text-align: right;
            padding-right: 15px;
        }
        .table td
        {
            text-align: left;
        }
        .table input, .table textarea
        {
            width: 300px;
        }
        .table input[readonly], .table textarea[readonly]
        {
            background-color: #EEEEEE;
        }
    </style>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function (K) {
            editor = K.create('#editor_id', {
                resizeType: 1,
                allowPreviewEmoticons: false,
                allowImageUpload: false,
                items: [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'image', 'link']
            });
        });
        function output() {
            editor.sync();
            $("#div1").html($("#editor_id").val());
            alert($("#div1").html());
        }
        function openDialog() {
            var diag = new Dialog();
            diag.Width = 620;
            diag.Height = 450;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = "../../Ctrl/SelectUsers/SelectUser_Jq.aspx";
            diag.OKEvent = function () {
                var ddl = diag.innerFrame.contentWindow.document.getElementById('lbRight');
                var resultName = "";
                var resultNo = "";
                for (i = 0; i < ddl.options.length; i++) {
                    resultName += ddl.options[i].text + ",";
                    resultNo += ddl.options[i].value + ",";
                }
                $("input[name=SenderEmpsName]").val(resultName);
                $("input[name=SenderEmps]").val(resultNo);
                diag.close();
            };
            diag.show();
        }
        function DoSubmit() {
            editor.sync();
            $("#form1").submit();
        }
        $(document).ready(function () {
            $("#form1").Validform({
                tiptype: 2,
                btnSubmit: "#btnSubmit",
                showAllError: true
            });
        });
        function RefreshParent() {
            window.parent.RefreashOrdingGrid();
        }
    </script>
</head>
<body>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
    <table class="table">
        <tr>
            <th>
                会议议题：
            </th>
            <td>
                <input type="text" value='<%=ording.Title%>' name="Title" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                主办部门：
            </th>
            <td>
                <input type="text" value="<%=ording.Fk_DeptName%>" name="SponsorDeptNo" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                会议地点：
            </th>
            <td>
                <input type="text" value="<%=ording.Fk_RoomName%>" name="RoomName" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                与会人员：
            </th>
            <td>
                <input type="text" value="<%=GetEmpName(sponsor.ParticipantsEmpNo)%>" name="ParticipantsEmpNo"
                    readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                开始时间：
            </th>
            <td>
                <input type="text" value="<%=ording.DateFrom%>" name="DateFrom" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                结束时间：
            </th>
            <td>
                <input type="text" value="<%=ording.DateTo%>" name="DateTo" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <th>
                纪要内容：
            </th>
            <td>
                <div>
                    <textarea id="editor_id" name="content" datatype="*" nullmsg="请输入纪要内容！" style="height: 300px"></textarea>
                </div>
                <div style="color: Red; margin-top: 8px;">
                    <span class="Validform_checktip"><span>*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                发送纪要给：
            </th>
            <td>
                <div style="float: left">
                    <input type="text" name="SenderEmpsName" datatype="*" nullmsg="请选择发送人！" readonly="readonly" />
                    <a href="javascript:void(0)" onclick="openDialog()">添加人员</a>
                    <input type="hidden" value="" name="SenderEmps" />
                </div>
                <div style="color: Red; float: left; margin: 5px 0px 0px 10px;">
                    <span class="Validform_checktip"><span>*</span></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                记录员：
            </th>
            <td>
                <input type="text" value="<%=BP.Web.WebUser.Name%>" />
                <input type="hidden" value="<%=BP.Web.WebUser.No%>" name="Recorder" />
            </td>
        </tr>
        <tr>
            <th>
                附件：
            </th>
            <td>
                <input type="file" name="Attachment" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="button" value="添加" id="btnSubmit" onclick="DoSubmit()" style="width: 100px" />
            </td>
        </tr>
    </table>
    <input type="hidden" value="<%=ording.OID%>" name="FK_OID" />
    </form>
</body>
</html>
