﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CCOA.App.Meeting
{
    public partial class Summary : System.Web.UI.Page
    {
        public BP.OA.Meeting.RoomOrding ording = new BP.OA.Meeting.RoomOrding();
        public BP.OA.Meeting.SponsorMeeting sponsor = new BP.OA.Meeting.SponsorMeeting();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Save();
            }
            else
            {
                Show();
            }
        }

        private void Show()
        {
            string oid = Request.QueryString["oid"];
            if (!string.IsNullOrEmpty(oid))
            {
                BP.OA.Meeting.RoomOrdings ordings = new BP.OA.Meeting.RoomOrdings();
                ordings.Retrieve(BP.OA.Meeting.RoomOrdingAttr.OID, oid);

                BP.OA.Meeting.SponsorMeetings sponsors = new BP.OA.Meeting.SponsorMeetings();
                sponsors.Retrieve(BP.OA.Meeting.SponsorMeetingAttr.FK_SponsorOID, oid);
                if (ordings.Count > 0 && sponsors.Count > 0)
                {
                    ording = ordings[0] as BP.OA.Meeting.RoomOrding;
                    sponsor = sponsors[0] as BP.OA.Meeting.SponsorMeeting;
                }
            }
        }

        private void Save()
        {
            BP.OA.Meeting.MeetingSummary summary = new BP.OA.Meeting.MeetingSummary();
            BP.OA.UI.PageCommon.GetEntity(this, summary);
            HttpPostedFile files = Request.Files["Attachment"];
            summary.FileName = files.FileName;
            summary.Attachment = Common.Upload(files);
            int i = summary.Insert();
            ClientScript.RegisterStartupScript(this.GetType(), "info", "alert('会议纪要发送成功！');RefreshParent()", true);
        }

        public string GetEmpName(object obj)
        {
            return MyHelper.GetEmpName(obj);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Save();
        }
    }
}