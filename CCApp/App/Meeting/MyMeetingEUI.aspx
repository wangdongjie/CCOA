﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="MyMeetingEUI.aspx.cs" Inherits="CCOA.App.Meeting.MyMeetingEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var GetOid;
        var ccSelvalue;
        var ddSelvalue;
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";

            //系统错误
            if (js.status && js.status == 500) {
                $("body").html("<b>访问页面出错，请联系管理员。<b>");
                return;
            }
            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                     { checkbox: true },
                      { field: 'TITLE', title: '议题', width: 50, align: 'left' },
                       { field: 'COMPEREEMPNO', title: '主持人', width: 50, align: 'center' },
                     { field: 'DEPT', title: '主办部门', width: 70, align: 'center' },
                    { field: 'DATEFROM', title: '时间从', width: 70, align: 'center' },
                    { field: 'DATETO', title: '时间到', width: 70, align: 'center' },
                    { field: 'ROOM', title: '地点', width: 80, align: 'center' },
                    { field: 'STATE', title: '状态', width: 70, align: 'center', formatter: function (value, row, index) {
                        var st = "已结束";
                        switch (row.STATE) {
                            case "0":
                                st = "预订中";
                                break;
                            case "1":
                                if (row.DATEFROM > getNowFormatDate()) {
                                    st = "待召开";
                                }
                                if (row.DATEFROM < getNowFormatDate() && row.DATETO > getNowFormatDate()) {
                                    st = "正在进行";
                                }
                                break;
                            case "2":
                                st = "已取消";
                                break;
                            case "3":
                                st = "已结束";
                                break;
                        }
                        return st;
                    }
                    },
                    { field: 'SENDTOUSERS', title: '纪要', width: 70, align: 'center', formatter: function (value, row, index) {
                        var s = "<a href='javascript:void(0)' onclick=openDialog('SummaryShow.aspx?oid=" + row.OID + "')>纪要</a>";
                        return s;
                    }
                    }]],
                idField: 'OID',
                selectOnCheck: false,
                checkOnSelect: true,
                singleSelect: true,
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }
        //时间格式
        function getNowFormatDate() {
            var date = new Date();
            var seperator1 = "-";
            var seperator2 = ":";
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();

            return currentdate;
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;

            ddSelvalue = $('#dd').combobox('getValue');
            ccSelvalue = $('#cc').combobox('getValue');
            var params = {
                method: "MyMeetingMet",
                impOID: GetOid,
                pageNumber: pageNumber,
                pageSize: pageSize,
                ccSelvalue: ccSelvalue,
                ddSelvalue: ddSelvalue
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        function RefreshGrid() {
            var grid = $('#newsGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            var pageSize = options.pageSize;
            LoadGridData(curPage, pageSize);
        }
        //初始化
        $(function () {
            LoadGridData(1, 20);
        });

        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "MeetingEUI.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
        var diag = new Dialog();
        function openDialog(url) {
            diag.Width = 600;
            diag.Height = 320;
            diag.Title = "纪要";
            diag.URL = url;
            diag.show();
        }
        //删除会议---预定的还没发起的
        var GetOid;
        function delMeetingMet() {
            var rows = $('#newsGrid').datagrid('getChecked');
            if (rows.length == 0) {
                $.messager.alert("提示", "您没有选中项!", 'info');
            }
            else {
                $.messager.confirm('警告', '确定删除所选数据?', function (y) {
                    var ids = [];
                    for (var i = 0; i < rows.length; i++) {
                        if (rows[i].STATE == 0 || rows[i].STATE == 2) {
                            ids.push(rows[i].OID);
                        }
                        else {
                            $.messager.alert("警告", "只可以删除预定中和已取消的会议!", 'warning');
                            return;
                        }
                    }
                    GetOid = ids.join(',');
                    //调用
                    RefreshGrid();
                });
            }
        }
        //刷新
        function RefreshGrid() {
            var grid = $('#newsGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            var pageSize = options.pageSize;
            LoadGridData(curPage, pageSize);
        }
        var commonWin = null;
        function DoOpen() {
            //预定窗口
            var url = "Order.aspx";
            $("<div id='dialogEnPanel'></div>").append($("<iframe width='100%' height='100%' frameborder=0 src='" + url + "'/>")).dialog({
                title: "预订",
                width: 650,
                height: 480,
                autoOpen: true,
                modal: true,
                resizable: true,
                onClose: function () {
                    $("#dialogEnPanel").remove();
                },
                buttons: [{
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $('#dialogEnPanel').dialog("close");
                    }
                }]
            });
        }

        function ReFreashRoomGrid() {
            $('#dialogEnPanel').dialog("close");
            RefreshGrid();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div id="pageloading">
    </div>--%>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 6px; height: 28px;">
            <div style="float: left; margin-left: 5px;">
                会议状态：<select id="cc" class="easyui-combobox" onchange="change()" name="dept" style="width: 100px;"
                    editable="false">
                    <option value="请选择" selected="selected">请选择...</option>
                    <option value="预定中">预定中</option>
                    <option value="待召开">待召开</option>
                    <option value="正在进行">正在进行</option>
                    <option value="已结束">已结束</option>
                    <option value="已取消">已取消</option>
                </select>&nbsp;&nbsp;&nbsp;&nbsp; 我的会议：<select id="dd" class="easyui-combobox" onchange="change()"
                    name="dept" style="width: 100px;" editable="false">
                    <option value="3" selected="selected">请选择...</option>
                    <option value="0">我预定的</option>
                    <option value="1">我发起的</option>
                    <option value="2">我参与的</option>
                </select>
            </div>
            <a id="DoQueryByKey" style="float: left; margin-left: 20px;" href="#" class="easyui-linkbutton"
                data-options="plain:true,iconCls:'icon-search'" onclick="LoadGridData(1, 20)">查询</a>
            <a id="reserveMeeting" href="#" onclick="DoOpen()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">
                预订</a> <a id="delMeeting" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                    onclick="delMeetingMet()">删除</a>
        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
       <%-- <div id="dialog" style="position: absolute; width: 10%;">
        </div>--%>
    </div>
</asp:Content>
