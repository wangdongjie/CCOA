﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoomList.aspx.cs" Inherits="CCOA.App.Meeting.RoomList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加会议室资源
        var diag = new Dialog();

        function openDialog(url, title, height, width) {
            diag.Width = width;
            diag.Height = height;
            diag.Title = title;
            diag.URL = url;
            diag.show();
        }
//        var win = new window();
        function openWin(url, title, height, width) {
            
            window.showModalDialog("AddRoom.aspx", null, "dialogHeight:" + height + ";dialogWidth: " + width + ";");
            window.location.href = location.href;
        }
        function DoRefresh() {
            window.location.reload()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h3 style="background-image: url( ../../Images/title.png); margin: 0px;">
        <a href="javascript:void(0)" onclick="openWin('AddRoom.aspx','添加会议室',320,600)">添加会议室</a>
        <a href="ResList.aspx">添加会议室资源</a> <a href="MeetingType.aspx">会议类型管理</a>
    </h3>
    <div>
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table class="easyui-datagrid">
                    <thead>
                        <tr>
                            <th data-options="field:'name'">
                                会议室名称
                            </th>
                            <th data-options="field:'num'">
                                容纳人数
                            </th>
                            <th data-options="field:'admin'">
                                会议室管理员
                            </th>
                            <th data-options="field:'note'">
                                会议室资源描述
                            </th>
                            <th data-options="field:'state'">
                                状态
                            </th>
                            <th data-options="field:'info'">
                                会议室说明
                            </th>
                            <th data-options="field:'operate'">
                                操作
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem ,"Name")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem ,"Capacity")%>
                    </td>
                    <td>
                        <%#GetEmpName(DataBinder.Eval(Container.DataItem, "FK_Emp"))%>
                    </td>
                    <td>
                        <%#GetResName(DataBinder.Eval(Container.DataItem, "MTResource"))%>
                    </td>
                    <td>
                        <%#(DataBinder.Eval(Container.DataItem ,"RoomSta"))%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem ,"Note")%>
                    </td>
                    <td>
                        <a href="javascript:openDialog('AddRoom.aspx?No=<%#Eval("No")%>','修改会议室',320,600)">修改</a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody></table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
