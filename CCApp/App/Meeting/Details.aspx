﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="CCOA.App.Meeting.Details" %>

<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form2" runat="server">
    <h3 style="background-image: url( ../../Images/title.png); margin: 0px;">
        会议室预订详情：</h3>
    <table class="easyui-datagrid">
        <thead>
            <tr>
                <th data-options="field:'title'">
                    标题
                </th>
                <th data-options="field:'orderEmp'">
                    预定人
                </th>
                <th data-options="field:'orderDept'">
                    预定部门
                </th>
                <th data-options="field:'BeginTime'">
                    开始时间
                </th>
                <th data-options="field:'EndTime'">
                    结束时间
                </th>
                <th data-options="field:'state'">
                    状态
                </th>
                <th data-options="field:'resourse'">
                    所用资源
                </th>
                <th data-options="field:'operate'">
                    操作
                </th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Eval("Title")%>
                        </td>
                        <td>
                            <%#Eval("OrderName")%>
                        </td>
                        <td>
                            <%#Eval("deptName")%>
                        </td>
                        <td>
                            <%#Eval("DateFrom")%>
                        </td>
                        <td>
                            <%#Eval("DateTo")%>
                        </td>
                        <td>
                            <span id='<%#"span"+Eval("OID")%>'>
                                <%#GetState(Eval("DateFrom"),Eval("DateTo"),Eval("State"))%>
                            </span>
                        </td>
                        <td>
                            <%#GetResourceName(Eval("MTResource"))%>
                        </td>
                        <td>
                            <a href="javascript:void(0)" onclick="Sponsor('<%#Eval("OID")%>')">会议发起</a> <a href="javascript:void(0)"
                                onclick="Summary('<%#Eval("OID")%>')">会议纪要 </a><a href="javascript:void(0)" onclick="Modify('<%#Eval("OID")%>')">
                                    会议变更</a> <a href="javascript:void(0)" onclick="Stop('<%#Eval("OID")%>')">结束会议</a>
                            <a href="javascript:void(0)" onclick="Cancel('<%#Eval("OID")%>')">取消会议 </a>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
    </form>
</body>
</html>
