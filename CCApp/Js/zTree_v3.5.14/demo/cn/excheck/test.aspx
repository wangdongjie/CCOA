﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="CCOA.Js.zTree.test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../../../css/demo.css" type="text/css">
    <link rel="stylesheet" href="../../../css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script type="text/javascript" src="../../../js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../../../js/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="../../../js/jquery.ztree.excheck-3.5.js"></script>
    <script type="text/javascript">
		<!--
        var setting = {
            check: {
                enable: true,
                chkStyle: "radio",
                radioType: "all"
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onCheck: zTreeOnCheck
            }
        };
        function zTreeOnCheck(event, treeId, treeNode) {
            alert(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);
        };
        var zNodes = [
			<%= JsonTree %>
		];

        $(document).ready(function () {
            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
        });
		//-->
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="zTreeDemoBackground left">
            <ul id="treeDemo" class="ztree">
            </ul>
        </div>
    </div>
    </form>
</body>
</html>
