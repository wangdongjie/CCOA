﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
namespace CCOA.Js.zTree
{
	public partial class test : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!this.IsPostBack)
            {
            }
		}
        public string JsonTree
        {
            get
            {
                string sSql = "select No,Name,ParentN from Port_Dept order by Idx";
                DataTable dt = null;
                if (BP.WF.Glo.OSModel == BP.Sys.OSModel.OneMore)
                    CCPortal.DA.DBAccess.RunSQLReturnTable(sSql);
                else
                    BP.DA.DBAccess.RunSQLReturnTable(sSql);
                if (dt == null) return String.Empty;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {   
                    if (sb.Length > 0) sb.Append(",");
                    string f=String.Format("id: {0}, pId: {2}, name: \"{1}\", open: true ", dr["No"], dr["Name"], dr["ParentNo"]);
                    sb.Append("{" + f + "}\r\n");
                }
                return sb.ToString();
            }
        }
        /// <summary>
        /// 对一个值的处理
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string value_Json(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            string temstr;
            temstr = value;
            temstr = temstr.Replace("{", "｛").Replace("}", "｝")
                .Replace(":", "：")
                .Replace(",", "，")
                .Replace("[", "【")
                .Replace("]", "】")
                .Replace(";", "；")
                .Replace("\n", "<br/>")
                .Replace("\r", "");

            temstr = temstr.Replace("\t", "   ");
            temstr = temstr.Replace("'", "\'");
            temstr = temstr.Replace(@"\", @"\\");
            temstr = temstr.Replace("\"", "\"\"");
            return temstr;
        }
	}
}