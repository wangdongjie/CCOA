﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="CCOA.Js_ExtJs.examples.calendar.Index" ResponseEncoding="GBK" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/examples.css" />
    <script type="text/javascript" src="../../ext-all.js"></script>
    <script type="text/javascript" src="../../locale/ext-lang-zh_CN.js"></script>
    <script type="text/javascript">
        var loadEvents = {"evts":<%=userCalendars %>};
        Ext.Loader.setConfig({
            enabled: true,
            paths: {
                'Ext.calendar': 'src'
            }
        });
        Ext.require([
        //'Ext.diag.layout.Context',
        //'Ext.diag.layout.ContextItem',
            'Ext.calendar.App'
        ]);
        Ext.onReady(function () {
            // launch the app:
            Ext.create('Ext.calendar.App');
            document.getElementById('logo-body').innerHTML =new Date().getDate();
            document.getElementById('app-calendar-tb-month-btnInnerEl').innerHTML="月历";
            document.getElementById('app-calendar-tb-week-btnInnerEl').innerHTML="周历";
            document.getElementById('app-calendar-tb-day-btnInnerEl').innerHTML="日历";
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="display:none;">
    <div id="app-header-content">
        <div id="app-logo">
            <div class="logo-top">&nbsp;</div>
            <div id="logo-body">&nbsp;</div>
            <div class="logo-bottom">&nbsp;</div>
        </div>
        <h1>我的日程</h1>
        <span id="app-msg" class="x-hidden"></span>
    </div>
    </div>
    </form>
</body>
</html>
