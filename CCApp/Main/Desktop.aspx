﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="Desktop.aspx.cs" Inherits="CCOA.Main.Desktop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <meta http-equiv="refresh" content="60" />
    <style type="text/css">
        table.main
        {
            border-collapse: separate;
            border: none 1px #333;
            width: 100%;
        }
        table.main td.col
        {
            overflow: hidden;
            vertical-align: top;
            border: solid 1px #999;
        }
        table.main div.title
        {
            height: 30;
            background-color: #eee;
            padding: 3px 3px 3px 3px;
        }
        
        table.main ul
        {
            line-height: 25px;
            font-size: 13px;
        }
    </style>
    <script src="../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        function WinOpenIt(text, url) {
            var winWidth = 850;
            var winHeight = 680;
            if (screen && screen.availWidth) {
                winWidth = screen.availWidth;
                winHeight = screen.availHeight;
            }
            var arguemnts = new Object();
            arguemnts.window = window;
            window.showModalDialog(url, arguemnts, "scrollbars=yes;resizable=yes;center=yes;dialogWidth=" + winWidth + ";dialogHeight=" + winHeight + ";dialogTop=50px;dialogLeft=50px;");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <table class="main" cellspacing="5px" cellpadding="5px">
        <%
            int rowsCount = (int)Math.Ceiling((double)this.RepeatCellCount / this.RepeatCount);
            int cellNo = 0;
            for (int i = 0; i < rowsCount; i++)
            {   
        %><tr>
            <%
                for (int j = 0; j < this.RepeatCount; j++)
                {
            %><td class="col" style="width: <%=this.RepeatColWidth %>; height: <%=this.RepeatColHeight %>">
                <%
                    if (cellNo == 0)
                    { 
                %>
                <div class="title">
                    <div style="float: left; width: 80px;">
                        公告</div>
                    <div style="float: right; width: 80px; text-align: right;">
                        <a href="javascript:parent.window.addTab('公告阅读','/App/Notice/MyNoticeList.aspx','')">
                            更多..</a></div>
                    <div style="clear: both">
                    </div>
                </div>
                <div style="padding: 5px 5px 5px 5px;">
                    &nbsp;&nbsp;<asp:Label runat="server" ID="lblNotice_Msg"></asp:Label>
                    <ul>
                        <asp:Repeater runat="server" ID="rptNotice">
                            <ItemTemplate>
                                <li><a style='<%# Convert.ToString(Eval("SetTop"))!=""?"color:red;": ""%>' href="javascript:zDialog_open0('/App/Notice/NoticeBrowser.aspx?OID=<%# Eval("No") %>','通知阅读情况',600,600,false)">
                                    <%# Eval("Name") %></a> (<%# Eval("ReadList") %>) (<%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %>)
                                    &nbsp;&nbsp;
                                    <%# Convert.ToInt32(Eval("ReadState"))==0?"<img alt='新邮件' src='Img/email_new.gif'>":"" %>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <%
                    }
                                   else if (cellNo == 1)
                                   { 
                %><div class="title">
                    <div style="float: left; width: 50px;">
                        邮件</div>
                    <div style="float: right; width: 100px;">
                        <a href="/App/Message/SendMsg.aspx">写邮件</a>&nbsp;&nbsp; <a href="javascript:parent.window.addTab('邮件阅读','/App/Message/InBox.aspx','')">
                            更多..</a>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
                <div style="padding: 5px 5px 5px 5px;">
                    &nbsp;&nbsp;<asp:LinkButton runat="server" ID="lbEmail_All" Text="所有" OnClick="lbEmail_All_Click"></asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton runat="server" ID="lbEmail_NotRead" Text="未读" OnClick="lbEmail_NotRead_Click"></asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton
                        runat="server" ID="lbEmail_Read" Text="已读" OnClick="lbEmail_Read_Click"></asp:LinkButton>
                    <ul>
                        <asp:Repeater runat="server" ID="rptEmail">
                            <ItemTemplate>
                                <li>
                                    <%# this.GetNameByUserNo(Eval("Sender"))%>：<a href="javascript:zDialog_open0('/App/Message/InDetail.aspx?OID=<%# Eval("No") %>','我的邮件',800,650,false)">
                                        <%# Eval("Name") %></a> (<%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %>)
                                    <%# Convert.ToInt32(Eval("Received"))==0?"<img alt='新邮件' src='Img/email_new.gif'>":"" %>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <%
                    }
                                   else if (cellNo == 2)
                                   { 
                %><div class="title">
                    <div style="float: left; width: 80px;">
                        新闻</div>
                    <div style="float: right; width: 80px; text-align: right;">
                        <a href="javascript:parent.window.addTab('新闻阅读','/App/News/MyNewsList.aspx','')">更多..</a>
                    </div>
                    <div style="clear: both">
                    </div>
                </div>
                <div style="padding: 5px 5px 5px 5px;">
                    &nbsp;&nbsp;<asp:Label runat="server" ID="lblNews_Msg"></asp:Label>
                    <ul>
                        <asp:Repeater runat="server" ID="rptNews">
                            <ItemTemplate>
                                <li><a style='<%# Convert.ToString(Eval("SetTop"))!=""?"color:red;": ""%>' href="javascript:parent.window.addTab('新闻阅读','<%# String.Format("/App/News/NewsArticle.aspx?ID={0}",Eval("No")) %>','')">
                                    <%# Eval("Name") %></a> (<%# Eval("BrowseCount") %>) (<%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %>)
                                    &nbsp;&nbsp;<%# BP.OA.Main.GetTimeImg(Eval("RDT"),2,"新邮件","Img/email_new.gif",0,0) %></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <%
                    }
                                   else if (cellNo == 3)
                                   {     
                %><div class="title">
                    待办</div>
                <div style="padding: 5px 5px 5px 5px;">
                    &nbsp;&nbsp;<asp:Label runat="server" ID="lblToDo_Msg"></asp:Label>
                    <ul>
                        <asp:Repeater runat="server" ID="rptToDo">
                            <ItemTemplate>
                                <li><a href="javascript:WinOpenIt('待办工作','/WF/MyFlow.aspx?WorkID=<%# Eval("No") %>&FK_Flow=<%# Eval("FK_Flow") %>&FK_Node=<%# Eval("FK_Node") %>&FID=<%# Eval("FID") %>&AtPara=<%# Eval("AtPara") %>');">
                                    <%# Eval("Name") %></a> (<%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %>)</li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <%                              
                    }                                  
                %>
            </td>
            <%
                cellNo++;
                }
            %></tr>
        <%
}  
        %>
    </table>
</asp:Content>
