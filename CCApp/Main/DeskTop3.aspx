﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeskTop3.aspx.cs" Inherits="CCOA.Main.DeskTop3" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/main3.css" rel="stylesheet" type="text/css" />
    <link href="js/lunbo/css/yx_rotaion.css" rel="stylesheet" />
    <script src="../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="js/ui/ui.core.min.js" type="text/javascript"></script>
    <script src="js/ui/ui.sortable.js" type="text/javascript"></script>
    <script src="js/Jh.js" type="text/javascript"></script>
    <script src="../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
        var DATA = <%=jsonStringData%>       
            Jh.Portal.init(DATA);
        });
        function cc() {
            Jh.Portal.eventMin();
            $("#hideDiv").hide();
            $("#showDiv").show();
        }
        function dd() {
            Jh.Portal.eventMax();
            $("#hideDiv").show();
            $("#showDiv").hide();
        }
        function ee() {
            Jh.Portal.saveInfo();
            location.reload();
        }
        function setDeskTop() {
            //$('#dlg').dialog('open').dialog('setTitle', '模块显示设置');                       
            $('#dlg').dialog({
                title: "模块显示设置",
                closed: false,
                modal: true
            });
        }
        function saveDeskTopModular() {
            var chenkedBox = $("#deskTopModular:checked").val([]);
            var chenkedBoxValue = "";
            for (var i = 0; i < chenkedBox.length; i++) {
                chenkedBoxValue += chenkedBox[i].value + ",";
            }
            $('#dlg').dialog('close');
            var param = { method: 'updateDesktopIsView', NoStr: chenkedBoxValue.substring(0, chenkedBoxValue.length - 1) };
            $.post("desktop/updateDeskTop.aspx", param, function (data) {
                alert(data);
                location.reload();
            });
        }
                       
    </script>
    <style type="text/css">
        .nav_desktop
        {
            float: right;
            margin-left: 10px;
            cursor: pointer;
            width: 100%;
            background: none repeat scroll 0 0 #deebfe;
            border-radius: 2px 2px 2px 2px;
            border-bottom: 1px solid #25283f;
            color: #40649c;
            font-size: 12px;
            font-weight: bold;
            height: 25px;
            line-height: 25px;
            padding-top: 3px;
            padding-bottom: 3px;
            padding-left: 10px;
            text-align: right;
        }
        #modular tr td
        {
            width: 200px;
            height: 40px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的工作台</title>
</head>
<body>
    <div class="nav_desktop" onclick="">
        <a href="#" id="hideDiv" onclick="cc();">
            <img src="Img/icon/arrom.gif" />收缩所有</a> <a href="#" id="showDiv" onclick="dd();"
                style="display: none;">
                <img src="Img/icon/arrow.gif" />展开所有</a> <a href="#" onclick="ee();">
                    <img src="Img/icon/filesave.png" />保存模块设置</a> <a href="#" onclick="setDeskTop();">
                        <img src="img/icon/ico_settings.gif" title="系统设置" style="text-align: center" />系统设置</a>
    </div>
    <br /><br />
    <div id="dlg" class="easyui-dialog" style="width: 400px; height: auto; max-height: 400px;
        overflow: auto" closed="true" buttons="#dlg-buttons">
        <table id="modular">
            <%=treejsonStr %>
        </table>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveDeskTopModular()">
            保存</a> <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#dlg').dialog('close')">
                取消</a>
    </div>
</body>
</html>
