﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="DesktopOA.aspx.cs" Inherits="CCOA.Main.DesktopOA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <meta http-equiv="refresh" content="60" />
    <script src="../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="js/bRoundCurve.js" type="text/javascript"></script>
    <link href="css/desktop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function MouseChangeImg(img, imgUrl, model) {
            img.src = imgUrl;
        }
        function WinOpenIt(text, url) {
            var winWidth = 850;
            var winHeight = 680;
            if (screen && screen.availWidth) {
                winWidth = screen.availWidth;
                winHeight = screen.availHeight - 36;
            }
            window.open(url, "_blank", "height=" + winHeight + ",width=" + winWidth + ",top=0,left=0,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <table class="main" cellspacing="5px" cellpadding="5px">
        <%
            int rowsCount = (int)Math.Ceiling((double)this.RepeatCellCount / this.RepeatCount);
            int cellNo = 0;
            for (int i = 0; i < rowsCount; i++)
            {   
        %><tr>
            <%
                for (int j = 0; j < this.RepeatCount; j++)
                {
            %><td class="col" style="width: <%=this.RepeatColWidth %>; height: <%=this.RepeatColHeight %>">
                <%
                    if (cellNo == 0)
                    { 
                %>
                <div class="xg_top" style="height: <%=this.RepeatColHeight %>">
                    <h1>
                        &nbsp;&nbsp;公告<span class="more"> <a href="javascript:parent.window.addTab('公告阅读','/App/Notice/MyNoticeList.aspx','')">
                            <b>更多..</b></a> </span>
                    </h1>
                    <div style="margin: 0px;">
                        <ul class="list_1">
                            &nbsp;&nbsp;<asp:Label runat="server" ID="lblNotice_Msg"></asp:Label>
                            <asp:Repeater runat="server" ID="rptNotice">
                                <ItemTemplate>
                                    <li><a style='<%# Convert.ToString(Eval("SetTop"))!=""?"color:black;": ""%>' href="javascript:parent.window.addTab('公告阅读','/App/Notice/NoticeBrowser.aspx?OID=<%# Eval("No") %>','')">
                                        <%# Eval("Name") %></a>&nbsp;&nbsp;<%# Convert.ToInt32(Eval("ReadState"))==0?"<img alt='新公告' src='Img/email_new.gif'>":"" %>
                                        <span>
                                            <%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %></span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <%
                    }
                    else if (cellNo == 1)
                    { 
                %>
                <div class="xg_top" style="height: <%=this.RepeatColHeight %>">
                    <h1>
                        &nbsp;&nbsp;邮件<span class="more"><a href="javascript:parent.window.addTab('邮件阅读','/App/Message/InBox.aspx','')"><b>更多..</b></a>
                        </span>
                    </h1>
                    <div style="margin: 0px;">
                        <ul class="list_1">
                            &nbsp;&nbsp;<asp:LinkButton runat="server" ID="lbEmail_All" Text="所有" OnClick="lbEmail_All_Click"></asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton runat="server" ID="lbEmail_NotRead" Text="未读" OnClick="lbEmail_NotRead_Click"></asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton
                                runat="server" ID="lbEmail_Read" Text="已读" OnClick="lbEmail_Read_Click"></asp:LinkButton>
                            <asp:Repeater runat="server" ID="rptEmail">
                                <ItemTemplate>
                                    <li>
                                        <%# this.GetNameByUserNo(Eval("Sender"))%>：<a href="javascript:parent.window.addTab('邮件','/App/Message/InDetail.aspx?OID=<%# Eval("No") %>','')">
                                            <%# Eval("Name") %></a><%# Convert.ToInt32(Eval("Received"))==0?"<img alt='新邮件' src='Img/email_new.gif'>":"" %>
                                        <span>
                                            <%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %></span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <%
                    }
                    else if (cellNo == 2)
                    { 
                %>
                <div class="xg_top" style="height: <%=this.RepeatColHeight %>">
                    <h1>
                        &nbsp;&nbsp;新闻<span class="more"> <a href="javascript:parent.window.addTab('新闻阅读','/App/News/MyNewsList.aspx','')">
                            <b>更多..</b></a> </span>
                    </h1>
                    <div style="margin: 0px;">
                        <ul class="list_1">
                            &nbsp;&nbsp;<asp:Label runat="server" ID="lblNews_Msg"></asp:Label>
                            <asp:Repeater runat="server" ID="rptNews">
                                <ItemTemplate>
                                    <li><a href="javascript:parent.window.addTab('新闻阅读','<%# String.Format("/App/News/NewsArticle.aspx?ID={0}",Eval("No")) %>','')">
                                        <%# Eval("Name") %></a> &nbsp;&nbsp;<%# BP.OA.Main.GetTimeImg(Eval("RDT"),2,"新邮件","Img/email_new.gif",0,0) %>
                                        <span>
                                            <%# String.Format("{0:yyyy-MM-dd}",Convert.ToDateTime(Eval("RDT"))) %></span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <%
                    }
                    else if (cellNo == 3)
                    {     
                %>
                <div class="xg_top" style="height: <%=this.RepeatColHeight %>">
                    <h1>
                        待办<span class="more"></span>
                    </h1>
                    <div style="margin: 0px;">
                        <ul class="list_1">
                            &nbsp;&nbsp;<asp:Label runat="server" ID="lblToDo_Msg"></asp:Label>
                            <asp:Repeater runat="server" ID="rptToDo">
                                <ItemTemplate>
                                    <li><a href="javascript:WinOpenIt('待办工作','/WF/MyFlow.aspx?WorkID=<%# Eval("No") %>&FK_Flow=<%# Eval("FK_Flow") %>&FK_Node=<%# Eval("FK_Node") %>&FID=<%# Eval("FID") %>')">
                                        <%# Eval("Name") %></a> <span>
                                            <%# Convert.ToDateTime(Eval("RDT")).ToString("yyyy-MM-dd") %></span> </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <%                              
                    }                                  
                %>
            </td>
            <%
                    cellNo++;
                }
            %></tr>
        <%
            }  
        %>
    </table>
    <script type="text/javascript">
        b_RoundCurve("xg_top", "#80d5ff", "", 4); //圆角背景图片
    </script>
</asp:Content>
