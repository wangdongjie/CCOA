﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="UserConfig.aspx.cs" Inherits="CCOA.Main.Sys.UserConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        input[type='text']{ border:solid 1px #aaa; text-align:center;}
        p.title{ font-weight:bolder; font-size:14px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="padding-left: 10px;">
        <p class="title">
            我的桌面信息行数设置</p>
        <ul>
            <li>共有&nbsp;<asp:TextBox ID="tbRepeatCellCount" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;个信息块</li>
            <li>每个信息块宽度&nbsp;<asp:TextBox ID="tbRepeatColWidth" runat="server" Width="50px" BorderWidth="1px"
                MaxLength="5" BorderStyle="Dashed"></asp:TextBox></li>
            <li>每个信息块高度&nbsp;<asp:TextBox ID="tbRepeatColHeight" runat="server" Width="50px"
                BorderWidth="1px" MaxLength="5" BorderStyle="Dashed"></asp:TextBox></li>
            <li>桌面每行&nbsp;<asp:TextBox ID="tbRepeatCount" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;栏</li>
            <li>公告&nbsp;<asp:TextBox ID="tbRowsCount_Notice" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;条</li>
            <li>新闻&nbsp;<asp:TextBox ID="tbRowsCount_News" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;条</li>
            <li>邮件&nbsp;<asp:TextBox ID="tbRowsCount_Email" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;条</li>
            <li>待办&nbsp;<asp:TextBox ID="tbRowsCount_ToDo" runat="server" Width="30px" BorderWidth="1px"
                MaxLength="2" BorderStyle="Dashed"></asp:TextBox>&nbsp;条</li>
        </ul>
        <div style="text-align: center; width: 240px; border-top: dashed 1px #777; padding-top: 3px;">
            <asp:Button runat="server" ID="btnMyDesktopSet" Text="设置" OnClick="btnMyDesktopSet_Click" /></div>
    </div>
</asp:Content>
