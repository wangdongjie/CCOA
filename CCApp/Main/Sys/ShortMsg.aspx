﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="ShortMsg.aspx.cs" Inherits="CCOA.Main.Sys.ShortMsg" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/icon.css">
    <link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
    <script src="/js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var name;
        var textval;
        $(function () {
            $('#div_SendBox').dialog({ autoOpen: false });
        });
        function OpenDlg(userNo) {
            $("#<%=hfUserNo.ClientID%>").val(userNo);
            $('#div_SendBox').dialog('open');
        }
        function ClickRow(row) {
            OpenDlg(row);
        }

        function SendMsgAndClose() {
            var userNo = $("#<%=hfUserNo.ClientID%>").val();
            var text = $("#<%=tbText.ClientID%>").val();
            if (userNo == null || userNo == "") {
                alert("用户竟然为空,出错,请管理员!"); return;
            }
            if (text == null || text == "") {
                alert("发送文本不能为空!"); return;
            }
            $.ajax({
                type: "GET",
                url: "/App/Serv/SendMsg.ashx?rand=" + Math.random(),
                dataType: 'html',
                type: 'post',
                data: { "userNo": userNo, "text": text },
                success: function (data) {
                    if (data == "SUCCESS") {
                        $("#<%=tbText.ClientID%>").val('');
                        $("#<%=hfUserNo.ClientID%>").val('');
                        $('#div_SendBox').dialog('close');
                    }
                    else {
                        alert("不明错误,请查看!");
                    }
                }
            });
        }
        //限定输入字数,多输入部分被截断.
        function checkLength(which) {
            var maxChars = 50; 
            if (which.value.length > maxChars) {
                $.messager.alert("提示","您的输入字数限定为"+maxChars+"字!","info");
                // 超过限制的字数了就将 文本框中的内容按规定的字数 截取
                which.value = which.value.substring(0, maxChars);
                return false;
            }
            else {
                var curr = maxChars - which.value.length; //50 减去 当前输入的
                document.getElementById("sy").innerHTML = curr.toString();
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="padding-left: 20px;" dir="ltr">
        <h3>
            <asp:Repeater runat="server" ID="rptMsgLabel">
                <ItemTemplate>
                    <a href="?Type=<%# Eval("SubjectTitle") %>" style="<%# Convert.ToString(Eval("SubjectTitle"))==this.rType?"text-decoration:underline;": "text-decoration:none;"%>">
                        <%#String.Format("{0}({1})",Eval("SubjectTitle"),Eval("CC")) %></a></ItemTemplate>
                <SeparatorTemplate>
                    &nbsp;|&nbsp;</SeparatorTemplate>
            </asp:Repeater>
        </h3>
        <div style="text-align: right; margin-bottom: 2px;">
            <asp:Button runat="server" OnClick="Del" Text="清除" ID="btnDel" OnClientClick="return confirm('你确定要清除当前类型吗？')" />&nbsp;<asp:Button
                runat="server" OnClick="DelAll" Text="清除所有类型" ID="btnDelAll" OnClientClick="return confirm('你确定要清除所有消息吗？')" /></div>
        <asp:GridView ID="GridView1" runat="server" CellPadding="3" CellSpacing="0" GridLines="Both"
            AutoGenerateColumns="false" Width="100%" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="编号" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px">
                    <ItemTemplate>
                        <img src="img/msg.png" style="width: 15px; height: 15px;" alt="短消息" />&nbsp;&nbsp;<%# Eval("OID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="内容">
                    <ItemTemplate>
                        <span style="<%# GetReceiveFlag(Eval("Received"))%>">
                            <%# Eval("Doc") %>
                            <%# Convert.ToInt32(Eval("Received")) == 0 ? "<img src='img/msg_New.gif' alt='新短消息'>" : ""%>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="消息类型" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <%# Eval("SubjectTitle") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="来自于" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <a href="javascript:void(0)" onclick="ClickRow('<%# Eval("FK_UserNo")%>')">
                            <%# BP.OA.GPM.GetUserNames(Convert.ToString(Eval("FK_UserNo"))) %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="时间" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <span title="<%# Eval("AddTime") %>">
                            <%# BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(Eval("AddTime")),null,null) %>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <%--直接回复--%>
        <div id="div_SendBox" class="easyui-dialog" closed="true" title="回复消息" data-options="iconCls:'icon-save'"
            style="width: 400px; height: 188px; padding: 10px;">
            <asp:TextBox runat="server" ID="tbText" TextMode="MultiLine" name="message" onkeyup="checkLength(this)"
                onkeydown="checkLength(this)" Width="95%" Height="100px"></asp:TextBox>
            限制字数为：50  剩余字数：<span id="sy" style="color: Red;">50</span>
            <input type="button" value="发送" style="margin-left: 130px;" onclick='SendMsgAndClose()' />
            <asp:HiddenField runat="server" ID="hfUserNo" />
        </div>
    </div>
    <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                    PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never" OnPageChanged="AspNetPager1_PageChanged"
                    CustomInfoTextAlign="Left" LayoutType="Table">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
</asp:Content>
