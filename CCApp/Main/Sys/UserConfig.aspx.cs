﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
namespace CCOA.Main.Sys
{
    public partial class UserConfig : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData(); 
            }
        }
        private void SetConfigItem(string key, object value)
        {
            BP.Sys.UserRegedit en = new BP.Sys.UserRegedit();
            en.MyPK = BP.Web.WebUser.No + key;
            en.FK_Emp = BP.Web.WebUser.No;
            en.CfgKey = key;
            en.Vals = value.ToString();
            en.Save();
        }
        private String GetConfigItem(string key)
        {
            BP.Sys.UserRegedit en = new BP.Sys.UserRegedit();
            en.MyPK = BP.Web.WebUser.No + key;
            en.RetrieveFromDBSources();
            return en.Vals;
        }
        private void LoadData()
        {
            this.tbRepeatCellCount.Text = this.GetConfigItem("MyDeskTop_RepeatCellCount");
            this.tbRepeatColWidth.Text = this.GetConfigItem("MyDeskTop_RepeatColWidth");
            this.tbRepeatColHeight.Text = this.GetConfigItem("MyDeskTop_RepeatColHeight");
            this.tbRepeatCount.Text = this.GetConfigItem("MyDeskTop_RepeatCount");
            this.tbRowsCount_Notice.Text = this.GetConfigItem("MyDeskTop_Notice_RowsCount");
            this.tbRowsCount_Email.Text = this.GetConfigItem("MyDeskTop_Email_RowsCount");
            this.tbRowsCount_News.Text = this.GetConfigItem("MyDeskTop_News_RowsCount");
            this.tbRowsCount_ToDo.Text = this.GetConfigItem("MyDeskTop_ToDo_RowsCount");
        }
        protected void btnMyDesktopSet_Click(object sender, EventArgs e)
        {
            int c = 0;
            if (int.TryParse(this.tbRowsCount_News.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_News_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_Email.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_Email_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_ToDo.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_ToDo_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_Notice.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_Notice_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRepeatCount.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_RepeatCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRepeatCellCount.Text, out c))
            {
                this.SetConfigItem("MyDeskTop_RepeatCellCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (!String.IsNullOrEmpty(this.tbRepeatColWidth.Text))
            {
                this.SetConfigItem("MyDeskTop_RepeatColWidth", this.tbRepeatColWidth.Text);
            }
            else
            {
                BP.OA.Debug.Alert(this, "信息块宽度不能为空值！"); return;
            }
            if (!String.IsNullOrEmpty(this.tbRepeatColHeight.Text))
            {
                this.SetConfigItem("MyDeskTop_RepeatColHeight", this.tbRepeatColHeight.Text);
            }
            else
            {
                BP.OA.Debug.Alert(this, "信息块高度不能为空值！"); return;
            }
        }
    }
}