﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.DA;
namespace CCOA.Main.Sys
{
    public partial class ShortMsg : System.Web.UI.Page
    {
        #region 接口
        public string DefaultType = "短消息";
        public string rType
        {
            get
            {
                string s = Convert.ToString(this.Request.QueryString["Type"]);
                if (String.IsNullOrEmpty(s))
                    return this.DefaultType;
                else
                {
                    if (s.Length > 10) s = s.Substring(0, 9);//防攻击
                    return s;
                }
            }
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        /// <summary>
        /// 是否启用流程消息在OA中显示
        /// </summary>
        private bool ShowFlowSMSInOA
        {
            get
            {
                string showFlowSMSInOA = BP.Sys.SystemConfig.AppSettings["ShowFlowSMSInOA"];
                //如果启用流程消息在OA中显示
                if (!string.IsNullOrEmpty(showFlowSMSInOA) && showFlowSMSInOA == "1")
                    return true;
                return false;
            }
        }
        #endregion

        #region 装载
        protected void Page_Load(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterArrayDeclaration("IDArray", string.Format("'{0}','{1}'", hfUserNo.ClientID, tbText.ClientID));
            if (!this.IsPostBack)
            {
                this.LoadMsgLabel();

                this.LoadData(true);

                BP.OA.ShortMsg.ReceivedInfo(null, this.rType);

                this.btnDel.Text = "清除所有" + this.rType;
            }
        }
        private void LoadMsgLabel()
        {
            string sql = String.Format("select SubjectTitle,(Select COUNT(*) from OA_ShortMsg Where Received=0 and FK_SendToUserNo='{0}' and SubjectTitle=A.SubjectTitle) as CC from (select distinct SubjectTitle from OA_ShortMsg) A", BP.Web.WebUser.No);
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            //如果启用流程消息在OA中显示
            if (ShowFlowSMSInOA == true)
            {
                DataRow row = dt.NewRow();
                row["SubjectTitle"] = "审批";
                row["CC"] = GetFlowSmsCount();
                dt.Rows.Add(row);
            }
            this.rptMsgLabel.DataSource = dt;
            this.rptMsgLabel.DataBind();
        }
        private void LoadData(bool start)
        {
            string con = null;
            if (!String.IsNullOrEmpty(this.rType))
            {
                con = String.Format(" and SubjectTitle='{0}'", this.rType);
            }
            string sql = String.Format("SELECT OID,FK_UserNo,FK_SendToUserNo,AddTime,Doc,AttachFile,SubjectTitle,GroupRedirect,GroupRedirectFormat,SingleRedirect,SingleRedirectFormat,Received"
                     + " FROM OA_ShortMsg where FK_SendToUserNo='{0}'{1}", BP.Web.WebUser.No, con);

            if (this.rType == "审批")
            {
                sql = String.Format("select MyPK OID,Sender FK_UserNo,SendTo FK_SendToUserNo,RDT AddTime,EmailTitle Doc,'' AttachFile,"
                     + "'审批' SubjectTitle,'' GroupRedirect,'' GroupRedirectFormat,"
                     + "'' SingleRedirect,'' SingleRedirectFormat,EmailSta Received "
                     + " FROM Sys_SMS where EmailTitle is not null and SendTo='{0}'", BP.Web.WebUser.No);
            }
            if (start)
            {
                AspNetPager1.RecordCount = BP.OA.Main.GetPagedRowsCount(sql);  //第一次需要初始化
                //AspNetPager1.PageSize = 10;
                AspNetPager1.PageSize = BP.OA.Main.GetConfigItem("PageSize") == null ? 20 : Convert.ToInt32(BP.OA.Main.GetConfigItem("PageSize"));
                AspNetPager1.CurrentPageIndex = 1;
            }
            int total = DBAccess.RunSQLReturnCOUNT(sql);
            int PageCount = total % (AspNetPager1.PageSize) == 0 ? total / (AspNetPager1.PageSize) : total / (AspNetPager1.PageSize) + 1;
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, PageCount, total, this.AspNetPager1.PageSize });

            if (this.rType == "审批")
                this.GridView1.DataSource = BP.OA.Main.GetPagedRows(sql, 0, "order by AddTime desc", this.AspNetPager1.PageSize, this.AspNetPager1.CurrentPageIndex);
            else
                this.GridView1.DataSource = BP.OA.Main.GetPagedRows(sql, 0, "order by OID desc", this.AspNetPager1.PageSize, this.AspNetPager1.CurrentPageIndex);


            //this.GridView1.DataSource = BP.OA.Main.GetPagedRows(sql, 0, "order by OID desc", this.AspNetPager1.PageSize, this.AspNetPager1.CurrentPageIndex);
            //this.GridView1.DataSource = BP.DA.DBAccess.RunSQLReturnTable(sql);
            this.GridView1.DataBind();
        }
        public string GetReceiveFlag(object evalFlag)
        {
            return Convert.ToInt32(evalFlag) == 1 ?
                "font-weight:normal"
                : "font-weight:bold;";
        }
        #endregion

        #region 流程消息处理
        private string GetFlowSmsCount()
        {
            string sql = string.Format("select COUNT(*) CC from Sys_SMS where  EmailTitle is not null and EmailSta=0 and SendTo='{0}'", BP.Web.WebUser.No);
            return BP.DA.DBAccess.RunSQLReturnString(sql).ToString();
        }
        #endregion

        #region 事件
        protected void Del(object sender, EventArgs e)
        {
            string sql = String.Format("Delete FROM OA_ShortMsg Where FK_SendToUserNo='{0}' and SubjectTitle='{1}'", BP.Web.WebUser.No, this.rType);
            BP.DA.DBAccess.RunSQL(sql);
            //如果启用流程消息
            if (ShowFlowSMSInOA)
            {
                sql = String.Format("Delete FROM Sys_SMS Where SendTo='{0}'", BP.Web.WebUser.No);
                BP.DA.DBAccess.RunSQL(sql);
            }
            this.LoadData(false);
        }
        protected void DelAll(object sender, EventArgs e)
        {
            string sql = String.Format("Delete FROM OA_ShortMsg Where FK_SendToUserNo='{0}'", BP.Web.WebUser.No);
            BP.DA.DBAccess.RunSQL(sql);
            //如果启用流程消息
            if (ShowFlowSMSInOA)
            {
                sql = String.Format("Delete FROM Sys_SMS Where SendTo='{0}'", BP.Web.WebUser.No);
                BP.DA.DBAccess.RunSQL(sql);
            }
            this.LoadData(false);
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            if (this.GridView1.Rows.Count > 0)
                this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            this.LoadData(false);
        }
        #endregion
        protected void Send(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string userName = this.hfUserNo.Value;
            string text = this.tbText.Text;
            Response.Write(userName + "|" + text);
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //    if (e.Row.RowType == DataControlRowType.DataRow)
            //        //e.Row.Attributes.Add("onclick", "ClickRow(this);");
            //        e.Row.Attributes.Add("onclick", "ClickRow('" + e.Row.Cells[4].Text.Trim().ToString() + "')");
            //    //e.Row.Attributes.Add("onclick", "ClientScript.RegisterStartupScript(ClientScript.GetType(), 'myscript', '')");
        }
    }
}