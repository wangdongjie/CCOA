﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.Main.Sys
{
    public partial class DefaultSetting : BP.Web.WebPage
    {
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        public int rGroup
        {
            get { return Convert.ToInt32(this.Request.QueryString["Group"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BP.Web.WebUser.No != "admin")
            {
                bool allowUserConfig = Convert.ToBoolean(BP.OA.Main.GetConfigItem("MyDeskTop_AllowUserConfig"));
                if (!allowUserConfig)
                {
                    this.ToCommMsgPage("目前系统不允许桌面个性化！");
                }
                else
                {
                    this.Response.Redirect("UserConfig.aspx", true);
                }
                return;
            }
            if (!this.IsPostBack)
            {
                Panel pnCur = (Panel)this.pn0.Parent.FindControl(String.Format("pn{0}", this.rGroup));
                pnCur.Visible = true;

                this.LoadData(); 
            }
        }
        private void LoadData()
        {
            this.cbAllowUserConfig.Checked = Convert.ToBoolean(BP.OA.Main.GetConfigItem("MyDeskTop_AllowUserConfig"));

            this.tbRepeatCellCount.Text = BP.OA.Main.GetConfigItem("MyDeskTop_RepeatCellCount");
            this.tbRepeatColWidth.Text = BP.OA.Main.GetConfigItem("MyDeskTop_RepeatColWidth");
            this.tbRepeatColHeight.Text = BP.OA.Main.GetConfigItem("MyDeskTop_RepeatColHeight");
            this.tbRepeatCount.Text = BP.OA.Main.GetConfigItem("MyDeskTop_RepeatCount");
            this.tbRowsCount_Notice.Text = BP.OA.Main.GetConfigItem("MyDeskTop_Notice_RowsCount");
            this.tbRowsCount_Email.Text = BP.OA.Main.GetConfigItem("MyDeskTop_Email_RowsCount");
            this.tbRowsCount_News.Text = BP.OA.Main.GetConfigItem("MyDeskTop_News_RowsCount");
            this.tbRowsCount_ToDo.Text = BP.OA.Main.GetConfigItem("MyDeskTop_ToDo_RowsCount");

            this.imgLoginImage.ImageUrl = BP.OA.Main.GetConfigItem("MainBoard_LoginImage");
            this.imgComLogo.ImageUrl = BP.OA.Main.GetConfigItem("MainBoard_CompanyLogo");
        }

        protected void btnSetCompLogo_Click(object sender, EventArgs e)
        {
            string logo = BP.OA.Main.GetConfigItem("MainBoard_CompanyLogo");
            if (this.fuCompLogo.HasFile)
            {
                this.fuCompLogo.SaveAs(this.Server.MapPath(logo));
            }
            this.imgComLogo.ImageUrl = logo;
        }

        protected void btnSetLoginImage_Click(object sender, EventArgs e)
        {
            string logo = BP.OA.Main.GetConfigItem("MainBoard_LoginImage");
            if (this.fuLoginImage.HasFile)
            {
                this.fuLoginImage.SaveAs(this.Server.MapPath(logo));
            }
            this.imgLoginImage.ImageUrl = logo;
        }

        protected void btnMyDesktopSet_Click(object sender, EventArgs e)
        {
            BP.OA.Main.SetConfigItem("MyDeskTop_AllowUserConfig",this.cbAllowUserConfig.Checked.ToString());
            int c = 0;
            if (int.TryParse(this.tbRowsCount_News.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_News_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_Email.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_Email_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_ToDo.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_ToDo_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRowsCount_Notice.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_Notice_RowsCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRepeatCount.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_RepeatCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (int.TryParse(this.tbRepeatCellCount.Text, out c))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_RepeatCellCount", c);
            }
            else
            {
                BP.OA.Debug.Alert(this, "必须为数字型！"); return;
            }
            if (!String.IsNullOrEmpty(this.tbRepeatColWidth.Text))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_RepeatColWidth", this.tbRepeatColWidth.Text);
            }
            else
            {
                BP.OA.Debug.Alert(this, "信息块宽度不能为空值！"); return;
            }
            if (!String.IsNullOrEmpty(this.tbRepeatColHeight.Text))
            {
                BP.OA.Main.SetConfigItem("MyDeskTop_RepeatColHeight", this.tbRepeatColHeight.Text);
            }
            else
            {
                BP.OA.Debug.Alert(this, "信息块高度不能为空值！"); return;
            }
        }
    }
}