﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnLineUsers1.aspx.cs" Inherits="CCOA.Main.Sys.OnLineUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/icon.css">
    <script src="/Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="/js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function SendMsg() {
            //$("#div_SendBox").dialog('open');
            $('#div_SendBox').dialog('open')
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h3>
        在线员工</h3>
    <div style="margin-left: 20px;">
        <asp:GridView ID="GridView1" runat="server" GridLines="Both" CellPadding="3" CellSpacing="0"
            AutoGenerateColumns="false" Width="300">
            <Columns>
                <asp:TemplateField HeaderText="在线用户">
                    <ItemTemplate>
                        <%# this.GetUserNames(Eval("UserNo")) %>
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="登录时间">
                    <ItemTemplate>
                        <span title="<%# String.Format("{0:yyyy年MM月dd日 HH:mm:ss}",Convert.ToDateTime(Eval("LoginTime"))) %>">
                            <%# BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(Eval("LoginTime")), null, null)%>
                        </span>
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发送消息">
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnSend" OnClick="Send" Text="发送" OnClientClick="return SendMsg()"
                            CommandName='<%#Eval("UserNo") %>' />
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div id="div_SendBox" class="easyui-dialog" title="Basic Dialog" data-options="iconCls:'icon-save'" style="width:400px;height:200px;padding:10px">
            <asp:TextBox runat="server" ID="tbText" TextMode="MultiLine" Width="390px" Height="390px"></asp:TextBox>
        </div>
    </div>
    </form>
</body>
</html>
