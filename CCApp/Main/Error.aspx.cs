﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.Main
{
    public partial class Error : System.Web.UI.Page
    {
        private string Msg
        {
            get { return Convert.ToString(Request.QueryString["msg"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lbMsg.Text = this.Msg;
            }
        }
    }
}