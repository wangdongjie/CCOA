﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
namespace CCOA.Login
{
    /// <summary>
    /// Login1 的摘要说明
    /// </summary>
    public class Login1 : IHttpHandler,IRequiresSessionState
    {
        #region //以下接口需要开发
        /// <summary>
        /// 错误处理接口
        /// </summary>
        /// <param name="msg"></param>
        private void RedirectToError(string msg)
        {
            //Response.Write(msg);
            BP.OA.GPM.RedirectErrorPage(msg);
        }
        /// <summary>
        /// 登录成功后，如果用户变量@Url为空，需要转到的地址
        /// </summary>
        private string LoginedMainUrl
        {
            get { return "/Main/"; }
        }
        /// <summary>
        /// 登录状态下，获取当前用户No；非登录状态下，为null。
        /// </summary>
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        /// <summary>
        /// 是否在登录状态
        /// </summary>
        private bool IsOnline
        {
            get { return BP.OA.Auth.IsOnline(); }
        }
        /// <summary>
        /// 通过用户名进行登录
        /// </summary>
        /// <param name="userNo"></param>
        /// <param name="createPersistStore"></param>
        private void LoginIn(string userNo,bool createPersistStore)
        {
            BP.OA.Auth.LoginIn(userNo, createPersistStore);
        }
        /// <summary>
        /// 注销当前用户
        /// </summary>
        private void LoginOut()
        {
            BP.OA.Auth.LoginOut();
        }
        /// <summary>
        /// 获取当前用户Auth值
        /// </summary>
        /// <param name="userNo"></param>
        /// <returns></returns>
        private string GetAuthString(string userNo)
        {
            return BP.OA.GPM.GetUserAuth(userNo);
        }
        #endregion

        #region //以下接口不需要开发
        /// <summary>
        /// 用户变量@用户编号
        /// </summary>
        private string rUserNo
        {
            get { return Convert.ToString(HttpContext.Current.Request.QueryString["UserId"]); }
        }
        /// <summary>
        /// 用户变量@Auth值
        /// </summary>
        private string rAuth
        {
            get { return Convert.ToString(HttpContext.Current.Request.QueryString["Sid"]); }
        }
        /// <summary>
        /// 用户变量@成功后需要转向的地址
        /// </summary>
        private string rUrl
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Request.QueryString["Url"]);
            }
        }
        #endregion
        //*************主处理过程************************
        public void ProcessRequest(HttpContext context)
        {
            string userNo = this.rUserNo;
            string url = this.rUrl;
            if (String.IsNullOrEmpty(userNo))
            {
                //context.Response.Write("SSO单点登录失败!(用户名为空)");
                string msg = "SSO登录失败!";
                this.RedirectToError(msg);
                return;
            }
            bool allowLogin = false;
            if (String.IsNullOrEmpty(url)) url = this.LoginedMainUrl;
            if (this.IsOnline)
            {
                if (BP.Web.WebUser.No != userNo)
                {   //已经登录了，但是其它用户，注销
                    this.LoginOut();
                    allowLogin = true;
                }
                else //已经登录了
                    allowLogin = false;
            }
            else
                allowLogin = true;

            if (!allowLogin)
            {
                context.Response.Redirect(url, true);
            }
            else
            {
                //1.验证部分  防止外来攻击，还未写
                if (this.rAuth != this.GetAuthString(userNo))
                {
                    string msg = "SSO单点登录失败!(Auth验证错误)";
                    this.RedirectToError(msg);
                }
                //2.进行登录
                this.LoginIn(userNo, false);
                context.Response.Redirect(url);
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}