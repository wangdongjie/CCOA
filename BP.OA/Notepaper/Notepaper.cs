﻿using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.Notepaper
{
    /// <summary>
    /// 记事便签属性
    /// </summary>
    public class NotepaperAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 添加人员
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 便签内容
        /// </summary>
        public const string NoteContent = "NoteContent";
        /// <summary>
        /// 日期
        /// </summary>
        public const string RDT = "RDT";
    }
    /// <summary>
    ///  记事便签
    /// </summary>
    public class Notepaper : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 添加人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(NotepaperAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(NotepaperAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 便签内容
        /// </summary>
        public string NoteContent
        {
            get
            {
                return this.GetValStrByKey(NotepaperAttr.NoteContent);
            }
            set
            {
                this.SetValByKey(NotepaperAttr.NoteContent, value);
            }
        }
        /// <summary>
        /// 便签内容
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStrByKey(NotepaperAttr.RDT);
            }
            set
            {
                this.SetValByKey(NotepaperAttr.RDT, value);
            }
        }
        #endregion 属性

        #region 构造方法
        /// <summary>
        /// 记事便签
        /// </summary>
        public Notepaper()
        {
        }
        /// <summary>
        /// 记事便签
        /// </summary>
        /// <param name="_No"></param>
        public Notepaper(string _No) : base(_No) { }

        #endregion

        /// <summary>
        /// 记事便签Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_Notepaper"; //要物理表。
                map.EnDesc = "记事便签";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";
                map.AddTBStringPK(NotepaperAttr.No, null, "编号", true, true, 10, 10, 50);
                map.AddTBString(NotepaperAttr.Name, null, "名称", true, false, 0, 200, 300, true);
                map.AddDDLEntities(NotepaperAttr.FK_Emp, null, "添加人", new BP.Port.Emps(), true);
                map.AddTBDateTime(NotepaperAttr.RDT, "记录日期", true, true);
                map.AddTBStringDoc(NotepaperAttr.NoteContent, null, "便签内容", true, false, true);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 记事便签
    /// </summary>
    public class Notepapers : EntitiesNoName
    {
        /// <summary>
        /// 记事便签s
        /// </summary>
        public Notepapers() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Notepaper();
            }
        }
    }
}
