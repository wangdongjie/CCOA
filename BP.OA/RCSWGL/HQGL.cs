﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;


namespace BP.OA.RCSWGL
{

    /// <summary>
    /// 处理状态
    /// </summary>
    public enum CLZTEnum
    {
        /// <summary>
        /// 未处理完
        /// </summary>
        WCLW,
        /// <summary>
        /// 处理完成
        /// </summary>
        CLWC
    }
    /// <summary>
    /// 类别
    /// </summary>
    public enum LB { 
        /// <summary>
        /// 固定资产
        /// </summary>
         GDZC,
        /// <summary>
        /// 后勤
        /// </summary>
        HQ
    }
    /// <summary>
    /// 后勤管理属性
    /// </summary>
    public class HQGLAttr : EntityNoNameAttr 
    {
        /// <summary>
        /// 类别
        /// </summary>
        public const string LB = "LB";
        /// <summary>
        /// 单号
        /// </summary>
        public const string Num = "Num";
        /// <summary>
        /// 上报日期
        /// </summary>
        public const string SBDate = "SBDate";
        /// <summary>
        /// 异常情况
        /// </summary>
        public const string YCQK = "YCQK";
        /// <summary>
        /// 地点
        /// </summary>
        public const string Place = "Place";
        /// <summary>
        /// 上报人
        /// </summary>
        public const string SBR = "SBR";
        /// <summary>
        /// 登记时间
        /// </summary>
        public const string DJDate = "DJDate";
        /// <summary>
        /// 后勤设施名称
        /// </summary>
        public const string HQSName = "HQSName";
        /// <summary>
        /// 后勤设施编号
        /// </summary>
        public const string HQSNo = "HQSNo";
        /// <summary>
        /// 维修负责人
        /// </summary>
        public const string WXFZR = "WXFZR";
        /// <summary>
        /// 原因
        /// </summary>
        public const string YY = "YY";
        /// <summary>
        /// 负责划分
        /// </summary>
        public const string FZHF = "FZHF";
        /// <summary>
        /// 处理意见
        /// </summary>
        public const string CLYJ = "CLYJ";
        /// <summary>
        /// 维修所需时长
        /// </summary>
        public const string VXSC = "VXSC";
        /// <summary>
        /// 完成时间
        /// </summary>
        public const string WCDate = "WCDate";
        /// <summary>
        /// 产生费用
        /// </summary>
        public const string CSFY = "CSFY";
        /// <summary>
        /// 处理状态
        /// </summary>
        public const string CLZT = "CLZT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";
        /// <summary>
        /// 上报备注
        /// </summary>
        public const string SBBZ = "SBBZ";
        

        public const string WorkId = "WorkId";
    }

    public class HQGL: EntityNoName
    {
        #region 属性
        /// <summary>
        /// 单号
        /// </summary>
        public string Num
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.Num);
            }
            set
            {
                this.SetValByKey(HQGLAttr.Num, value);
            }
        }
        /// <summary>
        /// 上报时间
        /// </summary>
        public string SBDate
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.SBDate);
            }
            set
            {
                this.SetValByKey(HQGLAttr.SBDate, value);
            }
        }
        /// <summary>
        /// 异常情况
        /// </summary>
        public string YCQK
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.YCQK);
            }
            set
            {
                this.SetValByKey(HQGLAttr.YCQK, value);
            }
        }
        /// <summary>
        /// 地点
        /// </summary>
        public string Place
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.Place);
            }
            set
            {
                this.SetValByKey(HQGLAttr.Place, value);
            }
        }
        /// <summary>
        /// 上报人
        /// </summary>
        public string SBR
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.SBR);
            }
            set
            {
                this.SetValByKey(HQGLAttr.SBR, value);
            }
        }
        /// <summary>
        /// 登记时间
        /// </summary>
        public string DJDate
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.DJDate);
            }
            set
            {
                this.SetValByKey(HQGLAttr.DJDate, value);
            }
        }
        /// <summary>
        /// 后勤设施名称
        /// </summary>
        public string HQSName
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.HQSName);
            }
            set
            {
                this.SetValByKey(HQGLAttr.HQSName, value);
            }
        }
        /// <summary>
        /// 后勤设施编号
        /// </summary>
        public string HQSNo
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.HQSNo);
            }
            set
            {
                this.SetValByKey(HQGLAttr.HQSNo, value);
            }
        }
        
        /// <summary>
        /// 产生费用
        /// </summary>
        public decimal CSFY
        {
            get {
                return this.GetValDecimalByKey(HQGLAttr.CSFY);
            }
            set {
                this.SetValByKey(HQGLAttr.CSFY,value);
            }
        }
        /// <summary>
        /// 维修负责人
        /// </summary>
        public string WXFZR {
            get {
                return this.GetValStrByKey(HQGLAttr.WXFZR);
            }
            set
            {
                this.SetValByKey(HQGLAttr.WXFZR,value);
            }
        }
        /// <summary>
        /// 原因
        /// </summary>
        public string YY
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.YY);
            }
            set
            {
                this.SetValByKey(HQGLAttr.YY, value);
            }
        }
        /// <summary>
        /// 责任划分
        /// </summary>
        public string FZHF
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.FZHF);
            }
            set
            {
                this.SetValByKey(HQGLAttr.FZHF, value);
            }
        }
        /// <summary>
        /// 处理意见
        /// </summary>
        public string CLYJ
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.CLYJ);
            }
            set
            {
                this.SetValByKey(HQGLAttr.CLYJ, value);
            }
        }
        /// <summary>
        /// 完成时间
        /// </summary>
        public String WCDate
        {
            get
            {
                return this.GetValStrByKey(HQGLAttr.WCDate);
            }
            set
            {
                this.SetValByKey(HQGLAttr.WCDate, value);
            }
        }
        /// <summary>
        /// 维修所需时长
        /// </summary>
        public decimal VXSC
        {
            get
            {
                return this.GetValDecimalByKey(HQGLAttr.VXSC);
            }
            set
            {
                this.SetValByKey(HQGLAttr.VXSC, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.Note);
            }
            set
            {
                this.SetValByKey(HQGLAttr.Note, value);
            }
        }
        /// <summary>
        /// 上报备注
        /// </summary>
        public string SBBZ
        {
            get
            {
                return this.GetValStringByKey(HQGLAttr.SBBZ);
            }
            set
            {
                this.SetValByKey(HQGLAttr.SBBZ, value);
            }
        }
        /// <summary>
        /// 上级审批
        /// </summary>
        //public String SJSP
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(HQGLAttr.SJSP);
        //    }
        //    set
        //    {
        //        this.SetValByKey(HQGLAttr.SJSP, value);
        //    }
        //}
        public Int64 WorkId
        {
            get
            {
                return this.GetValInt64ByKey(HQGLAttr.WorkId);
            }
            set
            {
                this.SetValByKey(HQGLAttr.WorkId, value);
            }
        }
       

        #endregion

        #region 枚举属性
        /// <summary>
        /// 处理状态
        /// </summary>
        public int CLZT
        {
            get
            {
                return this.GetValIntByKey(HQGLAttr.CLZT);
            }
            set
            {
                this.SetValByKey(HQGLAttr.CLZT, value);
            }
        }
        /// <summary>
        /// 处理状态名称
        /// </summary>
        public string CLZTText
        {
            get {
                return this.GetValRefTextByKey(HQGLAttr.CLZT);
            }
        }
        /// <summary>
        /// 类别
        /// </summary>
        public int LB {
            get {
                return this.GetValIntByKey(HQGLAttr.LB);
            }
            set {
                this.SetValByKey(HQGLAttr.LB,value);
            }
        }
        /// <summary>
        /// 类别名称
        /// </summary>
        public string LBText
        {
            get
            {
                return this.GetValRefTextByKey(HQGLAttr.LB);
            }
        }
       #endregion

        #region 构造方法
        /// <summary>
        /// 无参构造
        /// </summary>
        public HQGL() { }
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="_No"></param>
        public HQGL(string _No) : base(_No) { }
        #endregion
        /// <summary>
        /// 后勤管理Map
        /// </summary>
        
        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_HouQinGuanLi");
                map.EnDesc = "后勤管理";
                map.CodeStruct = "3";

                //entity     map    的存放位置
                map.DepositaryOfEntity = BP.DA.Depositary.None;
                map.DepositaryOfMap = BP.DA.Depositary.None;
                map.AddTBStringPK(HQGLAttr.No, null, "编号", true, true, 0, 100, 20);
                map.AddTBString(HQGLAttr.Num, null, "单号", true, true, 0, 100, 20);
                map.AddTBDate(HQGLAttr.SBDate, null, "上报日期", true, true);
                map.AddTBString(HQGLAttr.YCQK, null, "异常情况", true, true, 0, 100, 20);
                map.AddTBString(HQGLAttr.Place, null, "地点", true, true, 0, 100, 20);
                map.AddTBString(HQGLAttr.SBR, null, "上报人", true, true, 0, 100, 20);
                map.AddTBDate(HQGLAttr.DJDate, null, "登记时间", true, true);
                map.AddTBStringDoc(HQGLAttr.SBBZ, null, "上报备注", true, true, true);
                map.AddDDLSysEnum(HQGLAttr.LB, 0, "类别", true, true, HQGLAttr.LB,"@0=固定资产维修@1=后勤设施维修");
                map.AddTBString(HQGLAttr.HQSName, null, "后勤设施名称", true, false, 0, 100, 20, false);
                map.AddTBString(HQGLAttr.HQSNo, null, "后勤设施编号", true, false, 0, 100, 20);
                map.AddTBString(HQGLAttr.WXFZR, null, "维修负责人", true, false, 0, 100, 20);
                map.AddTBString(HQGLAttr.YY, null, "原因", true, false, 0, 100, 20);
                map.AddTBString(HQGLAttr.FZHF, null, "责任划分", true, false, 0, 100, 20);
                map.AddTBString(HQGLAttr.CLYJ, null, "处理意见", true, false, 0, 100, 20);
                map.AddTBFloat(HQGLAttr.VXSC,0,"维修所需时长（天）",true,false);
                map.AddTBDate(HQGLAttr.WCDate, null, "完成时间", true, false);
                map.AddTBMoney(HQGLAttr.CSFY, 0, "产生费用", true, false);

                map.AddDDLSysEnum(HQGLAttr.CLZT, 0, "处理状态", true, true, HQGLAttr.CLZT, "@0=已受理@1=处理完");

                map.AddTBStringDoc(HQGLAttr.Note, null, "备注", true, false, true);

                //设置查询条件
                map.AddSearchAttr(HQGLAttr.CLZT);

                this._enMap = map;
                return this._enMap;

            }
        }
        


        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }
        protected override bool beforeUpdate()
        {
            
            return base.beforeUpdate();
        }
        #endregion
 
    }
    /// <summary>
    /// 后勤管理
    /// </summary>
    public class HQGLs : EntitiesNoName
    {
        /// <summary>
        /// 后勤管理s
        /// </summary>
        public HQGLs() { }
        /// <summary>
        /// 得到它的Entity
        /// </summary>
        public override Entity GetNewEntity
        {
            get {
                return new HQGL();
            }
        }
    }
}
