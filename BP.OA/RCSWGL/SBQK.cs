﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;

namespace BP.OA.RCSWGL
{
    /// <summary>
    /// 处理状态
    /// </summary>
    public enum SCLZT
    {
        /// <summary>
        /// 未处理
        /// </summary>
        WCLW,
        /// <summary>
        /// 已受理
        /// </summary>
        YSL,
        /// <summary>
        /// 处理完成
        /// </summary>
        CLWC,
        
    }

    /// <summary>
    ///  上报情况属性
    /// </summary>
    public class SBQKAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 单号
        /// </summary>
        public const string Num = "Num";
        /// <summary>
        /// 上报日期
        /// </summary>
        public const string SBDate = "SBDate";
        /// <summary>
        /// 异常情况
        /// </summary>
        public const string YCQK = "YCQK";
        /// <summary>
        /// 地点
        /// </summary>
        public const string Place = "Place";
        /// <summary>
        /// 上报人
        /// </summary>
        public const string SBR = "SBR";
        /// <summary>
        /// 登记时间
        /// </summary>
        public const string DJDate = "DJDate";
        /// <summary>
        /// 处理状态
        /// </summary>
        public const string SCLZT = "SCLZT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        public const string WCSJ = "WCSJ";
    
    }
    /// <summary>
    /// 上报情况
    /// </summary>
    public class SBQK:EntityNoName
    {
        /// <summary>
        /// 单号属性
        /// </summary>
        public string Num {
            get {
                return this.GetValStringByKey(SBQKAttr.Num);
            }
            set {
                this.SetValByKey(SBQKAttr.Num,value);
            }
        }
        public DateTime WCSJ {
            get {
                return this.GetValDateTime(SBQKAttr.WCSJ);
            }
            set {
                this.SetValByKey(SBQKAttr.WCSJ,value);
            }
        }
        #region 构造方法
        /// <summary>
        /// 无参构造
        /// </summary>
        public SBQK() { }
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="No"></param>
        public SBQK(string No) : base(No) { }
        #endregion

        #region 枚举属性
        /// <summary>
        /// 处理状态
        /// </summary>
        public int SCLZT
        {
            get
            {
                return this.GetValIntByKey(SBQKAttr.SCLZT);
            }
            set
            {
                this.SetValByKey(SBQKAttr.SCLZT, value);
            }
        }
        /// <summary>
        /// 处理状态名称
        /// </summary>
        public string SCLZTText
        {
            get
            {
                return this.GetValRefTextByKey(SBQKAttr.SCLZT);
            }
        }
        #endregion

        /// <summary>
        /// 上报情况Map
        /// </summary>
        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_SBQK");
                map.EnDesc = "上报情况";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = BP.DA.Depositary.None;
                map.DepositaryOfMap = BP.DA.Depositary.None;

                map.AddTBStringPK(SBQKAttr.No, null, "编号", true, true, 0, 100, 20);
                map.AddTBString(SBQKAttr.Name,null,"",false,true,0,100,30);
                map.AddTBString(SBQKAttr.Num, null, "单号", true, false, 0, 100, 20);

                map.AddTBDate(SBQKAttr.SBDate, null, "上报日期", true, false);

                map.AddTBString(SBQKAttr.YCQK, null, "异常情况", true, false, 0, 100, 20);
                map.AddTBString(SBQKAttr.Place, null, "地点", true, false, 0, 100, 20);
                map.AddTBString(SBQKAttr.SBR, null, "上报人", true, false, 0, 100, 20);

                map.AddTBDate(SBQKAttr.DJDate, null, "登记时间", true, false);

                map.AddDDLSysEnum(SBQKAttr.SCLZT, 0, "处理状态", true, true, SBQKAttr.SCLZT, "@0=未处理@1=已受理@2=处理完成");
                map.AddTBDate(SBQKAttr.WCSJ,null,"完成时间",true,true);
                map.AddTBString(SBQKAttr.Note, null, "备注", true, false, 0, 100, 20);

                RefMethod rm = new RefMethod();
                rm.Title = "维修登记";
                rm.ClassMethodName = this.ToString() + ".DoVXDJ";
                map.AddRefMethod(rm);


                this._enMap = map;
                return this._enMap;
            }
        }
        public string DoVXDJ()
        {
            BP.Sys.PubClass.WinOpen("/WF/Comm/Search.aspx?EnsName=BP.OA.RCSWGL.VXDJs&FK_Num=" + this.No, 500, 600);
            return null;
        }

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }
        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.Num;
            return base.beforeUpdateInsertAction();
        }
        protected override void afterInsertUpdateAction()
        {
            HQGL temp = new HQGL();
            temp.No = this.No;
            temp.Num = this.Num;

            // 赋值其他属性
            temp.Save();

            
            base.afterInsertUpdateAction();
        }
        #endregion
    }
    /// <summary>
    /// 上报情况
    /// </summary>
    public class SBQKs : SimpleNoNames
    {
        #region 构造方法
        /// <summary>
        /// 无参构造
        /// </summary>
        public SBQKs(){}
        public override Entity GetNewEntity
        {
            get {
                return new SBQK();
            }
        }
        #endregion
    }
}
