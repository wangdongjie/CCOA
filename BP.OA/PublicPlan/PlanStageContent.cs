﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.PublicPlan
{
    /// <summary>
    /// 计划阶段反馈
    /// </summary>
    public class PlanStageContentAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 反馈人
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 记录日期
        /// </summary>
        public const String RDT = "RDT";
        /// <summary>
        /// 计划外键
        /// </summary>
        public const String FK_PlanStageContent = "FK_PlanStageContent";
        /// <summary>
        /// 开始时间
        /// </summary>
        public const String StartDate = "StartDate";
        /// <summary>
        /// 结束时间
        /// </summary>
        public const String EndDate = "EndDate";
        /// <summary>
        /// 完成目标工作量
        /// </summary>
        public const String FinishPercent = "FinishPercent";
        /// <summary>
        /// 上一时间段计划目标
        /// </summary>
        public const String TargetContent = "TargetContent";
        /// <summary>
        /// 本时间段完成工作
        /// </summary>
        public const String PerComplate = "PerComplate";
        /// <summary>
        /// 下一时间段计划完成工作
        /// </summary>
        public const String NextComplate = "NextComplate";
        /// <summary>
        /// 重难点问题及解决方法
        /// </summary>
        public const String DegreeItem = "DegreeItem";
    }
    /// <summary>
    /// 计划阶段内容存储类
    /// </summary>
    public partial class PlanStageContent : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 反馈人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 反馈人姓名
        /// </summary>
        public string FK_EmpText
        {
            get
            {
                return this.GetValRefTextByKey(PlanStageContentAttr.FK_Emp);
            }
        }
        /// <summary>
        /// 记录日期
        /// </summary>
        public DateTime RDT
        {
            get
            {
                return this.GetValDateTime(PlanStageContentAttr.RDT);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.RDT, value);
            }
        }
        /// <summary>
        /// 计划编号
        /// </summary>
        public string FK_PlanStageContent
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.FK_PlanStageContent);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.FK_PlanStageContent, value);
            }
        }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartDate
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.StartDate);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.StartDate, value);
            }
        }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.EndDate);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.EndDate, value);
            }
        }
        /// <summary>
        /// 完成百分比
        /// </summary>
        public decimal FinishPercent
        {
            get
            {
                return this.GetValDecimalByKey(PlanStageContentAttr.FinishPercent);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.FinishPercent, value);
            }
        }
        /// <summary>
        /// 本阶段完成工作
        /// </summary>
        public string TargetContent
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.TargetContent);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.TargetContent, value);
            }
        }
        /// <summary>
        /// 上一时间段计划目标
        /// </summary>
        public string PerComplate
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.PerComplate);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.PerComplate, value);
            }
        }
        /// <summary>
        /// 下一时间段计划完成工作
        /// </summary>
        public string NextComplate
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.NextComplate);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.NextComplate, value);
            }
        }
        /// <summary>
        /// 重难点问题及解决方法
        /// </summary>
        public string DegreeItem
        {
            get
            {
                return this.GetValStrByKey(PlanStageContentAttr.DegreeItem);
            }
            set
            {
                this.SetValByKey(PlanStageContentAttr.DegreeItem, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 计划阶段内容存储类
        /// </summary>
        public PlanStageContent() { }
        public PlanStageContent(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_PlanStageContent";
                map.EnDesc = "计划阶段内容存储类";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";
                map.IsAutoGenerNo = true;

                map.AddTBStringPK(PlanStageContentAttr.No, null, "编号", true, true, 10, 10, 10);
                map.AddTBString(PlanStageContentAttr.Name, null, "名称", true, false, 0, 200, 30);
                map.AddDDLEntities(PlanStageContentAttr.FK_Emp, null, "创建人", new Emps(), true);
                map.AddTBDateTime(PlanStageContentAttr.RDT, "创建日期", false, false);
                map.AddTBString(PlanStageContentAttr.FK_PlanStageContent, null, "计划外键", true, false, 0, 100, 100);
                map.AddTBDateTime(PlanStageContentAttr.StartDate, null, "开始时间", true, false);
                map.AddTBDateTime(PlanStageContentAttr.EndDate, null, "结束时间", true, false);
                map.AddTBInt(PlanStageContentAttr.FinishPercent, 0, "完成百分比", true, false);
                map.AddTBStringDoc(PlanStageContentAttr.TargetContent, null, "本阶段完成工作", true, false, true);
                map.AddTBStringDoc(PlanStageContentAttr.PerComplate, null, "上一时间段计划目标", true, false, true);
                map.AddTBStringDoc(PlanStageContentAttr.NextComplate, null, "下一时间段计划完成工作", true, false, true);
                map.AddTBStringDoc(PlanStageContentAttr.DegreeItem, null, "重难点问题", true, false, true);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///计划阶段内容存储类集合
    /// </summary>
    public class PlanStageContents : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new PlanStageContent();
            }
        }
        /// <summary>
        /// 计划阶段内容存储类集合
        /// </summary>
        public PlanStageContents()
        {
        }
    }
}
