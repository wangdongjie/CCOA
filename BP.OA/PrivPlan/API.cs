﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.PrivPlan
{
    public partial class PrivPlan
    {
        /// <summary>
        /// 当前用户是否已经开始使用计划
        /// </summary>
        public static bool IsStart
        {
            get 
            {
                string sql = String.Format("Select * from OA_PrivPlanEmp where PK_UserNo='{0}'", BP.Web.WebUser.No);
                return BP.DA.DBAccess.IsExits(sql);
            }
        }
        public static bool TodayIsPlan(String userNo,DateTime dt)
        {
            string sql = String.Format("Select * from OA_PrivPlan where FK_UserNo='{0}' and Date='{1:yyyy-MM-dd}'", userNo, dt);
            return BP.DA.DBAccess.IsExits(sql);
        }

        public static int StartPlan(string userNo)
        {
            string sql = String.Format("Insert OA_PrivPlanEmp(PK_UserNo,AddTime,Score) Values('{0}',GetDate(),0)",userNo);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sql = String.Format("Insert OA_PrivPlanEmp(PK_UserNo,AddTime,Score) Values('{0}',NOW(),0)", userNo);
            }
            return BP.DA.DBAccess.RunSQL(sql); 
        }
    }
}
