﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.PrivPlan
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class PrivPlanItemAttr : EntityOIDAttr
    {
        public const String FK_PrivPlan = "FK_PrivPlan";
        public const String AddTime = "AddTime";
        public const String Succeed = "Succeed";
        public const String Task = "Task";
        public const String Reason = "Reason";
        public const String Improve = "Improve";
        public const String Hours = "Hours";
        public const String Score = "Score";
        public const String Sort = "Sort";
        public const String PrivPlanItemType = "PrivPlanItemType";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class PrivPlanItem : EntityOID
    {
        #region 属性
        public int FK_PrivPlan
        {
            get { return this.GetValIntByKey(PrivPlanItemAttr.FK_PrivPlan); }
            set { this.SetValByKey(PrivPlanItemAttr.FK_PrivPlan, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(PrivPlanItemAttr.AddTime); }
            set { this.SetValByKey(PrivPlanItemAttr.AddTime, value); }
        }
        public int Score
        {
            get { return this.GetValIntByKey(PrivPlanItemAttr.Score); }
            set { this.SetValByKey(PrivPlanItemAttr.Score, value); }
        }
        public int Sort
        {
            get { return this.GetValIntByKey(PrivPlanItemAttr.Sort); }
            set { this.SetValByKey(PrivPlanItemAttr.Sort, value); }
        }
        public int Hours
        {
            get { return this.GetValIntByKey(PrivPlanItemAttr.Hours); }
            set { this.SetValByKey(PrivPlanItemAttr.Hours, value); }
        }
        public bool Succeed
        {
            get { return this.GetValBooleanByKey(PrivPlanItemAttr.Succeed); }
            set { this.SetValByKey(PrivPlanItemAttr.Succeed, value); }
        }
        public string Task
        {
            get { return this.GetValStrByKey(PrivPlanItemAttr.Task); }
            set { this.SetValByKey(PrivPlanItemAttr.Task, value); }
        }
        public string Reason
        {
            get { return this.GetValStrByKey(PrivPlanItemAttr.Reason); }
            set { this.SetValByKey(PrivPlanItemAttr.Reason, value); }
        }
        public string Improve
        {
            get { return this.GetValStrByKey(PrivPlanItemAttr.Improve); }
            set { this.SetValByKey(PrivPlanItemAttr.Improve, value); }
        }
        public int PrivPlanItemType
        {
            get { return this.GetValIntByKey(PrivPlanItemAttr.PrivPlanItemType); }
            set { this.SetValByKey(PrivPlanItemAttr.PrivPlanItemType, value); }
        }

        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public PrivPlanItem() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public PrivPlanItem(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_PrivPlanItem";
                map.EnDesc = "个人计划";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBInt(PrivPlanItemAttr.FK_PrivPlan, 0, "FK_PrivPlan", true, false);
                map.AddTBDateTime(PrivPlanItemAttr.AddTime, null, "添加时间", true, false);
                map.AddTBInt(PrivPlanItemAttr.Score, 0, "分数", true, false);
                map.AddTBInt(PrivPlanItemAttr.Hours, 0, "分数", true, false);
                map.AddTBInt(PrivPlanItemAttr.Sort, 0, "分数", true, false);
                map.AddBoolean(PrivPlanItemAttr.Succeed, false, "是否总结", true, false, false);
                map.AddTBStringDoc(PrivPlanItemAttr.Task, "", "计划", true, false, 10, 50, 600,5);
                map.AddTBStringDoc(PrivPlanItemAttr.Reason, "", "总结", true, false, 0, 50, 600, 5);
                map.AddTBStringDoc(PrivPlanItemAttr.Improve, "", "改进", true, false, 0, 50, 600, 5);
                map.AddDDLSysEnum(PrivPlanItemAttr.PrivPlanItemType, 1, "分数", true, false, PrivPlanItemAttr.PrivPlanItemType, "@0=可选任务@1=主要任务");
                //map.AddTBString(PrivPlanItemAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(PrivPlanItemAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class PrivPlanItems : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new PrivPlanItem();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public PrivPlanItems()
        {
        }
    }
}
