﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;
namespace BP.OA.PrivPlan
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class PrivPlanEmpAttr : EntityMyPKAttr
    {
        public const String PK_UserNo = "PK_UserNo";
        public const String AddTime = "AddTime";
        public const String Score = "Score";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public class PrivPlanEmp : EntityMyPK
    {
        #region 属性
        public string PK_UserNo
        {
            get { return this.GetValStrByKey(PrivPlanEmpAttr.PK_UserNo); }
            set { this.SetValByKey(PrivPlanEmpAttr.PK_UserNo, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(PrivPlanEmpAttr.AddTime); }
            set { this.SetValByKey(PrivPlanEmpAttr.AddTime, value); }
        }
        public int Score
        {
            get { return this.GetValIntByKey(PrivPlanEmpAttr.Score); }
            set { this.SetValByKey(PrivPlanEmpAttr.Score, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public PrivPlanEmp() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public PrivPlanEmp(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_PrivPlanEmp";
                map.EnDesc = "个人计划";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBStringPK(PrivPlanEmpAttr.PK_UserNo, null, "用户编号", true, false, 2, 50, 20);
                map.AddTBDateTime(PrivPlanEmpAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(PrivPlanEmpAttr.Score, "0", "分数", true, false, 0, 50, 50);
                
                //map.AddTBString(PrivPlanEmpAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(PrivPlanEmpAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class PrivPlanEmps : EntitiesMyPK
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new PrivPlanEmp();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public PrivPlanEmps()
        {
        }
    }
}