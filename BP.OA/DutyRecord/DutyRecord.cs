using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA
{
    public enum LeaveType
    {
        /// <summary>
        /// 出差
        /// </summary>
        ChuCai,
        /// <summary>
        /// 请假
        /// </summary>
        QingJia,
        /// <summary>
        /// 外出
        /// </summary>
        WaiChu,
    }
    /// <summary>
    /// 考勤记录属性
    /// </summary>
    public class DutyRecordAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        ///登记日期
        /// </summary>
        public const string RecordDate = "RecordDate";
        /// <summary>
        /// 类型
        /// </summary>
        public const string LeaveType = "LeaveType";
        /// <summary>
        /// 开始时间
        /// </summary>
        public const string StartTime = "StartTime";
        /// <summary>
        /// 结束时间
        /// </summary>
        public const string EndTime = "EndTime";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
        /// <summary>
        /// 地点
        /// </summary>
        public const string DiDian = "DiDian";
        /// <summary>
        /// 办理人
        /// </summary>
        public const string Recorder = "Recorder";
        #endregion
    }
    /// <summary>
    /// 考勤记录 的摘要说明。
    /// </summary>

    public class DutyRecord : EntityOID
    {
        #region 扩展属性
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime RecordDate
        {
            get
            {
                return this.GetValDateTime(DutyRecordAttr.RecordDate);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.RecordDate, value);
            }
        }

        /// <summary>
        /// 类型
        /// </summary>
        public LeaveType LeaveType
        {
            get
            {
                return (LeaveType)this.GetValIntByKey(DutyRecordAttr.LeaveType);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.LeaveType, value);
            }
        }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime
        {
            get
            {
                return this.GetValDateTime(DutyRecordAttr.StartTime);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.StartTime, value);
            }
        }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime
        {
            get
            {
                return this.GetValDateTime(DutyRecordAttr.EndTime);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.EndTime, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(DutyRecordAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.BeiZhu, value);
            }
        }
        /// <summary>
        /// 地点
        /// </summary>
        public string DiDian
        {
            get
            {
                return this.GetValStrByKey(DutyRecordAttr.DiDian);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.DiDian, value);
            }
        }
        /// <summary>
        /// 登记人
        /// </summary>
        public string Recorder
        {
            get
            {
                return this.GetValStrByKey(DutyRecordAttr.Recorder);
            }
            set
            {
                this.SetValByKey(DutyRecordAttr.Recorder, value);
            }
        }
        #endregion
        #region 公共方法
        #endregion 公共方法
        #region 构造函数
        /// <summary>
        /// 考勤记录
        /// </summary>
        public DutyRecord()
        {
        }
        public override UAC HisUAC
        {
            get
            {
                //设置权限    
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                #region 基本属性
                map.EnDBUrl =
                    new DBUrl(DBUrlType.AppCenterDSN); //要连接的数据源（表示要连接到的那个系统数据库）。
                map.PhysicsTable = "OA_DutyRecord";
                map.DepositaryOfMap = Depositary.Application;    //实体map的存放位置.
                map.DepositaryOfEntity = Depositary.Application; //实体存放位置
                map.EnDesc = "考勤记录通讯录"; // "用户"; // 实体的描述.
                map.EnType = EnType.App;   //实体类型。
                #endregion
                #region 字段
                /*关于字段属性的增加 */
                map.AddTBIntPKOID();
                map.AddTBDate(DutyRecordAttr.RecordDate, DateTime.Now.Date.ToString("yyyy-MM-dd"), "登记日期", true, false);
                map.AddDDLSysEnum(DutyRecordAttr.LeaveType, 0, "类型", true, true, DutyRecordAttr.LeaveType,
                   "@0=出差@1=请假@2=外出");
                //map.AddRadioBtnSysEnum(DutyRecordAttr.LeaveType, 0, "登记类型", true, true,
                //    DutyRecordAttr.LeaveType);
                map.AddTBDateTime(DutyRecordAttr.StartTime, null, "开始时间", true, false);
                map.AddTBDateTime(DutyRecordAttr.EndTime, null, "结束时间", true, false);
                map.AddTBString(DutyRecordAttr.BeiZhu, null, "备注", true, false, 1, 500, 10);
                map.AddTBString(DutyRecordAttr.DiDian, null, "地点", true, false, 0, 30, 10);
                map.AddTBString(DutyRecordAttr.Recorder, null, "登记人", true, false, 1, 20, 10);
                #endregion 字段
                this._enMap = map;
                return this._enMap;
            }
        }
        /// <summary>
        /// 获取集合
        /// </summary>
        public override Entities GetNewEntities
        {
            get { return new DutyRecords(); }
        }
        #endregion 构造函数
    }
    /// <summary>
    /// 考勤记录s
    // </summary>
    public class DutyRecords : EntitiesOID
    {
        #region 构造方法
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DutyRecord();
            }
        }
        /// <summary>
        /// 考勤记录s
        /// </summary>
        public DutyRecords()
        {
        }
        public override int RetrieveAll()
        {
            return base.RetrieveAll("Name");
        }
        #endregion 构造方法
    }
}