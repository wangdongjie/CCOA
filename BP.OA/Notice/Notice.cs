using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;

namespace BP.OA
{
    /// <summary>
    /// 通知属性
    /// </summary>
    public class NoticeAttr : EntityOIDAttr
    {
        #region 1.基本信息(No,Title,Doc,File,FK_NoticeCategory)
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 内容
        /// </summary>
        public const string Doc = "Doc";
        /// <summary>
        /// 上传文件
        /// </summary>
        public const string AttachFile = "AttachFile";
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_NoticeCategory = "FK_NoticeCategory";
        #endregion

        #region 2.创建信息(FK_UserNo,RDT,FK_Dept)
        /// <summary>
        /// 创建者
        /// </summary>
        public const string FK_UserNo = "FK_UserNo";
        /// <summary>
        /// 发布日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 发布部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 发给部分人
        /// </summary>
        public const string SendToPart = "SendToParts";
        /// <summary>
        /// 发往人员
        /// </summary>
        public const string SendToUsers = "SendToUsers";
        /// <summary>
        /// 发往部门
        /// </summary>
        public const string SendToDepts = "SendToDepts";
        /// <summary>
        /// 发往职位
        /// </summary>
        public const string SendToStations = "SendToStations";
        #endregion

        #region 3.展示设置(SetTop,Importance)
        /// <summary>
        /// 置顶时间
        /// </summary>
        public const string SetTop = "SetTop";
        /// <summary>
        /// 重要级别
        /// </summary>
        public const string Importance = "Importance";
        #endregion

        #region 4.反馈意见(GetAdvices,AdviceDesc,AdviceItems)
        /// <summary>
        /// 是否接受反馈意见
        /// </summary>
        public const string GetAdvices = "GetAdvices";
        /// <summary>
        /// 反馈意见说明
        /// </summary>
        public const string AdviceDesc = "AdviceDesc";
        /// <summary>
        /// 意见项
        /// </summary>
        public const string AdviceItems = "AdviceItems";
        #endregion

        #region 5.其它信息(NoticeSta,Starttime,Stoptime,SetMes)
        /// <summary>
        /// 状态(自动，开启，关闭)
        /// </summary>
        public const string NoticeSta = "NoticeSta";
        /// <summary>
        /// 开始时间
        /// </summary>
        public const string StartTime = "StartTime";
        /// <summary>
        /// 结束时间
        /// </summary>
        public const string StopTime = "StopTime";
        /// <summary>
        /// 是否设置消息推送
        /// </summary>
        public const string SetMes = "SetMes";
        #endregion
    }
    /// <summary>
    /// 通知
    /// </summary>
    public partial class Notice : EntityOID
    {
        #region 1.基本属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return this.GetValStrByKey(NoticeAttr.Title); }
            set { this.SetValByKey(NoticeAttr.Title, value); }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Doc
        {
            get
            {
                try
                {
                    string doc = this.GetValStrByKey(NoticeAttr.Doc);
                    if (doc.Length <= 10)
                        return this.GetBigTextFromDB("DocFile");
                    else
                        return doc;
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(NoticeAttr.Doc, "");
                }
                else
                {
                    this.SetValByKey(NoticeAttr.Doc, value);
                }
            }
        }
        /// <summary>
        /// 文件
        /// </summary>
        public string AttachFile
        {
            get { return this.GetValStringByKey(NoticeAttr.AttachFile); }
            set { this.SetValByKey(NoticeAttr.AttachFile, value); }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public String FK_NoticeCategory
        {
            get { return this.GetValStringByKey(NoticeAttr.FK_NoticeCategory); }
            set { this.SetValByKey(NoticeAttr.FK_NoticeCategory, value); }
        }

        #endregion

        #region 2.创建信息
        /// <summary>
        /// 用户编号
        /// </summary>
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(NoticeAttr.FK_UserNo); }
            set { this.SetValByKey(NoticeAttr.FK_UserNo, value); }
        }
        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime RDT
        {
            get { return this.GetValDateTime(NoticeAttr.RDT); }
            set { this.SetValByKey(NoticeAttr.RDT, value); }
        }
        /// <summary>
        /// 发布部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStrByKey(NoticeAttr.FK_Dept); }
            set { this.SetValByKey(NoticeAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public bool SendToPart
        {
            get { return this.GetValBooleanByKey(NoticeAttr.SendToPart); }
            set { this.SetValByKey(NoticeAttr.SendToPart, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public string SendToUsers
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToUsers); }
            set { this.SetValByKey(NoticeAttr.SendToUsers, value); }
        }
        /// <summary>
        /// 发往部门
        /// </summary>
        public string SendToDepts
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToDepts); }
            set { this.SetValByKey(NoticeAttr.SendToDepts, value); }
        }        /// <summary>
        /// 发往职位
        /// </summary>
        public string SendToStations
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToStations); }
            set { this.SetValByKey(NoticeAttr.SendToStations, value); }
        }
        #endregion

        #region 3.显示设置
        public DateTime SetTop
        {
            get { return this.GetValDateTime(NoticeAttr.SetTop); }
            set { this.SetValByKey(NoticeAttr.SetTop, value); }
        }
        public int Importance
        {
            get { return this.GetValIntByKey(NoticeAttr.Importance); }
            set { this.SetValByKey(NoticeAttr.Importance, value); }
        }
        #endregion

        #region 4.反馈设置
        public bool GetAdvices
        {
            get { return this.GetValBooleanByKey(NoticeAttr.GetAdvices); }
            set { this.SetValByKey(NoticeAttr.GetAdvices, value); }
        }
        public String AdviceDesc
        {
            get { return this.GetValStrByKey(NoticeAttr.AdviceDesc); }
            set { this.SetValByKey(NoticeAttr.AdviceDesc, value); }
        }
        public String AdviceItems
        {
            get { return this.GetValStrByKey(NoticeAttr.AdviceItems); }
            set { this.SetValByKey(NoticeAttr.AdviceItems, value); }
        }
        #endregion

        #region 5.其它信息
        public int NoticeSta
        {
            get { return this.GetValIntByKey(NoticeAttr.NoticeSta); }
            set { this.SetValByKey(NoticeAttr.NoticeSta, value); }
        }
        public DateTime StartTime
        {
            get { return this.GetValDateTime(NoticeAttr.StartTime); }
            set { this.SetValByKey(NoticeAttr.StartTime, value); }
        }
        public DateTime StopTime
        {
            get { return this.GetValDateTime(NoticeAttr.StopTime); }
            set { this.SetValByKey(NoticeAttr.StopTime, value); }
        }
        public int SetMes
        {
            get { return this.GetValIntByKey(NoticeAttr.SetMes); }
            set { this.SetValByKey(NoticeAttr.SetMes, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知
        /// </summary>
        public Notice() { }
        /// <summary>
        /// 通知
        /// </summary>
        /// <param name="no">编号</param>
        public Notice(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Notice";
                map.EnDesc = "通知";   // 实体的描述.

                //基本信息
                map.AddTBIntPKOID();
                map.AddTBString(NoticeAttr.Title, null, "标题", true, false, 0, 300, 30, true);
                map.AddTBStringDoc(NoticeAttr.Doc, null, "内容", true, false, true);
                map.AddTBString(NoticeAttr.AttachFile, null, "上传附件", false, false, 0, 4000, 30, true);
                //map.AddMyFile("资料","rar");
                map.AddDDLEntities(NoticeAttr.FK_NoticeCategory, null, "类别", new NoticeCategorys(), true);

                //创建信息
                map.AddTBDate(NoticeAttr.RDT, null, "发布日期", true, false);
                map.AddDDLEntities(NoticeAttr.FK_Dept, null, "隶属部门", new BP.Port.Depts(), true);

                map.AddDDLEntities(NoticeAttr.FK_UserNo, null, "用户", new BP.Port.Emps(), true);
                map.AddBoolean(NoticeAttr.SendToPart, true, "是否发往部门个人", true, false);

                map.AddTBStringDoc(NoticeAttr.SendToUsers, null, "发往用户", true, false);
                map.AddTBStringDoc(NoticeAttr.SendToDepts, null, "发往部门", true, false);
                map.AddTBStringDoc(NoticeAttr.SendToStations, null, "发往岗位", true, false);

                //展示设置
                map.AddTBDateTime(NoticeAttr.SetTop, "置顶", false, false);
                map.AddDDLSysEnum(NoticeAttr.Importance, 0, "重要程度", true, true, NoticeAttr.Importance,
                    "0=普通@1=比较重要@2=很重要");
                //反馈信息
                map.AddBoolean(NoticeAttr.GetAdvices, false, "是否反馈", true, true);
                map.AddTBStringDoc(NoticeAttr.AdviceDesc, null, "反馈说明", true, false, true);
                map.AddTBStringDoc(NoticeAttr.AdviceItems, null, "反馈项", true, false, true);

                //其它信息
                map.AddDDLSysEnum(NoticeAttr.NoticeSta, 0, "状态", true, true, NoticeAttr.NoticeSta, "@0=自动@1=打开@2=关闭");
                map.AddTBDateTime(NoticeAttr.StartTime, "开始时间", true, false);
                map.AddTBDateTime(NoticeAttr.StopTime, "结束时间", true, false);
                map.AddDDLSysEnum(NoticeAttr.SetMes, 0, "是否发布消息", true, true, NoticeAttr.SetMes, "@0=不推送@1=站内消息");

                //查询条件。
                map.AddSearchAttr(NoticeAttr.FK_Dept);
                map.AddSearchAttr(NoticeAttr.FK_NoticeCategory);
                map.AddSearchAttr(NoticeAttr.NoticeSta);

                RefMethod rm = new RefMethod();
                rm.Title = "打开";
                rm.ClassMethodName = this.ToString() + ".DoOpen";
                map.AddRefMethod(rm);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        /// <summary>
        /// 打开这个通知.
        /// </summary>
        /// <returns></returns>
        public string DoOpen()
        {
            PubClass.WinOpen("http://ccflow.org/sss.aspx?OID=" + this.OID, 100, 200);
            return null;
        }
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class Notices : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Notice();
            }
        }
        /// <summary>
        /// 通知集合
        /// </summary>
        public Notices()
        {
        }
    }
}
