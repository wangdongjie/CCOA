﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.DA;
using System.Data;
namespace BP.OA.BH
{
    class SqWorkFlowAttr : EntityOIDAttr
    {
        /// <summary>
        /// 事件标题
        /// </summary>
        public const string Titel = "Titel";
        /// <summary>
        /// 事件类别
        /// </summary>
        public const string Leibie = "Leibie";
        /// <summary>
        /// 排查人单位
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 排查人姓名
        /// </summary>
        public const string Name = "Name";
        /// <summary>
        /// 排查人电话
        /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 排查时间
        /// </summary>
        public const string PCTime = "PCTime";
        /// <summary>
        /// 问题描述
        /// </summary>
        public const string ProblemSay = "ProblemSay";
        /// <summary>
        /// 问题具体地址
        /// </summary>
        public const string Address = "Address";
        /// <summary>
        /// 办理方式
        /// </summary>
        public const string Mode = "Mode";
    }
    public class SqWorkFlow : EntityOID
    {
        #region 属性
        /// <summary>
        /// 事件标题
        /// </summary>
        public string Titel
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Titel);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Titel, value);
            }
        }
        /// <summary>
        /// 事件类型
        /// </summary>
        public string Leibie
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Leibie);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Leibie, value);
            }
        }
        /// <summary>
        /// 排查人单位
        /// </summary>
        public string FK_Dept
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.FK_Dept, value);
            }
        }
        /// <summary>
        /// 排查人姓名
        /// </summary>
        public string Name
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Name);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Name, value);
            }
        }
        /// <summary>
        /// 问题具体地址
        /// </summary>
        public string Address
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Address);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Address, value);
            }
        }
        /// <summary>
        /// 排查人电话
        /// </summary>
        public string Tel
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Tel);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Tel, value);
            }
        }
        /// <summary>
        /// 排查时间
        /// </summary>
        public string PCTime
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.PCTime);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.PCTime, value);
            }
        }
      
   
        /// <summary>
        /// 问题描述
        /// </summary>
        public string ProblemSay
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.ProblemSay);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.ProblemSay, value);
            }
        }
        
        /// <summary>
        /// 办理方式
        /// </summary>
        public string Mode
        {
            get
            {
                return this.GetValStrByKey(SqWorkFlowAttr.Mode);
            }
            set
            {
                this.SetValByKey(SqWorkFlowAttr.Mode, value);
            }
        }
        
        #endregion
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                //uac.OpenForSysAdmin();
                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;


            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_SqWorkFlow");
                map.EnDesc = "社情业务查询";
                map.CodeStruct = "9";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(SqWorkFlowAttr.Titel, string.Empty, "事件标题", true, false, 0, 10, 30);
                //map.AddTBString(SqWorkFlowAttr.Leibie, string.Empty, "事件类别", true, false, 0, 4000, 500);
                map.AddDDLSysEnum(SqWorkFlowAttr.Leibie, 0, "事件类别", true, true, SqWorkFlowAttr.Leibie, "@0=市场监督@1=经济运行@2=公共服务@3=社会管理@4=民情日志@5=H7N9禽流感防控@6=其他");
                //map.AddDDLEntities(SqWorkFlowAttr.FK_Dept, string.Empty, "排查人员单位", new BP.GPM.Depts() , true);
                map.AddDDLEntities(SqWorkFlowAttr.FK_Dept, string.Empty, "排查人员单位", new BP.Port.Depts(), true);
                map.AddTBString(SqWorkFlowAttr.Name, string.Empty, "排查人员姓名", true, false, 0, 4000, 500);
                map.AddTBString(SqWorkFlowAttr.Tel, string.Empty, "排查人员电话", true, false, 0, 4000, 500);
                map.AddTBDate(SqWorkFlowAttr.PCTime, null, "排查时间", true, false);
                map.AddTBString(SqWorkFlowAttr.Address, string.Empty, "问题具体地址", true, false, 0, 4000, 500);
                map.AddTBStringDoc(SqWorkFlowAttr.ProblemSay, string.Empty, "问题描述", true, false, true);
                //map.AddTBString(SqWorkFlowAttr.Mode, string.Empty, "办理方式", true, false, 0, 200, 200);
                map.AddDDLSysEnum(SqWorkFlowAttr.Mode, 0, "办理方式", true, true, SqWorkFlowAttr.Mode, "@0=提交@1=备案");
                map.AddSearchAttr(SqWorkFlowAttr.Leibie);//根据事件类别查询
                //map.AddSearchAttr(SqWorkFlowAttr.Name);
                map.AddSearchAttr(SqWorkFlowAttr.FK_Dept);
                map.DTSearchKey = SqWorkFlowAttr.PCTime;
                map.DTSearchWay = Sys.DTSearchWay.ByDate;//时间段查询
              
              this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 预定信息
    /// </summary>
    public class SqWorkFlows: EntitiesOID
    {
        /// <summary>
        /// 预定信息s
        /// </summary>
        public SqWorkFlows() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SqWorkFlow();
            }
        }
    }
}

