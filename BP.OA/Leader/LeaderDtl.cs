using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA
{
    /// <summary>
    /// 领导活动属性
    /// </summary>
    public class LeaderDtlAttr : EntityOIDAttr
    {
        /// <summary>
        /// 领导编号
        /// </summary>
        public const string FK_Leader = "FK_Leader";
        /// <summary>
        /// 时间从
        /// </summary>
        public const string DTFrom = "DTFrom";
        /// <summary>
        /// 到
        /// </summary>
        public const string DTTo = "DTTo";
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";

    }
    /// <summary>
    ///  领导活动
    /// </summary>
    public class LeaderDtl : EntityOID
    {
        #region 属性
        /// <summary>
        /// 领导
        /// </summary>
        public string FK_Leader
        {
            get
            {
                return this.GetValStrByKey(LeaderDtlAttr.FK_Leader);
            }
            set
            {
                this.SetValByKey(LeaderDtlAttr.FK_Leader, value);
            }
        }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(LeaderDtlAttr.Title);
            }
            set
            {
                this.SetValByKey(LeaderDtlAttr.Title, value);
            }
        }
        /// <summary>
        /// 从
        /// </summary>
        public string DTFrom
        {
            get
            {
                return this.GetValStrByKey(LeaderDtlAttr.DTFrom);
            }
            set
            {
                this.SetValByKey(LeaderDtlAttr.DTFrom, value);
            }
        }
        /// <summary>
        /// 到
        /// </summary>
        public string DTTo
        {
            get
            {
                return this.GetValStrByKey(LeaderDtlAttr.DTTo);
            }
            set
            {
                this.SetValByKey(LeaderDtlAttr.DTTo, value);
            }
        }
        #endregion 属性

        #region 权限控制属性.
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 领导活动
        /// </summary>
        public LeaderDtl()
        {
        }
        /// <summary>
        /// 领导活动
        /// </summary>
        /// <param name="_No"></param>
        public LeaderDtl(int _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 领导活动Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_LeaderDtl");
                map.EnDesc = "领导活动";
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(LeaderDtlAttr.FK_Leader, null, "领导", false, false, 0, 10, 10);
                map.AddTBString(LeaderDtlAttr.DTFrom, null, "从", true, false, 0, 70, 100);
                map.AddTBString(LeaderDtlAttr.DTTo, null, "到", true, false, 0, 70, 100);
                map.AddTBString(LeaderDtlAttr.Title, null, "活动信息", true, false, 0, 4000, 400);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 领导活动s
    /// </summary>
    public class LeaderDtls : EntitiesOID
    {
        /// <summary>
        /// 领导活动s
        /// </summary>
        public LeaderDtls() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new LeaderDtl();
            }
        }
      
    }
    
}
