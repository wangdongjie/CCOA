﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class MessageAttr : EntityOIDAttr
    {
        public const String AddTime = "AddTime";
        public const String AttachFile = "AttachFile";
        public const String Title = "Title";
        public const String Doc = "Doc";
        public const String FK_UserNo = "FK_UserNo";
        public const String MessageState = "MessageState";
        public const String SendToUsers = "SendToUsers";
        public const String CopyToUsers = "CopyToUsers";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class Message : EntityOID
    {
        #region 属性
      
        public DateTime AddTime
        {
            get { return this.GetValDateTime(MessageAttr.AddTime); }
            set { this.SetValByKey(MessageAttr.AddTime, value); }
        }
        public String AttachFile
        {
            get { return this.GetValStrByKey(MessageAttr.AttachFile); }
            set { this.SetValByKey(MessageAttr.AttachFile, value); }
        }
        public string Title
        {
            get { return this.GetValStrByKey(MessageAttr.Title); }
            set { this.SetValByKey(MessageAttr.Title, value); }
        }
        public string Doc
        {
            get 
            {
                string doc = this.GetValStrByKey(MessageAttr.Doc);
                if (doc.Length <= 10)
                    return this.GetBigTextFromDB("DocFile");
                else
                    return doc;
            }
            set {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(MessageAttr.Doc, "");
                }
                else 
                {
                    this.SetValByKey(MessageAttr.Doc, value);
                }
                 }
        }
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(MessageAttr.FK_UserNo); }
            set { this.SetValByKey(MessageAttr.FK_UserNo, value); }
        }
        /// <summary>
        /// 邮件状态(0-草稿，1-已发送)
        /// </summary>
        public int MessageState
        {
            get { return this.GetValIntByKey(MessageAttr.MessageState); }
            set { this.SetValByKey(MessageAttr.MessageState, value); }
        }
        public string SendToUsers
        {
            get { return this.GetValStrByKey(MessageAttr.SendToUsers); }
            set { this.SetValByKey(MessageAttr.SendToUsers, value); }
        }
        public string CopyToUsers
        {
            get { return this.GetValStrByKey(MessageAttr.CopyToUsers); }
            set { this.SetValByKey(MessageAttr.CopyToUsers, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public Message() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public Message(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Message";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBDateTime(MessageAttr.AddTime, null, "添加时间",true, false);
                map.AddTBString(MessageAttr.Title, null, "附件", true, false, 0, 500, 50);
                map.AddTBString(MessageAttr.AttachFile, null, "附件", true, false, 0, 4000, 50);
                map.AddTBStringDoc(MessageAttr.Doc, null, "消息内容", true, false);
                map.AddTBString(MessageAttr.FK_UserNo, null, "用户名", true, false, 2, 50, 50);
                map.AddTBInt(MessageAttr.MessageState, 0, "邮件状态", true, false);
                map.AddTBStringDoc(MessageAttr.SendToUsers, null, "发送对象",true, false);
                map.AddTBStringDoc(MessageAttr.CopyToUsers, null, "抄送对象", true, false);
                //map.AddTBString(MessageAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(MessageAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        public static void DeleteUseLessData()
        {
            //1.删除文件
            string sSql = "select AttachFile from OA_Message where OID not in (select FK_MsgNo from OA_MessageInBox) "
                            + "                                            and OID not in (select FK_MsgNo from OA_MessageSendBox) "
                            + "                                            and OID not in (select FK_MsgNo from OA_MessageDraftBox) "
                            + "                                            and OID not in (select FK_MsgNo from OA_MessageRecycleBox) ";
            
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string files = String.Format("{0}",dr[0]);
                    if (String.IsNullOrEmpty(files)) continue;
                    string[] arr_files = files.Split(new String[] { ",", ";", "|" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String file in arr_files)
                    {
                        try
                        {
                            System.IO.File.Delete(HttpContext.Current.Server.MapPath(file));
                        }
                        catch
                        {
                            ;
                        }
                    }
                }
            }
            
            //2.删除数据
            sSql = "delete from OA_Message where OID not in (select FK_MsgNo from OA_MessageInBox)  "
                        + " and OID not in (select FK_MsgNo from OA_MessageSendBox) "
                        + " and OID not in (select FK_MsgNo from OA_MessageDraftBox) "
                        + " and OID not in (select FK_MsgNo from OA_MessageRecycleBox) ";
            BP.DA.DBAccess.RunSQL(sSql);
        }
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class Messages : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Message();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public Messages()
        {
        }
    }
}
