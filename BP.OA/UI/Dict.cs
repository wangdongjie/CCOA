﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BP.En;
using BP.DA;

namespace BP.OA.UI
{
    public class Dict
    {
        #region //1.GetTable
        /// <summary>
        /// 从Sql语句获取DataTable,本函数是Dict模块中的对外接口，以保持对外的独立性。
        /// </summary>
        /// <param name="sSql"></param>
        /// <returns></returns>
        private static DataTable GetDataTable(string sSql)
        {
            return GetDataTable("OA", sSql);
        }
        private static DataTable GetDataTable(String cn, string sSql)
        {

            if (String.IsNullOrEmpty(sSql)) 
                return null;

            if (String.IsNullOrEmpty(cn) || cn == "OA")
                return BP.DA.DBAccess.RunSQLReturnTable(sSql) ;
            else
                return CCPortal.DA.DBAccess.RunSQLReturnTable(sSql) ;
        }
        #endregion

        #region //2.GetListItems
        /// <summary>
        /// 从Sql语句获取ListItems集合
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <returns></returns>
        public static ListItem[] GetListItems(string sSql, string textField, string valueField)
        {
            return GetListItems(GetDataTable(sSql), textField, valueField);
        }
        public static ListItem[] GetListItems(string cn, string sSql, string textField, string valueField)
        {
            return GetListItems(GetDataTable(cn, sSql), textField, valueField);
        }
        public static ListItem[] GetListItems(DataTable dt, string textField, string valueField)
        {
            if (dt == null) return null;
            List<ListItem> li = new List<ListItem>();
            foreach (DataRow dr in dt.Rows)
            {
                li.Add(new ListItem(Convert.ToString(dr[textField]), Convert.ToString(dr[valueField])));
            }
            return li.ToArray();
        }
        public static ListItem[] GetListItems(Entities ens, string textField, string valueField)
        {
            if (ens == null) return null;
            List<ListItem> li = new List<ListItem>();
            foreach (Entity en in ens)
            {
                li.Add(new ListItem(Convert.ToString(en.GetValByKey(textField)), Convert.ToString(en.GetValByKey(valueField))));
            }
            return li.ToArray();
        }
        /// <summary>
        /// 从Sql语句获取ListItems集合
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <returns></returns>
        public static ListItem[] GetListItems(string sSql)
        {
            return GetListItems(GetDataTable(sSql));
        }
        public static ListItem[] GetListItems(string cn, string sSql)
        {
            return GetListItems(GetDataTable(cn, sSql));
        }
        public static ListItem[] GetListItems(DataTable dt)
        {
            if (dt == null) return null;
            List<ListItem> li = new List<ListItem>();
            foreach (DataRow dr in dt.Rows)
            {
                li.Add(new ListItem(Convert.ToString(dr[1]), Convert.ToString(dr[0])));
            }
            return li.ToArray();
        }
        #endregion

        #region //3.BindListCtrl
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String sSql, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(sSql), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="Cn">数据连接</param>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String cn, String sSql, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(cn, sSql), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="dt">DataTable数据类型</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(System.Data.DataTable dt, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(dt), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将BP.Entitis（格式：id+name，两个字段）
        /// </summary>
        /// <param name="ens">实体集合数据</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(Entities ens, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(ens, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String sSql, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(sSql, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="Cn">数据连接</param>
        /// <param name="sSql">Sql语句</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(String cn, String sSql, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(cn, sSql, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="dt">DataTable数据类型</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(System.Data.DataTable dt, object listCtrl, string textField, string valueField, string textFormat, string defaultValue, string SelectedValue)
        {
            BindListCtrl(GetListItems(dt, textField, valueField), listCtrl, textFormat, defaultValue, SelectedValue);
        }
        /// <summary>
        /// 直接将Sql语句（格式：id+name，两个字段）
        /// </summary>
        /// <param name="sSql">listItems对象数组</param>
        /// <param name="listCtrl">列表控件</param>
        /// <param name="defaultValue">默认值(格式：值#文本)</param>
        /// <param name="SelectedValue">选择值</param>
        public static void BindListCtrl(ListItem[] listItems, object listCtrl, string textFormat, string defaultValue, string SelectedValue)
        {
            if (String.IsNullOrEmpty(textFormat))
            {
                textFormat = "{0}";
            }
            if (listCtrl is ListControl)
            {
                ListControl lc = (ListControl)listCtrl;
                lc.Items.Clear();
                if (!String.IsNullOrEmpty(defaultValue))
                {
                    string[] arr_d = defaultValue.Split('#');
                    lc.Items.Add(new ListItem(arr_d[1], arr_d[0]));
                }
                foreach (ListItem li in listItems)
                {
                    li.Text = String.Format(textFormat, li.Text);
                }
                lc.Items.AddRange(listItems);
                if (!String.IsNullOrEmpty(SelectedValue))
                {
                    lc.SelectedValue = SelectedValue;
                }
            }
            else if (listCtrl is HtmlSelect)
            {
                HtmlSelect lc = (HtmlSelect)listCtrl;
                lc.Items.Clear();
                if (!String.IsNullOrEmpty(defaultValue))
                {
                    string[] arr_d = defaultValue.Split('#');
                    lc.Items.Add(new ListItem(arr_d[1], arr_d[0]));
                }
                foreach (ListItem li in listItems)
                {
                    li.Text = String.Format(textFormat, li.Text);
                }
                lc.Items.AddRange(listItems);
                if (!String.IsNullOrEmpty(SelectedValue))
                {
                    lc.Value = SelectedValue;
                }
            }
        }
        #endregion
    }
}
