﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA
{
    public class SCGY02NEE : BP.WF.FlowEventBase
    {
        #region 重写属性.
        /// <summary>
        /// 流程标记
        /// </summary>
        public override string FlowMark
        {
            get { return API.SCGY_SCGY_FlowMark; }
        }
        ///// <summary>
        ///// 节点标记
        ///// </summary>
        //public override string NodeMarks
        //{
        //    get { return "01,02,03"; }
        //}
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {
            return null;
        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
           
            return null;
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
        public override string SendSuccess()
        {
            SCGY a = new SCGY();
            a.OID = Int32.Parse(this.WorkID + "");
            a.Retrieve();
            a.BT = this.GetValStr("BT");
            a.QQR = this.GetValStr("QQR");
            a.FK_FGS = this.GetValStr("OP_Dept");
            a.LCZT = 1;
            a.Update();
            return null;
        }
    }
}
