﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;

namespace BP.OA.Desktop
{
    /// <summary>
    /// 桌面配置属性
    /// </summary>
    public class DesktopConfigAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 所属人员
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 每行列数
        /// </summary>
        public const string CellCount = "CellCount";
        /// <summary>
        /// 信息块宽度
        /// </summary>
        public const string ColWidth = "ColWidth";
        /// <summary>
        /// 信息块高度
        /// </summary>
        public const string ColHeight = "ColHeight";
    }
    /// <summary>
    ///  桌面配置
    /// </summary>
    public class DesktopConfig : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 添加人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(DesktopConfigAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(DesktopConfigAttr.FK_Emp, value);
            }
        }
        
        #endregion 属性

        #region 构造方法
        /// <summary>
        /// 桌面配置
        /// </summary>
        public DesktopConfig()
        {
        }
        /// <summary>
        /// 桌面配置
        /// </summary>
        /// <param name="_No"></param>
        public DesktopConfig(string _No) : base(_No) { }

        #endregion

        /// <summary>
        /// 桌面配置Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_DesktopConfig"; //要物理表。
                map.EnDesc = "桌面配置";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";
                map.AddTBStringPK(DesktopConfigAttr.No, null, "编号", true, true, 10, 10, 10);
                map.AddTBString(DesktopConfigAttr.Name, null, "名称", true, false, 0, 200, 300, false);
                map.AddDDLEntities(DesktopConfigAttr.FK_Emp, null, "添加人", new BP.Port.Emps(), true);
                
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 桌面配置
    /// </summary>
    public class DesktopConfigs : EntitiesNoName
    {
        /// <summary>
        /// 桌面配置s
        /// </summary>
        public DesktopConfigs() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DesktopConfig();
            }
        }
    }
}
