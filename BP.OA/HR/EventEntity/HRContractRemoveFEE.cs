﻿using BP.Port;
using BP.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace BP.OA.HR.EventEntity
{ /// <summary>
    /// 固定资产合同解除 流程事件实体
    /// </summary>
    public class HRContractRemoveeFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产合同解除 流程事件实体
        /// </summary>
        public HRContractRemoveeFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "HRContractRemove"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            ////把合同解除信息写入api.

            //BP.OA.HR.HRContracts hrcs = new BP.OA.HR.HRContracts();
            //hrcs.RetrieveAll();
            //BP.OA.HR.HRRecords emps = new BP.OA.HR.HRRecords();
            //emps.RetrieveAll();
            //BP.OA.HR.HRRecord emp = (BP.OA.HR.HRRecord)emps.Filter("Name", GetValStr("TXR"));
            //HRContract hrc = (HRContract)hrcs.Filter("FK_HrRecord", emp.No);
            //if (hrc==null)
            //{
            //    return "此员工没有签订合同";
            //}
            ////把合同解除信息写入api.     
            //BP.OA.HR.API.HR_ContractRemove(hrc.No, hrc.FK_HRRecord, hrc.FK_HRRecord,
            //   this.GetValStr("TXSJ"), hrc.GuDingQiXian, hrc.StartDT, hrc.EndDT, GetValStr("LZSJ"), GetValStr("LZYY")
            //   );
            //return "写入成功....";
            //Web.WebUser.No
            int zt = GetValInt("zongjingliZT");
            BP.OA.HR.HRRecords rs = new BP.OA.HR.HRRecords();
            rs.RetrieveAll();
            HRRecord r = (HRRecord)rs.Filter("Name", GetValStr("TXR"));
            if (zt == 0)
            {
                r.ZhuangTai = "1";
                r.Update();
            }
            HRContracts cs = new HRContracts();
            cs.RetrieveAll();
            HRContract c = (HRContract)cs.Filter("FK_HRRecord", r.No);
            c.Delete();
            return null;
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
