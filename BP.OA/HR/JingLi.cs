﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
namespace BP.OA.HR
{
    /// <summary>
    /// 个人经历属性
    /// </summary>
    public class JingLiAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        /// FK_Name
        /// </summary>
        public const string FK_Name = "FK_Name";
        /// <summary>
        /// 工作年限
        /// </summary>
        public const string WorkYearCount = "WorkYearCount";
        /// <summary>
        /// 时间从
        /// </summary>
        public const string TimeStart = "TimeStart";
        /// 时间到
        /// </summary>
        public const string TimeTo = "TimeTo";
        /// <summary>
        /// 工作单位
        /// </summary>
        public const string WorkUnit = "WorkUnit";
        /// <summary>
        /// 职务
        /// </summary>
        public const string Duty = "Duty";
        /// <summary>
        /// 主要业绩
        /// </summary>
        public const string MainResults = "MainResults";
        /// <summary>
        /// 离职原因
        /// </summary>
        public const string LeavingReason = "LeavingReason";
        #endregion
    }
    /// <summary>
    /// 个人经历
    /// </summary>
    public class JingLi : EntityOID
    {
        #region 基本属性
        /// <summary>
        /// 姓名
        /// </summary>
        public string FK_Name
        {
            get { return this.GetValStringByKey(JingLiAttr.FK_Name); }
            set { this.SetValByKey(JingLiAttr.FK_Name, value); }
        }
        /// <summary>
        /// 工作年限
        /// </summary>
        public string WorkYearCount
        {
            get { return this.GetValStringByKey(JingLiAttr.WorkYearCount); }
            set { this.SetValByKey(JingLiAttr.WorkYearCount, value); }
        }
        /// <summary>
        /// 时间从
        /// </summary>
        public string TimeStart
        {
            get { return this.GetValStringByKey(JingLiAttr.TimeStart); }
            set { this.SetValByKey(JingLiAttr.TimeStart, value); }
        }
        /// <summary>
        /// 时间到
        /// </summary>
        public string TimeTo
        {
            get { return this.GetValStringByKey(JingLiAttr.TimeTo); }
            set { this.SetValByKey(JingLiAttr.TimeTo, value); }
        }
        /// <summary>
        /// 工作单位
        /// </summary>
        public string WorkUnit
        {
            get { return this.GetValStringByKey(JingLiAttr.WorkUnit); }
            set { this.SetValByKey(JingLiAttr.WorkUnit, value); }
        }
        /// <summary>
        /// 职务
        /// </summary>
        public string Duty
        {
            get { return this.GetValStringByKey(JingLiAttr.Duty); }
            set { this.SetValByKey(JingLiAttr.Duty, value); }
        }
        /// <summary>
        /// 主要业绩
        /// </summary>
        public string MainResults
        {
            get { return this.GetValStringByKey(JingLiAttr.MainResults); }
            set { this.SetValByKey(JingLiAttr.MainResults, value); }
        }
        /// <summary>
        ///离职原因
        /// </summary>
        public string LeavingReason
        {
            get { return this.GetValStringByKey(JingLiAttr.LeavingReason); }
            set { this.SetValByKey(JingLiAttr.LeavingReason, value); }
        }
        #endregion

        #region 构造方法

        public JingLi()
        {

        }

        public JingLi(int oid)
            : base(oid)
        {

        }
        #endregion
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                HRRecord r = new HRRecord(this.FK_Name);
                if (Web.WebUser.No == "admin" || Web.WebUser.Name == r.Name)
                { uac.OpenAll(); }
                else
                {
                    uac.Readonly();
                }
                return uac;
            }
        }
        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_JingLi");
                map.EnDesc = "工作经历";
                //编号
                map.AddTBIntPKOID();
                //字段
                map.AddDDLEntities(JingLiAttr.FK_Name, null, "姓名", new BP.OA.HR.HRRecords(), true);
                map.AddDDLSysEnum(JingLiAttr.WorkYearCount, 0, "工作年限", true, true, JingLiAttr.WorkYearCount, "@0=应届毕业生@1=实习生@2=1年以下@3=1年@4=1.5年@5=2年@6=3-5年@7=5年以上", true);
                map.AddTBDate(JingLiAttr.TimeStart, null, "时间从", true, false);
                map.AddTBDate(JingLiAttr.TimeTo, null, "时间到", true, false);
                map.AddTBString(JingLiAttr.WorkUnit, null, "工作单位", true, false, 1, 100, 100);
                map.AddTBString(JingLiAttr.Duty, null, "担任职务", true, false, 1, 50, 100);
                map.AddTBStringDoc(JingLiAttr.MainResults, null, "主要业绩", true, false,true);
                map.AddTBStringDoc(JingLiAttr.LeavingReason, null, "离职原因", true, false,true);
                //查询条件
                map.AddSearchAttr(JingLiAttr.FK_Name);
                //map.AddSearchAttr(JingLiAttr.FK_Name);
                //map.DTSearchKey = JingLiAttr.TimeStart;
                //map.DTSearchKey = JingLiAttr.TimeTo;
                //map.DTSearchWay = Sys.DTSearchWay.ByDate;
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    /// 劳动合同变更与续签s
    /// </summary>
    public class JingLis : EntitiesOID
    {
        #region 构造方法

        public JingLis()
        {

        }
        #endregion

        #region 重写父类的方法
        public override Entity GetNewEntity
        {
            get { return new JingLi(); }
        }
        #endregion
    }
}
