﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA
{
    public class Soft
    {
        public static string SysName
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SysName"]);
            }
        }
        public static string SysNo
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SysNo"]);
            }
        }
        public static string SysLanguage
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SysLanguage"]);
            }
        }
        public static string Integrate 
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Integrate"]);
            }
        }
    }
}
