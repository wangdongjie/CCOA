﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{
    public class ZiJingJiHuaAttr : EntityOIDAttr
    {
        /// <summary>
        /// 计划人
        /// </summary>
        public const string JiHuaRen = "JiHuaRen";
        /// <summary>
        /// 编制时间
        /// </summary>
        public const string BianZhiDate = "BianZhiDate";
        /// <summary>
        /// 计划年月
        /// </summary>
        public const string JuHuaNianYue = "JuHuaNianYue";

        /// <summary>
        /// 提交日期
        /// </summary>
        public const string TiJiaoDate = "TiJiaoDate";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
    }



    /// <summary>
    /// 市场收款计划实体类
    /// </summary>
    public class ZiJingJiHua : EntityOID
    {
        #region 属性
        
        /// <summary>
        /// 计划人
        /// </summary>
        public string JiHuaRen
        {
            get
            {
                return this.GetValStrByKey(ZiJingJiHuaAttr.JiHuaRen);
            }
            set
            {
                this.SetValByKey(ZiJingJiHuaAttr.JiHuaRen, value);
            }
        }

        /// <summary>
        ///编制时间
        /// </summary>
        public DateTime BianZhiDate
        {
            get
            {
                return this.GetValDateTime(ZiJingJiHuaAttr.BianZhiDate);
            }
            set
            {
                this.SetValByKey(ZiJingJiHuaAttr.BianZhiDate, value);
            }
        }
        /// <summary>
        ///计划年月
        /// </summary>
        public DateTime JuHuaNianYue
        {
            get
            {
                return this.GetValDateTime(ZiJingJiHuaAttr.JuHuaNianYue);
            }
            set
            {
                this.SetValByKey(ZiJingJiHuaAttr.JuHuaNianYue, value);
            }
        }
        /// <summary>
        ///提交日期
        /// </summary>
        public DateTime TiJiaoDate
        {
            get
            {
                return this.GetValDateTime(ZiJingJiHuaAttr.TiJiaoDate);
            }
            set
            {
                this.SetValByKey(ZiJingJiHuaAttr.TiJiaoDate, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(ZiJingJiHuaAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(ZiJingJiHuaAttr.BeiZhu, value);
            }
        }
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public ZiJingJiHua()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public ZiJingJiHua(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_ShouKuanJiHua");
                map.EnDesc = "市场收款计划";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                //增加OID主键字段。
                map.AddTBIntPKOID();

                map.AddTBString(ZiJingJiHuaAttr.JiHuaRen, null, "计划人", true, false, 0, 100, 40);

       

                map.AddTBDate(ZiJingJiHuaAttr.BianZhiDate, "编制时间", true, false);
                map.AddTBDate(ZiJingJiHuaAttr.JuHuaNianYue, "计划年月", true, false);
                map.AddTBDate(ZiJingJiHuaAttr.TiJiaoDate, "提交日期", true, false);
                
                map.AddTBStringDoc(ZiJingJiHuaAttr.BeiZhu, null, "备注", true, false, true);
                this._enMap = map;
                return this._enMap;
            }
        }

    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class ZiJingJiHuas : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public ZiJingJiHuas() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ZiJingJiHua();
            }
        }
    }
}
