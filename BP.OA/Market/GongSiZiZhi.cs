﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{

    /// <summary>
    /// 客户档案
    /// </summary>
    public class GongSiZiZhiAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 资质名称
        /// </summary>
        public const string ZiZhiMingCheng = "ZiZhiMingCheng";
        /// <summary>
        /// 发证机关
        /// </summary>
        public const string FaZhengJiGuan = "FaZhengJiGuan";
        /// <summary>
        /// 资质等级
        /// </summary>
        public const string ZiZhiDengJi = "ZiZhiDengJi";
        /// <summary>
        ///颁布日期
        /// </summary>
        public const string BanBuDate = "BanBuDate";
        /// <summary>
        ///有效日期
        /// </summary>
        public const string YouXiaoDate = "YouXiaoDate";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
        
    }



    /// <summary>
    /// 公司资质实体类
    /// </summary>
    public class GongSiZiZhi: EntityNoName
    {
        #region 属性
        /// <summary>
        /// 资质名称
        /// </summary>
        public string ZiZhiMingCheng
        {
            get
            {
                return this.GetValStrByKey(GongSiZiZhiAttr.ZiZhiMingCheng);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.ZiZhiMingCheng, value);
            }
        }
        /// <summary>
        /// 发证机关
        /// </summary>
        public string FaZhengJiGuan
        {
            get
            {
                return this.GetValStrByKey(GongSiZiZhiAttr.FaZhengJiGuan);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.FaZhengJiGuan, value);
            }
        }
        /// <summary>
        /// 资质等级
        /// </summary>
        public string LianXiDianHua
        {
            get
            {
                return this.GetValStrByKey(GongSiZiZhiAttr.ZiZhiDengJi);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.ZiZhiDengJi, value);
            }
        }
        /// <summary>
        /// 有效日期
        /// </summary>
        public DateTime YouXiaoDate
        {
            get
            {
                return this.GetValDateTime(GongSiZiZhiAttr.YouXiaoDate);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.YouXiaoDate, value);
            }
        }
        /// <summary>
        /// 颁布日期
        /// </summary>
        public DateTime BanBuDate
        {
            get
            {
                return this.GetValDateTime(GongSiZiZhiAttr.BanBuDate);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.BanBuDate, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(GongSiZiZhiAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(GongSiZiZhiAttr.BeiZhu, value);
            }
        }
       
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public GongSiZiZhi()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public GongSiZiZhi(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_GongSiZiZhi");
                map.EnDesc = "公司资质信息查询";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                map.AddTBStringPK(GongSiZiZhiAttr.No, null, "资质编号", true, true, 0, 100, 40);
                map.AddTBString(GongSiZiZhiAttr.ZiZhiMingCheng, null, "资质名称", true, false, 0, 100, 40);
                map.AddTBString(GongSiZiZhiAttr.FaZhengJiGuan, null, "发证机关", true, false, 0, 100, 40);
                map.AddTBString(GongSiZiZhiAttr.ZiZhiDengJi, null, "资质等级", true, false, 0, 100, 40);
                map.AddTBDate(GongSiZiZhiAttr.BanBuDate, "颁布日期", true, false);
                map.AddTBDate(GongSiZiZhiAttr.YouXiaoDate, "有效日期", true, false);
                map.AddTBStringDoc(GongSiZiZhiAttr.BeiZhu, null, "备注", true, false,true);
                this._enMap = map;
                return this._enMap;
            }
        }

    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class GongSiZiZhis : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public GongSiZiZhis() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new GongSiZiZhi();
            }
        }
    }
}





