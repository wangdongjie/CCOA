using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Market
{

    /// <summary>
    /// 投标
    /// </summary>
    public class UnitAttr : EntityNoNameAttr
    {
      /// <summary>
      /// 单位名称
      /// </summary>
        public const string Dwmc = "Dwmc";


    }
    /// <summary>
    ///  投标实体类
    /// </summary>
    public class Unit : EntityNoName
    {
        #region 属性
      
     
        #endregion 属性

        #region 权限控制属性.
        
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public Unit()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public Unit(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Unit");
                map.EnDesc = "投标登记与查询";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //四位编号0001开始
                map.AddTBStringPK(UnitAttr.No, null, "编号", false, true, 3, 3, 3);
                map.AddTBString(UnitAttr.Name, null, "单位名称", true, false, 0, 10, 3);

                this._enMap = map;
                return this._enMap;
            }
        }
       
    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class Units : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public Units() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Unit();
            }
        }
    }
}
