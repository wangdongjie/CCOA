﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    public class FixAssBigCatSonAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 父类名
        /// </summary>
        public const string FK_FixAssBigCatagory = "FK_FixAssBigCatagory";
    }
    public class FixAssBigCatSon : EntityNoName
    {
        #region 属性
     /// <summary>
     /// 父类名
     /// </summary>
        public string FK_FixAssBigCatagory
        {
            get
            {
                return this.GetValStringByKey(FixAssBigCatSonAttr.FK_FixAssBigCatagory);
            }
            set
            {
                this.SetValByKey(FixAssBigCatSonAttr.FK_FixAssBigCatagory, value);
            }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 固定资产台帐
        /// </summary>
        public FixAssBigCatSon() { }
        /// <summary>
        /// 固定资产台帐
        /// </summary>
        /// <param name="no">编号</param>
        public FixAssBigCatSon(string no) : base(no) { }
        #endregion

        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_FixAssBigCatSon");
                map.EnDesc = "固定资产小类";

                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;

                map.AddTBStringPK("No", null, "编号", true, true, 4, 4, 4);
                map.AddDDLEntities(FixAssBigCatSonAttr.FK_FixAssBigCatagory, null, "所属大类", new FixAssBigCatagorys(), true);
                map.AddTBString("Name", null, "小类名称", true, false, 1, 100, 50);

                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class FixAssBigCatSons : EntitiesNoName
    {
        /// <summary>
        /// 类别s
        /// </summary>
        public FixAssBigCatSons() { }
        /// <summary>
        /// 构造函数
        /// </summary>


        //public FixAssBigCatSons(string no) 
        //{

        //}

        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get { return new FixAssBigCatSon(); }
        }
    }
}
