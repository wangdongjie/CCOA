using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 报废流程 - 开始节点.
    /// </summary>
    public class FixManGetBadND1601NEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 报废流程事件
        /// </summary>
        public FixManGetBadND1601NEE()
        {
        }
        #endregion 属性.

        #region 重写属性.
        /// <summary>
        /// 流程标记
        /// </summary>
        public override string FlowMark
        {
            get { return BP.FixAss.API.FixMan_GetBad_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {
            return null;
        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
            if (this.HisNode.NodeID == 1601)
            {
                //限制多次申请同一物品
                int GetNameInd = 0;
                string[] GetName = new string[10];

                string sql = "select * from ND1601Dtl1 where  RefPK=" + this.WorkID;

                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //数组赋值
                    GetName[GetNameInd] = dr["OA_FixMan"].ToString();
                    GetNameInd += 1;
                }
                //判断
                for (int i = 0; i < GetNameInd; i++)
                {
                    for (int j = i + 1; j < GetName.Length; j++)
                    {
                        if (GetName[i] == GetName[j])
                        {
                            throw new Exception("@不允许填写重复项!");
                        }
                    }
                }
                return "合计已经在发送前事件完成.";
            }

            return null;
        }
        /// <summary>
        /// 发送成功后
        /// </summary>
        public override string SendSuccess()
        {
            if (this.HisNode.NodeID == 1601)
            {
                string sql = "select * from ND1601Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);

                foreach (DataRow dr in dt.Rows)
                {
                    BP.FixAss.API.FixManGetBad_OneInsert(this.WorkID,
                        dr["OA_FixMan"].ToString(),
                        this.GetValStr("TianXieRen"),
                        this.GetValStr("LiShuBuMen"),
                        this.GetValStr("ShiJian"),
                        dr["BeiZhu"].ToString(),
                        "0");
                }
                return "SendSuccess调用成功....";
            }

            return null;
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
