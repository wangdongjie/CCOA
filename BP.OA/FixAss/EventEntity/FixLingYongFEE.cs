﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.FixAss.EventEntity
{
    public class FixLingYongFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return "FixLingYong"; }
        }

        public override string FlowOverAfter()
        {
            BP.OA.FixAss.API.FixLingYong(this.GetValStr("WPMC"), this.GetValStr("ZCBH"), this.GetValStr("GGXH"), WebUser.Name, WebUser.FK_Dept, this.GetValStr("LYSJ"), this.GetValStr("BZ"));
            return "物品领用成功...";
        }
        public override string AfterFlowDel()
        {
            return null;
        }
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string FlowOverBefore()
        {
            return null;
        }
    }
}
