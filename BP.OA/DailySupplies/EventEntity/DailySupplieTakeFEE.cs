using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 固定资产领用 流程事件实体
    /// </summary>
    public class DailySupplieTakeFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产领用 流程事件实体
        /// </summary>
        public DailySupplieTakeFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "DailySupplieTake"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            string sql = "select * from ND601Dtl1 where  RefPK=" + this.WorkID;
            DataTable dt = DBAccess.RunSQLReturnTable(sql);
            foreach (DataRow dr in dt.Rows)
            {
                string fk_DsMain = dr["OA_DSMain"].ToString();

                string GetOidsSql = "select OID FROM  OA_DSTake WHERE WorkID='" + this.WorkID + "' AND  FK_DSMain=" + fk_DsMain;
                //string GetOidsSql = "select OID FROM  OA_DSTake WHERE FK_DSMain=" + fk_DsMain;
                DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                //BP.DS.API.DailySupplieTake_FinallyUpdate(
                //    oid,
                //  int.Parse(dr["ChuKuShuLiang"].ToString()),
                //  this.GetValStr("ChuKuRen"));
            }
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
