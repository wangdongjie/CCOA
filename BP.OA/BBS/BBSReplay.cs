﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.BBS
{
    //回复主题
    public class BBSReplayAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 主题编号
        /// </summary>
        public const String FK_Motif = "FK_Motif";
        /// <summary>
        /// 内容
        /// </summary>
        public const String Contents = "Contents";
        /// <summary>
        /// 回复人员
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 次序
        /// </summary>
        public const String Ranking = "Ranking";
        /// <summary>
        /// 新建时间
        /// </summary>
        public const String AddTime = "AddTime";
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public const String LastEditTime = "LastEditTime";
    }
    /// <summary>
    /// 回复主题
    /// </summary>
    public partial class BBSReplay : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 主题编号
        /// </summary>
        public string FK_Motif
        {
            get
            {
                return this.GetValStrByKey(BBSReplayAttr.FK_Motif);
            }
            set
            {
                this.SetValByKey(BBSReplayAttr.FK_Motif, value);
            }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Contents
        {
            get 
            {
                string contents = this.GetValStrByKey(BBSReplayAttr.Contents);
                if (contents.Length <= 10)
                    return this.GetBigTextFromDB("DocFile");
                else
                    return contents;
            }
            set {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(BBSReplayAttr.Contents, "");
                }
                else 
                {
                    this.SetValByKey(BBSReplayAttr.Contents, value);
                }
                 }
        }
        /// <summary>
        /// 回复人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(BBSReplayAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(BBSReplayAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 回复人员性名
        /// </summary>
        public string FK_EmpText
        {
            get
            {
                return this.GetValRefTextByKey(BBSReplayAttr.FK_Emp);
            }
        }
        /// <summary>
        /// 次序
        /// </summary>
        public int Ranking
        {
            get
            {
                return this.GetValIntByKey(BBSReplayAttr.Ranking);
            }
            set
            {
                this.SetValByKey(BBSReplayAttr.Ranking, value);
            }
        }
        /// <summary>
        /// 新建时间
        /// </summary>
        public DateTime AddTime
        {
            get
            {
                return this.GetValDateTime(BBSReplayAttr.AddTime);
            }
            set
            {
                this.SetValByKey(BBSReplayAttr.AddTime, value);
            }
        }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime LastEditTime
        {
            get
            {
                return this.GetValDateTime(BBSReplayAttr.LastEditTime);
            }
            set
            {
                this.SetValByKey(BBSReplayAttr.LastEditTime, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 回复主题
        /// </summary>
        public BBSReplay() { }
        public BBSReplay(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_BBSReplay";
                map.EnDesc = "回复主题";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";
                map.AddTBStringPK(BBSReplayAttr.No, null, "编号", true, true, 2, 2, 2);
                map.AddTBString(BBSReplayAttr.Name, null, "名称", true, false, 0, 200, 30);
                map.AddTBInt(BBSReplayAttr.FK_Motif, 0, "主题编号", true, false);
                map.AddTBString(BBSReplayAttr.Contents, null, "内容", true, false, 0, 2000, 30);
                map.AddDDLEntities(BBSReplayAttr.FK_Emp, null, "回复人员", new Emps(), true);
                map.AddTBInt(BBSReplayAttr.Ranking, 0, "次序", true, false);
                map.AddTBDateTime(BBSReplayAttr.AddTime, null, "新建时间", true, false);
                map.AddTBDateTime(BBSReplayAttr.LastEditTime, null, "最后更新时间", true, false);

                this._enMap = map;
                return this._enMap;
            }
        }

        protected override bool beforeInsert()
        {
            //修改几楼
            BBSReplays replays = new BBSReplays();
            replays.RetrieveByAttr(BBSReplayAttr.FK_Motif, this.FK_Motif);
            this.Ranking = replays.Count + 1;

            return base.beforeInsert();
        }

        protected override void afterInsert()
        {
            //编辑主题被回复数量
            BBSMotif motif = new BBSMotif();
            motif.RetrieveByAttr(BBSMotifAttr.No, this.FK_Motif);
            motif.Replays = motif.Replays + 1;
            motif.LastReEmp = this.FK_Emp;
            motif.LastReTime = DateTime.Now;
            motif.Update();

            //版块的回复次数
            BBSClass bbsClass = new BBSClass(motif.FK_Class);
            bbsClass.ReplyCount = bbsClass.ReplyCount + 1;
            bbsClass.Update();

            //获取配置信息
            BBSConfig config = new BBSConfig(0);

            //用户经验值
            BBSUserInfo bbsUser = new BBSUserInfo(this.FK_Emp);
            bbsUser.Exp = Convert.ToInt32(config.R_exp) + bbsUser.Exp;
            bbsUser.Integral = Convert.ToInt32(config.R_integral) + bbsUser.Integral;
            bbsUser.Gold = Convert.ToInt32(config.R_gold) + bbsUser.Gold;
            bbsUser.ReplayCount = bbsUser.ReplayCount + 1;
            bbsUser.Update();

            base.afterInsert();
        }
        #endregion
    }
    /// <summary>
    ///回复主题集合
    /// </summary>
    public class BBSReplays : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BBSReplay();
            }
        }
        /// <summary>
        /// 回复主题集合
        /// </summary>
        public BBSReplays()
        {
        }
    }
}
