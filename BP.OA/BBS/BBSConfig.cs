﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.BBS
{
    public enum WaterSet
    {
        none,//不添加水印
        text,//文字水印
        img//图片水印
    }

    //论坛配置信息
    public class BBSConfigAttr : EntityOIDAttr
    {
        /// <summary>
        /// 论坛名称
        /// </summary>
        public const String WebName = "WebName";
        /// <summary>
        /// 站点
        /// </summary>
        public const String WebSite = "WebSite";
        /// <summary>
        /// 论坛关键字
        /// </summary>
        public const String KeyWords = "KeyWords";
        /// <summary>
        /// 描述
        /// </summary>
        public const String Description = "Description";
        /// <summary>
        /// 过滤词汇
        /// </summary>
        public const String Filter = "Filter";
        /// <summary>
        /// 发帖加经验
        /// </summary>
        public const String F_exp = "F_exp";
        /// <summary>
        /// 发帖加积分
        /// </summary>
        public const String F_integral = "F_integral";
        /// <summary>
        /// 发帖加金币
        /// </summary>
        public const String F_gold = "F_gold";
        /// <summary>
        /// 回帖加经验
        /// </summary>
        public const String R_exp = "R_exp";
        /// <summary>
        /// 回帖加积分
        /// </summary>
        public const String R_integral = "R_integral";
        /// <summary>
        /// 回帖加金币
        /// </summary>
        public const String R_gold = "R_gold";
        /// <summary>
        /// 上传图片的水印
        /// </summary>
        public const String WaterSet = "WaterSet";
        /// <summary>
        /// 水印文字（或图片地址）
        /// </summary>
        public const String WaterTxt = "WaterTxt";
    }
    /// <summary>
    /// 论坛配置信息
    /// </summary>
    public partial class BBSConfig : EntityOID
    {
        #region 属性
        /// <summary>
        /// 论坛名称
        /// </summary>
        public string WebName
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.WebName);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.WebName, value);
            }
        }
        /// <summary>
        /// 站点
        /// </summary>
        public string WebSite
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.WebSite);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.WebSite, value);
            }
        }
        /// <summary>
        /// 论坛关键字
        /// </summary>
        public string KeyWords
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.KeyWords);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.KeyWords, value);
            }
        }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.Description);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.Description, value);
            }
        }
        /// <summary>
        /// 过滤词汇
        /// </summary>
        public string Filter
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.Filter);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.Filter, value);
            }
        }
        /// <summary>
        /// 发帖加经验
        /// </summary>
        public string F_exp
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.F_exp);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.F_exp, value);
            }
        }
        /// <summary>
        /// 发帖加积分
        /// </summary>
        public string F_integral
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.F_integral);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.F_integral, value);
            }
        }
        /// <summary>
        /// 发帖加金币
        /// </summary>
        public string F_gold
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.F_gold);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.F_gold, value);
            }
        }
        /// <summary>
        /// 回帖加经验
        /// </summary>
        public string R_exp
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.R_exp);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.R_exp, value);
            }
        }
        /// <summary>
        /// 回帖加积分
        /// </summary>
        public string R_integral
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.R_integral);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.R_integral, value);
            }
        }
        /// <summary>
        /// 回帖加金币
        /// </summary>
        public string R_gold
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.R_gold);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.R_gold, value);
            }
        }
        /// <summary>
        /// 上传图片的水印
        /// </summary>
        public WaterSet WaterSet
        {
            get
            {
                return (WaterSet)this.GetValIntByKey(BBSConfigAttr.WaterSet);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.WaterSet, (int)value);
            }
        }
        /// <summary>
        /// 水印文字（或图片地址）
        /// </summary>
        public string WaterTxt
        {
            get
            {
                return this.GetValStrByKey(BBSConfigAttr.WaterTxt);
            }
            set
            {
                this.SetValByKey(BBSConfigAttr.WaterTxt, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 论坛配置信息
        /// </summary>
        public BBSConfig() { }
        /// <summary>
        /// 论坛配置信息
        /// </summary>
        /// <param name="oid"></param>
        public BBSConfig(int oid) 
        {
            this.OID = oid;
            this.RetrieveFromDBSources();
        }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_BBSConfig";
                map.EnDesc = "论坛配置信息";

                map.AddTBIntPKOID();
                map.AddTBString(BBSConfigAttr.WebName, null, "论坛名称", true, false, 0, 200, 300, true);
                map.AddTBString(BBSConfigAttr.WebSite, null, "站点", true, false, 0, 200, 300, true);
                map.AddTBString(BBSConfigAttr.KeyWords, null, "论坛关键字", true, false, 0, 200, 300, true);
                map.AddTBString(BBSConfigAttr.Filter, null, "过滤词汇", true, false, 0, 200, 200);
                map.AddTBInt(BBSConfigAttr.F_exp, 4, "发帖加经验", true, false);
                map.AddTBInt(BBSConfigAttr.F_integral, 5, "发帖加积分", true, false);
                map.AddTBInt(BBSConfigAttr.F_gold, 3, "发帖加金币", true, false);
                map.AddTBInt(BBSConfigAttr.R_exp, 2, "回帖加经验", true, false);
                map.AddTBInt(BBSConfigAttr.R_integral, 2, "回帖加积分", true, false);
                map.AddTBInt(BBSConfigAttr.R_gold, 1, "回帖加金币", true, false);
                map.AddDDLSysEnum(BBSConfigAttr.WaterSet, 0, "上传图片的水印", true, true, BBSConfigAttr.WaterSet, "@0=不添加水印@1=文字水印@2=图片水印");
                map.AddTBString(BBSConfigAttr.WaterTxt, null, "水印文字（或图片地址）", true, false, 0, 200, 200, true);
                map.AddTBString(BBSConfigAttr.Description, null, "描述", true, false, 0, 2000, 300, true);

                this._enMap = map;
                return this._enMap;
            }
        }

        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        #endregion
    }
    /// <summary>
    ///论坛配置信息集合
    /// </summary>
    public class BBSConfigs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BBSConfig();
            }
        }
        /// <summary>
        /// 论坛配置信息集合
        /// </summary>
        public BBSConfigs()
        {
        }
    }
}
