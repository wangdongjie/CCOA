﻿using BP.DA;
using BP.En;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.WebLog
{/// <summary>
    /// 类别 属性
    /// </summary>
    public class LogTypeAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 是否可以下载
        /// </summary>
        public const string Type = "IsDownload";
    }
    /// <summary>
    ///  类别
    /// </summary>
    public class LogType : EntityNoName
    {
        #region 属性
        #endregion 属性

        #region 权限控制属性.
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 类别
        /// </summary>
        public LogType()
        {
        }
        /// <summary>
        /// 类别
        /// </summary>
        /// <param name="_No"></param>
        public LogType(string _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 类别Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_LogType");
                map.EnDesc = "类别";


                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.CodeStruct = "3";
                map.IsAutoGenerNo = true;
                map.AddTBStringPK(LogTypeAttr.No, null, "编号", true, true, 3, 3, 3);

                map.AddTBString(LogTypeAttr.Name, null, "类别", true, false, 0, 100, 30, false);

                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 类别
    /// </summary>
    public class LogTypes : SimpleNoNames
    {
        /// <summary>
        /// 类别s
        /// </summary>
        public LogTypes() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new LogType();
            }
        }
    }
}
