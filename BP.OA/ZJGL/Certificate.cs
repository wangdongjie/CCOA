﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
using System.Data;

namespace BP.OA.ZJGL
{
    /// <summary>
    /// 证件属性
    /// </summary>
    public class CertificateAttr : EntityNoNameAttr
    {
        
        /// <summary>
        /// 证件编号
        /// </summary>
        public const string CertificatesID = "CertificatesID";
        /// <summary>
        /// 证件名称
        /// </summary>
        public const string FK_CName = "FK_CName";
        /// <summary>
        /// 证件类型
        /// </summary>
        public const string CertificatesType = "CertificatesType";
        /// <summary>
        /// 管理部门
        /// </summary>
        public const string Unit = "Unit";
        /// <summary>
        /// 保管人
        /// </summary>
        public const string KeepManID = "KeepManID";
        /// <summary>
        /// 最近使用人
        /// </summary>
        public const string OperatorName = "OperatorName";
        /// <summary>
        /// 最近使用时间
        /// </summary>
        public const string RegistrationDate = "RegistrationDate";
        ///
        ///证件状态
        ///
        public const string CertificatesState = "CertificatesState";
    }

    public class Certificate : EntityNoName
    {
        #region  属性
        public string CertificatesName {
            get {
                return this.GetValStringByKey(CertificateAttr.FK_CName);
            }
            set{
                this.SetValByKey(CertificateAttr.FK_CName, value);
            }
        }
        
        public string CertificatesID
        {
            get
            {
                return this.GetValStrByKey(CertificateAttr.CertificatesID);
            }
            set
            {
                this.SetValByKey(CertificateAttr.CertificatesID,value);
            }
        }
        public string CertificatesType
        {
            get
            {
                return this.GetValStrByKey(CertificateAttr.CertificatesType);
            }
            set
            {
                this.SetValByKey(CertificateAttr.CertificatesType, value);
            }
        }
        public string OperatorName
        {
            get
            {
                return this.GetValStrByKey(CertificateAttr.OperatorName);
            }
            set
            {
                this.SetValByKey(CertificateAttr.OperatorName, value);
            }
        }
        public string RegistrationDate
        {
            get
            {
                return this.GetValStrByKey(CertificateAttr.RegistrationDate);
            }
            set
            {
                this.SetValByKey(CertificateAttr.RegistrationDate, value);
            }
        }
        #endregion

        #region 权限控制
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 印章
        /// </summary>
        public Certificate()
        { }
        /// <summary>
        /// 印章
        /// </summary>
        /// <param name="_No"></param>
        public Certificate(string _No) : base(_No) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_ZJGLCertificate");
                map.EnDesc = "证件登记";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(CertificateAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBStringPK(CertificateAttr.Name,null,"",false,false,0,100,30);
                //map.AddTBString(CertificateAttr.Name,null,"Name",false,true,0,100,30);
                map.AddTBString(CertificateAttr.CertificatesID, null, "证件编号", true, false, 3, 3, 3);
               // map.AddDDLSysEnum(CertificateAttr.CertificatesType, 0, "证件类型", true, true, CertificateAttr.CertificatesType,
               //     "@0=营业执照@2=行医资格证@3=房产证");
                map.AddTBString(CertificateAttr.FK_CName, null, "证件名称", true, false, 0, 100, 30, false);
                map.AddTBString(CertificateAttr.Unit, null, "管理部门", true, false, 0, 100, 30, false);
                map.AddTBString(CertificateAttr.KeepManID, null, "保管人", true, false, 0, 100, 30, false);
                map.AddTBString(CertificateAttr.OperatorName, null, "最近使用人", true, false, 0, 100, 30, false);
                map.AddTBDate(CertificateAttr.RegistrationDate, null, "最近使用时间", true, false);
                map.AddDDLSysEnum(CertificateAttr.CertificatesState, 0, "证件状态", true, true, CertificateAttr.CertificatesState,
                                   "@0=未归还@1=完整@2=损坏");
                //查询条件
                //map.AddSearchAttr(CertificateAttr.CertificatesState);

                RefMethod rm = new RefMethod();
                rm.Title = "添加使用信息";
                rm.ClassMethodName = this.ToString() + ".DoUse";
                map.AddRefMethod(rm);
                this._enMap = map;
                return this._enMap;
                }
        }
        public string DoUse()    
        {
            if (BP.OA.ZJGL.API.Certificates_Use_FlowIsEnable == true)
                BP.Sys.PubClass.WinOpen("/WF/MyFlow.aspx?FK_Flow=" + API.Certificates_Use_FlowIsEnable + "&FK_Name=" + this.No, 500, 600);
            else
                BP.Sys.PubClass.WinOpen("/Comm/UIEn.aspx?EnsName=BP.OA.YJGL.SealUses&FK_Name=" + this.No, 500, 600);
            return null;
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.CertificatesName;
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
    public class Certificates : SimpleNoNames
    {
        /// <summary>
        /// 卡s
        /// </summary>
        public Certificates() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Certificate();
            }
        }
    }
}
