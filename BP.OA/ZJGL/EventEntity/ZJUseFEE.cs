﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using BP.WF.Data;
namespace BP.OA.ZJGL.EventEntity
{
    public class ZJUseFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 车辆年检 流程事件实体
        /// </summary>
        public ZJUseFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return API.Certificates_Use_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            string deptNo = this.GetValStr(NDXRptBaseAttr.FK_Dept);
            string userNo = this.GetValStr(NDXRptBaseAttr.FlowStarter);
            string CertificatesName = this.GetValStr("OA_ZJGLCertificate");
            string BeginDT = this.GetValStr("BeginDT");
            string EndDT = this.GetValStr("EndDT");
            int UseTime = this.GetValInt("UseTime");
            string CType = this.GetValStr("YXLX");
            string UseReason = this.GetValStr("UseReason");
            Int64 workid = this.WorkID;
            API.Certificates_Use(UseTime, userNo, deptNo, UseReason,CType, CertificatesName,BeginDT,EndDT, workid);
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}

