﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.DA;

namespace BP.OA.Meeting
{
    public class SponsorMeetingAttr : EntityOIDAttr
    {
        //会议议题；
        public const string Title = "Title";
        //会议类型
        public const string Type = "Type";
        //会议发起人
        public const string Fk_Emp = "Fk_Emp";
        //主持人
        public const string CompereEmpNo = "CompereEmpNo";
        //主办部门
        public const string SponsorDeptNo = "SponsorDeptNo";
        //与会人员
        public const string ParticipantsEmpNo = "ParticipantsEmpNo";
        //会议记录员
        public const string MeetingRecorder = "MeetingRecorder";
        //会议纪要
        public const string MeetingAbstract = "MeetingAbstract";
        //会议内容
        public const string Content = "Content";
        //附件
        public const string Attachment = "Attachment";
        //预定编号
        public const string FK_SponsorOID = "FK_SponsorOID";
    }
    /// <summary>
    /// 会议发起
    /// </summary>
    public class SponsorMeeting : EntityOID
    {
        #region 属性
        //会议议题；
        public string Title
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.Title);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.Title, value);
            }
        }
        /// <summary>
        /// 发起人
        /// </summary>
        public string Fk_Emp
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.Fk_Emp);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.Fk_Emp, value);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FK_EmpName
        {
            get
            {
                return this.GetValRefTextByKey(SponsorMeetingAttr.Fk_Emp);
            }
        }
        //会议类型
        public string Type
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.Type);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.Type, value);
            }
        }
        /// <summary>
        /// 主持人
        /// </summary>
        public string CompereEmpNo
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.CompereEmpNo);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.CompereEmpNo, value);
            }
        }
        //主办部门
        public string SponsorDeptNo
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.SponsorDeptNo);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.SponsorDeptNo, value);
            }
        }
        /// <summary>
        /// 与会人员
        /// </summary>
        public string ParticipantsEmpNo
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.ParticipantsEmpNo);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.ParticipantsEmpNo, value);
            }
        }
        /// <summary>
        /// 会议记录员
        /// </summary>
        public string MeetingRecorder
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.MeetingRecorder);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.MeetingRecorder, value);
            }
        }
        //会议纪要
        public string MeetingAbstract
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.MeetingAbstract);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.MeetingAbstract, value);
            }
        }
        //会议内容
        public string Content
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.Content);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.Content, value);
            }
        }
        //附件
        public string Attachment
        {
            get
            {
                return this.GetValStrByKey(SponsorMeetingAttr.Attachment);
            }
            set
            {
                this.SetValByKey(SponsorMeetingAttr.Attachment, value);
            }
        }
        /// <summary>
        /// 预定编号
        /// </summary>
        public int FK_SponsorOID
        {
            get { return this.GetValIntByKey(SponsorMeetingAttr.FK_SponsorOID); }
            set { this.SetValByKey(SponsorMeetingAttr.FK_SponsorOID, value); }
        }
        #endregion

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_SponsorMeeting");
                map.EnDesc = "会议发起";
                map.CodeStruct = "9";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(SponsorMeetingAttr.Title, string.Empty, "会议议题", true, false, 1, 200, 200);
                //会议类型
                map.AddTBString(SponsorMeetingAttr.Type, string.Empty, "会议类型", true, false, 1, 5, 50);
                //会议发起人
                map.AddDDLEntities(SponsorMeetingAttr.Fk_Emp, null, "会议发起人", new BP.Port.Emps(), true);
                //主持人
                map.AddTBString(SponsorMeetingAttr.CompereEmpNo, string.Empty, "主持人", true, false, 0, 50, 50);
                //主办部门
                map.AddTBString(SponsorMeetingAttr.SponsorDeptNo, string.Empty, "主办部门", true, false, 0, 50, 50);
                //与会人员
                map.AddTBString(SponsorMeetingAttr.ParticipantsEmpNo, string.Empty, "与会人员", true, false, 0, 4000, 500);
                //会议记录员
                map.AddTBString(SponsorMeetingAttr.MeetingRecorder, string.Empty, "会议记录员", true, false, 0, 200, 200);
                //会议纪要
                map.AddTBString(SponsorMeetingAttr.MeetingAbstract, string.Empty, "会议纪要", true, false, 0, 4000, 500);
                //会议内容
                map.AddTBString(SponsorMeetingAttr.Content, string.Empty, "会议内容", true, false, 0, 4000, 500);
                //附件
                map.AddTBString(SponsorMeetingAttr.Attachment, string.Empty, "附件", true, false, 0, 500, 50);
                //预定编号
                map.AddTBInt(SponsorMeetingAttr.FK_SponsorOID, 0, "预定编号", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }

    public class SponsorMeetings : EntitiesOID
    {
        /// <summary>
        /// 领导活动s
        /// </summary>
        public SponsorMeetings() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SponsorMeeting();
            }
        }
    }
}
