﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 短消息类别属性
    /// </summary>
    public class ShortMsgAttr : EntityOIDAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String FK_SendToUserNo = "FK_SendToUserNo";
        public const String AddTime = "AddTime";
        public const String Doc = "Doc";
        public const String AttachFile = "AttachFile";
        public const String AttachId = "AttachId";
        public const String SubjectTitle = "SubjectTitle";
        public const String GroupRedirect = "GroupRedirect";
        public const String GroupRedirectFormat = "GroupRedirectFormat";
        public const String SingleRedirect = "SingleRedirect";
        public const String SingleRedirectFormat = "SingleRedirectFormat";
        public const String Received = "Received";
        public const String IsWap = "IsWap";
    }
    /// <summary>
    /// 短消息类别
    /// </summary>
    public partial class ShortMsg : EntityOID
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(ShortMsgAttr.FK_UserNo); }
            set { this.SetValByKey(ShortMsgAttr.FK_UserNo, value); }
        }
        public string FK_SendToUserNo
        {
            get { return this.GetValStrByKey(ShortMsgAttr.FK_SendToUserNo); }
            set { this.SetValByKey(ShortMsgAttr.FK_SendToUserNo, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(ShortMsgAttr.AddTime); }
            set { this.SetValByKey(ShortMsgAttr.AddTime, value); }
        }
        public string Doc
        {
            get { return this.GetValStrByKey(ShortMsgAttr.Doc); }
            set { this.SetValByKey(ShortMsgAttr.Doc, value); }
        }
        public string AttachFile
        {
            get { return this.GetValStrByKey(ShortMsgAttr.AttachFile); }
            set { this.SetValByKey(ShortMsgAttr.AttachFile, value); }
        }
        public string AttachId
        {
            get { return this.GetValStrByKey(ShortMsgAttr.AttachId); }
            set { this.SetValByKey(ShortMsgAttr.AttachId, value); }
        }
        public String SubjectTitle
        {
            get { return this.GetValStrByKey(ShortMsgAttr.SubjectTitle); }
            set { this.SetValByKey(ShortMsgAttr.SubjectTitle, value); }
        }
        public String GroupRedirect
        {
            get { return this.GetValStrByKey(ShortMsgAttr.GroupRedirect); }
            set { this.SetValByKey(ShortMsgAttr.GroupRedirect, value); }
        }
        public String GroupRedirectFormat
        {
            get { return this.GetValStrByKey(ShortMsgAttr.GroupRedirectFormat); }
            set { this.SetValByKey(ShortMsgAttr.GroupRedirectFormat, value); }
        }
        public String SingleRedirect
        {
            get { return this.GetValStrByKey(ShortMsgAttr.SingleRedirect); }
            set { this.SetValByKey(ShortMsgAttr.SingleRedirect, value); }
        }
        public String SingleRedirectFormat
        {
            get { return this.GetValStrByKey(ShortMsgAttr.SingleRedirectFormat); }
            set { this.SetValByKey(ShortMsgAttr.SingleRedirectFormat, value); }
        }
        public bool Received
        {
            get { return this.GetValBooleanByKey(ShortMsgAttr.Received); }
            set { this.SetValByKey(ShortMsgAttr.Received, value); }
        }
        public bool IsWap
        {
            get { return this.GetValBooleanByKey(ShortMsgAttr.IsWap); }
            set { this.SetValByKey(ShortMsgAttr.IsWap, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 短消息类别
        /// </summary>
        public ShortMsg() { }
        /// <summary>
        /// 短消息类别
        /// </summary>
        /// <param name="no">编号</param>
        public ShortMsg(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_ShortMsg";
                map.EnDesc = "短消息文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(ShortMsgAttr.FK_UserNo, null, "发布用户", true, false, 2, 50, 20);
                map.AddTBString(ShortMsgAttr.FK_SendToUserNo, null, "发布对象", true, false, 2, 50, 20);
                map.AddTBDateTime(ShortMsgAttr.AddTime, null, "添加时间", true, false);
                map.AddTBStringDoc(ShortMsgAttr.Doc, null, "短消息内容", true, false);
                map.AddTBString(ShortMsgAttr.AttachFile, null, "短消息附件", true, false, 0, 4000, 50);
                map.AddTBString(ShortMsgAttr.AttachId, null, "关联编号", true, false, 0, 10, 10);
                map.AddTBString(ShortMsgAttr.SubjectTitle, null, "短消息标题", true, false, 0, 100, 50);
                map.AddTBString(ShortMsgAttr.GroupRedirect, null, "组导航链接", true, false, 0, 500, 100);
                map.AddTBString(ShortMsgAttr.GroupRedirectFormat, null, "组导航模板", true, false, 0, 500, 100);
                map.AddTBString(ShortMsgAttr.SingleRedirect, null, "单导航链接", true, false, 0, 500, 100);
                map.AddTBString(ShortMsgAttr.SingleRedirectFormat, null, "单导航模板", true, false,0,500,100);
                map.AddBoolean(ShortMsgAttr.Received, false, "是否查看", true, false);
                map.AddBoolean(ShortMsgAttr.IsWap, false, "是否同时发送手机短信", true, false);
                
                //map.AddTBString(ShortMsgAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(ShortMsgAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 开发
        /// <summary>
        /// 发送短消息
        /// </summary>
        /// <param name="sendToUsers">发送给，多个人员用逗号分开.</param>
        /// <param name="text">文本</param>
        /// <param name="attachFile"></param>
        /// <param name="attachId"></param>
        /// <param name="subject"></param>
        /// <param name="groupRedirectUrl"></param>
        /// <param name="groupRedirectFormat"></param>
        /// <param name="singleRedirectUrl"></param>
        /// <param name="singleRedirectFormat"></param>
        /// <returns></returns>
        public static int Send(string sendToUsers,string text,string attachFile,string attachId,string subject
             ,string groupRedirectUrl,string groupRedirectFormat
             ,string singleRedirectUrl,string singleRedirectFormat
            ,bool isWap)
        {
            String[] users = sendToUsers.Split(new String[] { ",", ";", "|" }, StringSplitOptions.RemoveEmptyEntries);
            int i = 0;
            foreach (String user in users)
            {
                ShortMsg sm = new ShortMsg();
                sm.FK_UserNo = BP.Web.WebUser.No;
                
                sm.FK_SendToUserNo = user;

                sm.AddTime = DateTime.Now;
                sm.Doc = text;
                sm.AttachFile = attachFile;
                sm.AttachId = attachId;
                sm.SubjectTitle = subject;
                sm.GroupRedirect = groupRedirectUrl;
                sm.GroupRedirectFormat = groupRedirectFormat;
                sm.SingleRedirect = singleRedirectUrl;
                sm.SingleRedirectFormat = singleRedirectFormat;
                sm.Received = false;
                sm.IsWap = isWap;
                sm.Insert();
                i++;
            }
            return i;
        }
        public static int Send_ShortInfo(string sendToUsers, string text, string attachFile, bool isWap)
        {
            return Send(sendToUsers, text, attachFile, "", "短消息", "/Main/Sys/ShortMsg.aspx", "", "/Main/Sys/ShortMsg.aspx", "", isWap);
        }
        public static int Send_Email(string sendToUsers, string text, string attachFile, string inID, bool isWap)
        {
            return Send(sendToUsers, text, attachFile, inID, "邮件", "/App/Message/InBox.aspx", "", "/App/Message/InDetail1.aspx?OID=" + inID, "", isWap);
        }
        public static int Send_News(string sendToUsers, string text, string attachFile, string newsId, bool isWap)
        {
            return Send(sendToUsers, text, attachFile, newsId, "新闻", "/App/News/NewsList.aspx", "", "/App/News/NewsArticle.aspx?ID=" + newsId, "", isWap);
        }
        public static int Send_Notice(string sendToUsers, string text, string attachFile, string noticeId, bool isWap)
        {
            return Send(sendToUsers, text, attachFile, noticeId, "公告", "/App/Notice/MyNoticeList.aspx", "", "/App/Notice/NoticeBrowser.aspx?OID=" + noticeId, "", isWap);
        }
        public static void ReceivedInfo(string idList,string type)
        {
            string con = String.Empty;
            if (!String.IsNullOrEmpty(idList))
            {
                con += String.Format(" and AttachId in ({0})",idList);
            }
            if (!String.IsNullOrEmpty(type))
            {
                con += String.Format(" and SubjectTitle='{0}'", type);
            }
            string sql = String.Format("Update OA_ShortMsg set Received=1 where FK_SendToUserNo='{0}'{1}", BP.Web.WebUser.No, con);
            BP.DA.DBAccess.RunSQL(sql);
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class ShortMsgs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ShortMsg();
            }
        }
        /// <summary>
        /// 短消息类别集合
        /// </summary>
        public ShortMsgs()
        {
        }
    }
}
